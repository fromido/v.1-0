import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {map} from 'rxjs/operators'
import {decimalDigest} from "@angular/compiler/src/i18n/digest";

@Injectable({
  providedIn: 'root'
})
export class AyuntamientoService {
  API_ENDPOINT = "http://tpv-generico.gestionmunicipal.es/api";
  constructor(private http: HttpClient) {}

  getQuery ( query : string) {
    const url = `${this.API_ENDPOINT}/get/${ query }`;
    return this.http.get(url);
  }

  postQuery ( query : string,data: Object) {
    const url = `${this.API_ENDPOINT}/post/${ query }`;
    return this.http.post(url,data);
  }

  getAyuntamiento(id:string) {
    return this.getQuery('ayuntamiento/'+id)
      .pipe(map(data => data));
  }
  postAyuntamiento(data:object) {
    return this.postQuery('ayuntamiento',data)
      .pipe(map(data => data));
  }
  getDocumento(id,idAyuntamiento) {
    return this.getQuery('documento/'+id+'/'+idAyuntamiento)
      .pipe(map(data => data));
  }
  getJustificante(idAyuntamiento,data:object) {
    return this.postQuery('justificante/'+idAyuntamiento,data)
      .pipe(map(data => data));
  }
  sendParams(id:number,data:object) {
    return this.postQuery('documento/'+id,data);
      //.pipe(datos => {datos});
  }
  validatePopUpForm(id:number,data:object) {
    return this.postQuery('popup/form/'+id,data);
    //.pipe(datos => {datos});
  }
  getEntidad(data:object) {
    return this.postQuery('entidad',data);
    //.pipe(datos => {datos});
  }

}
