import {Injectable} from '@angular/core';
import {Location} from '@angular/common';
import { map } from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  activeLang = 'es';
  statusSerices: boolean = false;
  messageTitle: string = 'Error';
  messageText: string = 'Error';
  messageStyle: string = 'animated fadeIn alert-danger';

  loading: boolean;
  origen:string;
  textoorigen: string;

  documentos: any[];
  idiomas: Array<{ idioma: string, value: string }> = [];
  documentosIdiomas: any[];
  documento: any[];
  ayuntamiento: any[];
  entidades: any[];
  enviarDatos:any[];


  constructor(private _location: Location,private translate: TranslateService) {
  }

  drawMessage(data): any {
    if (data.status) {
      switch (data.status) {
        case 200:
          this.messageTitle = "Solicitud OK";
          this.messageText = data.message;
          this.messageStyle = "animated fadeIn alert-success";
          setTimeout(() =>
              //this.statusSerices = false;
              this.messageStyle = 'animated zoomOut alert-success'
            , 3000);
          break;
        case 500:
          this.messageTitle = "Error";
          this.messageText = data.message;
          this.messageStyle = "animated fadeIn alert-danger";
          setTimeout(() =>
              //this.statusSerices = false;
              this.messageStyle = 'animated zoomOut alert-danger'
            , 3000);
          break
        default:
          this.messageTitle = "Error";
          this.messageText = "Error";
          this.messageStyle = "animated fadeIn alert-danger";
      }
    }
    //hay una diferencia de 0.5 segundos correspondiente con los 2 segundos del estilo de fadeOut
    setTimeout(() => {
        this.statusSerices = false;
      }
      , 3500);

  }

  originRouter(router): any {
    router.params.pipe(map(data => console.log(data['origen'])));
    router.params.subscribe(params => {
      let p = params['origen'];
      if(p){
        p = p.toLowerCase();
      }
      switch (p) {
        case 'pagament':
          this.origen = params['origen'].toLowerCase();
          this.textoorigen = '';
          break;
        case 'formulario2_ovt':
          this.origen = params['origen'].toLowerCase();
          //meter traductor
          this.textoorigen = 'general.ovt';
          break;
        case 'formulario2_ventanilla':
          this.origen = params['origen'].toLowerCase();
          //meter traductor
          this.textoorigen = 'general.ventanilla';
          break;
        case 'ppv':
          this.origen = params['origen'].toLowerCase();
          //meter traductor
          this.textoorigen = 'general.ppv';
          break;
        default:
          this.origen = 'pagament';
         //this._location.replaceState("/pagament/home/");
         this.textoorigen = '';
      }
    });
    this.cambiarLenguaje(this.activeLang);
  }
  public sendForm(datos){
    var url = datos.ruta_tpv;
    document.getElementById('div-form').innerHTML='';
    document.getElementById('div-form').innerHTML='<form name ="formSendParams" id="formSendParams" style="display: none" action="' + url + '" method="post">' +
      '<input type="text" name="Ds_SignatureVersion" value="' + datos.Ds_SignatureVersion + '" />' +
      '<input type="text" name="Ds_MerchantParameters" value="' + datos.Ds_MerchantParameters + '" />' +
      '<input type="text" name="Ds_Signature" value="' + datos.Ds_Signature + '" />' +
      '</form>';
    document.forms["formSendParams"].submit();
    //document.getElementById('formSendParams');
  }
  cambiarLenguaje(lang) {
    if(this.documentosIdiomas){
      this.documentos = this.documentosIdiomas[lang];
    }
    this.activeLang = lang;
    this.translate.use(lang);
  }
}
