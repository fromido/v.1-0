import {Injectable} from '@angular/core';
import {AbstractControl, FormControl, FormGroup} from '@angular/forms';
import {Observable} from "rxjs";

import {AyuntamientoService} from "./ayuntamiento.service";
import {DataService} from "./data.service";

interface ErrorValidate {
  [s: string]: boolean
}

@Injectable({
  providedIn: 'root'
})
export class ValidadoresService {

  constructor(private _dataService:DataService,private _ayuntamientoService:AyuntamientoService) {
  }

  comprobarEntidad(userControl: AbstractControl) {
    return new Promise(resolve => {
      this._ayuntamientoService.getEntidad( { id: userControl.value,id_documento:this._dataService.documento['id']}).subscribe((datos: any) => {
      if(!datos.entidadNotAvailable){
        resolve(null);
      }
        resolve(datos);
      }, (error) => {
        console.log(error);
        resolve(null);
      });
    });
  }

}
