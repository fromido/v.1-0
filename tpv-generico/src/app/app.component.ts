import {Component} from '@angular/core';
import {AyuntamientoService} from "./services/ayuntamiento.service";

import {DataService} from "./services/data.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public href: string = "";
  public title: string = "";
  public favicon: string = "";
  constructor(
    private ayuntServ: AyuntamientoService,
    public _dataService: DataService,
    private router: Router
  ) {
    this.loadDataApp();
  }

  loadDataApp(){
    this._dataService.loading = true;
    var parse_url = /(?<scheme>.*):\/\/(?<domain>[^\/\r\n]+)?/;
    var url =document.location.href;
    var result = parse_url.exec(url);
    const d= {url: result[0]};
    if (!this._dataService.ayuntamiento && !this._dataService.documentos) {
      this.ayuntServ.postAyuntamiento(d).subscribe((data: any) => {
        let datos = data;
        this._dataService.ayuntamiento = datos;
        this._dataService.activeLang = 'es';
        this.title = this._dataService.ayuntamiento['nombre'];
        if(this._dataService.ayuntamiento['img'] && this._dataService.ayuntamiento['img'].hasOwnProperty("mdpi")){
          document.getElementById('appFavicon').setAttribute('href', this._dataService.ayuntamiento['img']['mdpi']);
        }
        if(this._dataService.ayuntamiento['idiomadefecto']){
          this._dataService.activeLang = this._dataService.ayuntamiento['idiomadefecto'];
        }
        this._dataService.cambiarLenguaje(this._dataService.activeLang);
        if(datos['idioma'] && datos['docs']){
          let data = [];
          for (const [key, value] of Object.entries(datos['idioma'])) {
            this._dataService.idiomas.push({idioma: key, value: key});
            data[key] = datos['docs'][key];
          }
          this._dataService.documentosIdiomas=data;
          this._dataService.documentos = datos['docs'][this._dataService.activeLang];
          delete datos['docs'];
        }
        //this._dataService.entidades = datos['entidades'];
        this._dataService.statusSerices = false;
        this._dataService.loading = false;
      }, (error) => {
        this._dataService.statusSerices = false;
        //this._dataService.drawMessage({"message": "Error en la conexion", "status": 500});
        this._dataService.loading = false;
      });
    }
  }
}
