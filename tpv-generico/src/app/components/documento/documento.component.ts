import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {AyuntamientoService} from "../../services/ayuntamiento.service";
import {DataService} from "../../services/data.service";
import {decimalDigest} from "@angular/compiler/src/i18n/digest";

@Component({
  selector: 'app-documento',
  templateUrl: './documento.component.html',
  styles: []
})
export class DocumentoComponent {
  loadingDoc: boolean;
  public documento: any;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _ayunService: AyuntamientoService,
    private _dataService: DataService
  ) {
    this.getData();
  }

  getData() {
    this.loadingDoc = true;
    this._activatedRoute.params.subscribe(params => {
      if (this._dataService.origen) {
        this._dataService.originRouter(this._activatedRoute);
      }
      if(params && params.hasOwnProperty("id")){
        this.getDocumento(params['id']);
      }
    }, (erorrDoc) => {

    });
  }

  getDocumento(id: number) {
    if (this._dataService.ayuntamiento['id'] && this._dataService.ayuntamiento['id'] != undefined && this._dataService.ayuntamiento['id'] != null) {
      this._ayunService.getDocumento(id, this._dataService.ayuntamiento['id']).subscribe((data: any) => {
        if (data != null) {
          this.documento = data;
          this._dataService.documento = this.documento;

        } else {
          this._dataService.statusSerices = true;
          this._dataService.drawMessage({"message": "Error al obtener el documento", "status": 500});
        }

      }, (error) => {
        this._dataService.statusSerices = true;
        this._dataService.drawMessage({"message": "Error en la conexion", "status": 500});
      });
    }
    this.loadingDoc = false;
  }

}
