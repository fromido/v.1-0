import {Component, Input, Output, OnInit, EventEmitter} from '@angular/core';
import {FormGroup, FormBuilder, FormControl, Validators} from "@angular/forms";
import {formatNumber, formatCurrency, getCurrencySymbol} from '@angular/common';
import {MyValidations} from '../../utils/my-validations';
import {debounceTime} from "rxjs/operators";

import {AyuntamientoService} from "../../services/ayuntamiento.service";
import {DataService} from "../../services/data.service";
import {ValidadoresService} from "../../services/validadores.service";

// Declaramos las variables para jQuery
declare var $: any;

@Component({
  selector: 'mod1y2Form',
  templateUrl: './mod1y2.component.html',
  styleUrls: ['./mod1y2.component.css']
})

export class Mod1y2Component implements OnInit {

  @Input() idDoc: number;
  @Output() loadingValidacion = new EventEmitter<boolean>();
  loadSend: boolean = false;

  modalidad: number;
  formMod1y2: FormGroup;
  currencyPattern: string

  constructor(
    private formBuilder: FormBuilder,
    private _ayuntamientoService: AyuntamientoService,
    private _dataService: DataService,
    private _validatorService: ValidadoresService
  ) {
    //Inicializamos los controles del formulario

    this.modalidad = this._dataService.documento['modalidad_id'];

    this.buildForm();
  }

  ngOnInit() {
    this.formMod1y2.get('codBar').disable();
  }

  get codBarNoValido() {
    return this.formMod1y2.get('codBar').invalid && this.formMod1y2.get('codBar').touched;
  }

  get entidadNoValido() {
    return this.formMod1y2.get('entidad').invalid && this.formMod1y2.get('entidad').touched
  }

  get referenciaNoValido() {
    return this.formMod1y2.get('referencia').invalid && this.formMod1y2.get('referencia').touched;
  }

  get identificacionNoValido() {
    return this.formMod1y2.get('identificacion').invalid && this.formMod1y2.get('identificacion').touched;
  }

  get importeNoValido() {
    return this.formMod1y2.get('importe').invalid && this.formMod1y2.get('importe').touched;
  }

  public getEntidades() {
    type MyArrayType = Array<{ id: string, text: string }>;
    let arrayEntidades: MyArrayType = [];
    this._dataService.documento['docstpv_emisoras'].forEach((myObject, index) => {
      arrayEntidades.push({id: myObject.emisora_id, text: myObject.emisora_id});
    });
    arrayEntidades.push({id: "666666", text: "este no va"});
    return arrayEntidades;
  }

  public formatCurrencyValue(event) {
    var intIndex = event.target.value.indexOf(",");
    var mystring = event.target.value;
    if (intIndex >= -1) {
      mystring = event.target.value.replace(",", ".");
    }
    if (mystring.split(".").length > 2) {
      var temp = mystring.split(".");
      mystring = '';
      temp.forEach((myObject, index) => {
        if (temp.length - 1 != index) {
          mystring += myObject;
        } else {
          mystring += '.' + myObject;
        }
      });
    }
    event.target.value = formatNumber(mystring, 'en-US', '1.2-2');
    this.formMod1y2.get('importe').setValue(event.target.value);
  }

  private buildForm() {
    this.currencyPattern = "^([0-9]+[\,.]?[0-9]{0,10})$";
    this.formMod1y2 = this.formBuilder.group({
      cbCodBar: [false],
      codBar: ['', [Validators.required, MyValidations.isInvalidCodBar(this.modalidad)]],
      entidad: ['', Validators.required, this._validatorService.comprobarEntidad.bind(this._validatorService)],
      referencia: ['', [Validators.required, Validators.minLength(12), Validators.maxLength(12)]],
      identificacion: ['', [Validators.required, MyValidations.isInvalidIdentificacion(this.modalidad)]],
      //importe: ['', [Validators.required, Validators.pattern(this.currencyPattern)]],
      importe: ['', Validators.required],
      idAyuntamiento: [this._dataService.ayuntamiento['id'], Validators.required]
    });

    this.formMod1y2.get('cbCodBar').valueChanges.subscribe(v => {
      if (v) {
        console.log("El value del cbcodbar es: " + this.formMod1y2.get('cbCodBar').value);
        this.habilitaCodBar();
      } else {
        console.log("El value del cbcodbar es: " + this.formMod1y2.get('cbCodBar').value);
        this.deshabilitaCodBar();
      }
    });
    /*
    //Escucha reactiva cada al medio segundo de inactividad
    this.formMod1y2.valueChanges
    .pipe(
    debounceTime(500)
    )
    .subscribe(value => {
       console.log(value);
     });
    */

  }

  //Enviamos el evento nativo de html y cancelamos el comportamiento por defecto para que nunca haga una recarga.
  save(event: Event, idDoc) {
    //setear value que ha guardado lo el send
    this.loadSend = true;
    const value = this.formMod1y2.value;
    value['origen']=this._dataService.origen;
    value['idioma']=this._dataService.activeLang;
    event.preventDefault();
    if (this.formMod1y2.valid) {
      this._ayuntamientoService.sendParams(idDoc, value).subscribe((datos: any) => {
        //this.loadingValidacion.emit(false);

        if (datos.status && datos.status != 500) {
          if(this._dataService.origen != 'pagament'){
            this._dataService.enviarDatos = value;
            $("#boton_popup").click();
            this.loadSend = false;
          }else{
            this._dataService.sendForm(datos);
          }
        } else {
          this._dataService.statusSerices = true;
          this._dataService.drawMessage(datos);
          this.loadSend = false;
        }
      }, (error) => {
        console.log("ERROR");
        console.log(error)
      });
    } else {
      this.formMod1y2.markAllAsTouched();
      this.loadSend = false;
    }
  }

  //Habilitamos el checkbox del código de barras
  habilitaCodBar() {
    this.formMod1y2.get('codBar').enable();
    this.formMod1y2.get('entidad').disable();
    this.formMod1y2.get('referencia').disable();
    this.formMod1y2.get('identificacion').disable();
    this.formMod1y2.get('importe').disable();
    this.formMod1y2.get('entidad').setValue('');
    this.formMod1y2.get('referencia').setValue('');
    this.formMod1y2.get('identificacion').setValue('');
    this.formMod1y2.get('importe').setValue('');
    this.formMod1y2.markAsUntouched();
  }

  deshabilitaCodBar() {
    this.formMod1y2.get('codBar').disable();
    this.formMod1y2.get('codBar').setValue('');
    this.formMod1y2.get('entidad').enable();
    this.formMod1y2.get('referencia').enable();
    this.formMod1y2.get('identificacion').enable();
    this.formMod1y2.get('importe').enable();
    this.formMod1y2.markAsUntouched();
  }

  //Validar entrada de números, sin permitir decimales
  isNumberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
  }

  getErrorMessages(control: string) {
    if (this.formMod1y2.get(control).errors && this.formMod1y2.get(control).touched) {
      switch (control) {
        case 'codBar':
          return this.formMod1y2.get(control).hasError('required') ? 'error.required.codbar' :
            this.formMod1y2.get(control).hasError('isInvalidCodBar') ? 'error.format.codbar':
              '';
          break;
        case 'entidad':
          if (this.formMod1y2.get(control).hasError('required')) {
            return 'error.required.entidad';
          }
          if (this.formMod1y2.get(control).hasError('minlength') || this.formMod1y2.get(control).hasError('maxlength')) {
            return 'error.length.entidad';
          }
          if (this.formMod1y2.get(control).hasError('entidadNotAvailable')) {
            return 'La emisora introducida no es correcta';
          }
          break;
        case 'referencia':
          return this.formMod1y2.get(control).hasError('required') ? 'error.required.referencia' :
            this.formMod1y2.get(control).hasError('minlength') || this.formMod1y2.get(control).hasError('maxlength') ? 'error.format.referencia' :
              '';
          break;
        case 'identificacion':
          return this.formMod1y2.get(control).hasError('required') ? 'error.required.identificacion' :
            this.formMod1y2.get(control).hasError('isInvalidIdentificacion') ? 'error.format.identificacion' :
              '';
          break;
        case 'importe':
          return this.formMod1y2.get(control).hasError('required') ? 'error.required.importe' :
            this.formMod1y2.get(control).hasError('pattern') ? 'error.format.importe' :
              '';
          break;
        default :
          return '';
      }
      this._dataService.cambiarLenguaje(this._dataService.activeLang);
    }
  }


}
