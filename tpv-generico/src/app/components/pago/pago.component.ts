import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {AyuntamientoService} from "../../services/ayuntamiento.service";
import {DataService} from "../../services/data.service";
import {PdfMakeWrapper, Txt} from 'pdfmake-wrapper';

@Component({
  selector: 'app-pago',
  templateUrl: './pago.component.html'
})
export class PagoComponent {
  loadingPago: boolean;
  public dataJustificante: any;
  dsMerchantParameters: string = '';
  dsSignature: any = '';

  dsSignatureVersion: string = '';
  nombreDocumento: string = '';
  fechaDocumento: string = '';
  paramentros: any = '';

//  dsMerchantData:any;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private _ayunService: AyuntamientoService, private _activatedRoute: ActivatedRoute, private _dataService: DataService
  ) {
    this.dataJustificante = null;
    this.getData();
  }

  getData() {
    this.loadingPago = true;
    this.activatedRoute.queryParams.subscribe(params => {
        try {
          if (this._dataService.origen) {
            this._dataService.originRouter(this._activatedRoute);
          }
          if (params['Ds_MerchantParameters'] && params['Ds_MerchantParameters'] != '') {
            this.dsMerchantParameters = JSON.parse(atob(params['Ds_MerchantParameters']));
          }
          if (params['Ds_SignatureVersion'] && params['Ds_SignatureVersion'] != '') {
            this.dsSignatureVersion = params['Ds_SignatureVersion'];
          }
          if (params['Ds_Signature'] && params['Ds_Signature'] != '') {
            this.dsSignature = params['Ds_Signature'];
          }
          if (this.dsSignatureVersion != '' && this.dsMerchantParameters != '' && this.dsSignature != '') {
            this.getJustificantePago(this._dataService.ayuntamiento['id']);
          }
        } catch (Error) {
          this._dataService.statusSerices = true;
          this._dataService.drawMessage({"message": "Los elementos enviados no coinciden", "status": 500});
          this.router.navigate([this._dataService.origen,'home']);
        }
      }, (erorrDoc) => {
          this.router.navigate([this._dataService.origen,'home']);
      }
    );
  }

  getJustificantePago(id: number) {
    this.dataJustificante = {
      dsSignatureVersion: this.dsSignatureVersion,
      dsMerchantParameters: this.dsMerchantParameters,
      dsSignature: this.dsSignature
    };
    this._ayunService.getJustificante(id, this.dataJustificante).subscribe((data: any) => {
      if (data != null) {
        if ((data['data']) != null) {
          this.printData(data['data']);
          this._dataService.statusSerices = true;
          this._dataService.drawMessage({"message": data['message'], "status": 200});
        } else {
          this.router.navigate([this._dataService.origen,'home']);
          this._dataService.drawMessage({
            "message": "No encuentra la notificacion que quieres imprimir ",
            "status": 500
          });
          this._dataService.statusSerices = true;
        }
      } else {
        this._dataService.statusSerices = true;
        this._dataService.drawMessage({"message": "Error al obtener el pago", "status": 500});
      }
      this.loadingPago = false;
    }, (error) => {
      this._dataService.statusSerices = true;
      this._dataService.drawMessage({"message": "Error en la conexion", "status": 500});
    });
    this.loadingPago = false;
  }

  printData(data: any) {
    //buscar el documento que se envia
    let nombreDocumento = '';
    this._dataService.documentos.forEach(function (doc, key) {
      if (data.idDoc == doc['docs_id']) {
        nombreDocumento = doc.nombre_doc;
      }
    });
    //*ngFor="let parametro of paramentros"
    this.nombreDocumento = nombreDocumento;
    this.fechaDocumento = data.fecha;
    let arr = [];
    Object.keys(data).map(function (key) {
      if (data.hasOwnProperty("codbar") && !arr.hasOwnProperty("codbar")) {
        arr[key] = data[key];
      } else {
        if (!data.hasOwnProperty("codbar")) {
          arr[key] = data[key];
        }
      }
    });

    this.paramentros = arr;
  }


  imprimir(idDiv: string) {
    let popupWinindow;
    let innerContents = document.getElementById(idDiv).innerHTML;
    popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
    popupWinindow.document.open();
    popupWinindow.document.write('' +
      '<html>' +
      '<head>' +
      '<link rel="stylesheet" type="text/css" href="style.css" />' +
      '</head>' +
      '<body onload="window.print()">' + innerContents + '</body>' +
      '</html>');
    popupWinindow.document.close();
  }

  descargar(idDiv: string) {
    const pdf = new PdfMakeWrapper();
    pdf.info({
      title: 'Resguardo de pago',
      author: 'Tpv- del ayuntamiento',
      subject: 'cliente',
    });
    pdf.add(
      new Txt('INTRODUCIR EL CONTENIDO QUE QUEREMOS EN EL PAGO').bold().end
    );
    pdf.add(
      new Txt('CON IMAGENES Y TODO ESO').italics().end
    );
    pdf.create().open();
  }
}

