import {Component, OnInit, Input} from '@angular/core';
import {Router} from "@angular/router";
import {DataService} from "../../../services/data.service";

@Component({
  selector: 'app-card-doc',
  templateUrl: './card-doc.component.html',
  styles: []
})
export class CardDocComponent {
  @Input() doc: any[];

  constructor(private router: Router, public _dataService: DataService) {
  }
  verDocumento(doc:any){
    let idDoc;
    if(doc.docs_id){
      idDoc = doc.docs_id
    }
    this.router.navigate([this._dataService.origen,'documento',idDoc]);
  }
}
