import {Component, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {DataService} from "../../../services/data.service";
import {AyuntamientoService} from  "../../../services/ayuntamiento.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html'
})
export class PopupComponent implements OnInit {

  @Input() d: any[];
  @Input() sw: number;
  forma:FormGroup;
  loadSendPopUp: boolean = false;
  mesajeError:string;
  typeData:string;

  constructor(
    private fb:FormBuilder,
    private router: Router,
    public _dataService: DataService,
    public _ayuntamientoService:AyuntamientoService) {
    this.crearFormulario();
  }

  /**
   * ESTA PARTE ESTA DISEÑADA PARA CUANDO SE LE ENVIA UN SW 1 AL POP UP Y SOLAMENTE SALDRA LAS ENTIDADES
   */
  verDocumento(id:any){
    let idDoc;
    if(id){
      idDoc = id
    }
    this.router.navigate([this._dataService.origen,'documento',idDoc]);
  }
  /**
   * FIN DE LA PARTE 1 DEL SW
   */
  ngOnInit(): void {
  }

  /**
   * ESTA PARTE ESTA DISEÑADA PARA CUANDO SE LE ENVIA UN SW 2 AL POP UP
   */
  crearFormulario(){
    this.forma= this.fb.group({
      type:['',Validators.required],
      data:['',Validators.required],
      check:['']
    });
    /*this.forma= this.fb.group({
      telefono:['',[Validators.required,Validators.minLength(9),Validators.maxLength(9)]],
      correo:['',[Validators.required,Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]]
    });*/
    this.forma.get('type').disable();
    this.forma.get('data').disable();
  }

  get selectTypeValido(){
    return this.forma.get('type').invalid && this.forma.get('type').touched;
  }

  get typeNoValido(){
    return this.forma.get('data').invalid && this.forma.get('data').touched;
  }

  deleteValue(){
    this.loadSendPopUp = false;
    this.forma.get('type').setValue('');
    this.forma.get('data').setValue('');
    this.forma.get('type').disable();
    this.forma.get('data').disable();
    this.forma.get('check').setValue('');
  }

  activedButton(event){
    this.forma.get('type').setValue('');
    this.forma.get('data').setValue('');
    if(event.target.checked){
      this.forma.get('type').enable();
    }else{
      this.forma.get('type').disable();
      this.forma.get('data').disable();
    }
    this.loadSendPopUp=false;
  }

  changeDataForm(event){
    if(event.target.value){
    switch (event.target.value) {
      case "telefono":
        this.forma.get('data').setValidators([Validators.required,Validators.minLength(9),Validators.maxLength(9)]);
        this.mesajeError='Telefono Incorreto';
        break;
        case "email":
          this.forma.get('data').setValidators([Validators.required,Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]);
          this.mesajeError='Correo electronico incorrecto';
        break;
    }
    this.forma.get('data').enable();
  }else{
      this.mesajeError='';
      this.forma.get('data').disable();
      this.forma.get('data').setValidators([]);
    }
  }

  sendFormPopUp(idDoc,event: Event) {
    this.loadSendPopUp = true;
    event.preventDefault();
    if (this.forma.valid) {
      const value = Object.assign({}, this.forma.value, this._dataService.enviarDatos);
      this._ayuntamientoService.validatePopUpForm(idDoc, value ).subscribe((datos: any) => {
        console.log(datos);
        if (datos.status && datos.status != 500) {
            this._dataService.sendForm(datos);
        } else {
          Object.keys(datos).map((key) => {
            this.forma.controls[key].setErrors({'incorrect': datos[key]});
          });
          this.loadSendPopUp = false;
        }
      }, (error) => {
        console.log("ERROR");
        console.log(error)

      });
    } else {
      this.forma.markAllAsTouched();
      this.loadSendPopUp = false;
    }
  }
  /**
   * FIN DE LA PARTE 2 DEL SW
   */
  redirectLocation(web){
    if(web){
      window.location.href=web;
    }
  }
}
