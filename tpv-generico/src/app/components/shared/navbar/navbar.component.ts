import { Component, OnInit } from '@angular/core';
import {DataService} from "../../../services/data.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styles: []
})
export class NavbarComponent implements OnInit {

 constructor(public _dataService: DataService,private _activatedRoute: ActivatedRoute,) {
    this._dataService.originRouter(this._activatedRoute);
  }

  ngOnInit() {
  }
}
