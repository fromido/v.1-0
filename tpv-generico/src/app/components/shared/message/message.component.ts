import {Component, OnInit} from '@angular/core';
import {DataService} from '../../../services/data.service'

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html'
})
export class MessageComponent implements OnInit {

  alerta: string = 'alert-danger';

  constructor(public _dataService: DataService) {
  }

  ngOnInit() {
  }

}
