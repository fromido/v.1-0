import {Component, OnInit} from '@angular/core';
import {AyuntamientoService} from "../../services/ayuntamiento.service";
import {ActivatedRoute} from "@angular/router";
import {DataService} from "../../services/data.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent implements OnInit {


  constructor(public _dataService:DataService,private _activatedRoute:ActivatedRoute) {
    this._dataService.originRouter(this._activatedRoute);
  }

  ngOnInit() {

  }

}
