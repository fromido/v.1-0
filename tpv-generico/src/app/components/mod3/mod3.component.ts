import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MyValidations} from "../../utils/my-validations";
import {DataService} from "../../services/data.service";
import {DatePipe} from "@angular/common";
import {AyuntamientoService} from "../../services/ayuntamiento.service";

// Declaramos las variables para jQuery
declare var $: any;

@Component({
  selector: 'mod3Form',
  templateUrl: './mod3.component.html',
  styleUrls: ['./mod3.component.css']
})
export class Mod3Component implements OnInit {

  @Input() idDoc: number;
  @Output() loadingValidacion = new EventEmitter<boolean>();

    loadSend: boolean = false;


  formMod3: FormGroup;
  currencyPattern: string
  tipodoc: string;

  constructor(
    private formBuilder: FormBuilder,
    private _ayuntamientoService: AyuntamientoService,
    private _dataService : DataService,
    private datePipe : DatePipe)
  {
    //Inicializamos los controles del formulario

    this.buildForm();
  }

  ngOnInit() {
    this.formMod3.get('codBar').disable();
  }

  private buildForm() {
    this.currencyPattern = "^([0-9]+[\,.]?[0-9]{0,10})$";

    this.formMod3 = this.formBuilder.group({
      idAyuntamiento: [this._dataService.ayuntamiento['id'], Validators.required],
      cbCodBar: [false],
      codBar: ['', [Validators.required, MyValidations.isInvalidCodBar(3)]],
      entidad: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(6)]],
      numJustificante: ['', [Validators.required, Validators.minLength(13), Validators.maxLength(13)]],
      importe: ['', [Validators.required, Validators.pattern(this.currencyPattern)]],
      tipoDoc: ['', [Validators.required]],
      numDoc: ['', [Validators.required, MyValidations.isInvalidDocIden]],
      fecDevengo: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
      expediente: ['', [Validators.required, Validators.minLength(12), Validators.maxLength(12)]],
      datoEspecifico: ['', [Validators.required, Validators.minLength(20), Validators.maxLength(20)]],
    });

    this.formMod3.get('numDoc').disable();
    this.formMod3.get('expediente').disable();
    this.formMod3.get('fecDevengo').disable();
    this.formMod3.get('datoEspecifico').disable();

    this.formMod3.get('cbCodBar').valueChanges.subscribe(v => {
      if (v) {
        this.habilitaCodBar()
      } else {
        this.deshabilitaCodBar()
      }
    });

    this.formMod3.get('fecDevengo').valueChanges.subscribe(v => {
      if (v) {
        console.log(console.log("el valor de la fecha devengo es: " + v));
        //TODO Cuando enviemos la fecDevengo, coger el transform del pipe de abajo
        console.log("el valor de la fecha devengo formateada es: " + this.datePipe.transform(v, 'ddMMyyyy'));
      }
    });

  }

  save(event: Event, idDoc) {
    //loadSend es una variable que nos marca cuando se están enviando los datos
    this.loadSend = true;
    //almacenamos en value los datos del formulario
    const value = this.formMod3.value;
    //enviamos el evento nativo de html y cancelamos el comportamiento por defecto para que nunca haga una recarga
    event.preventDefault();
    //almacenamos el parámetro origen dentro como un dato más del formulario, que proviene del dataService
    value['origen'] = this._dataService.origen;
    //almacenamos el parámetro idioma como un dato más del formulario, que proviene del dataService
    value['idioma']=this._dataService.activeLang;

    //validamos el formulario
    if (this.formMod3.valid) {
      //enviamos al AyuntamientoService los parámetros y comprobamos la vuelta
      this._ayuntamientoService.sendParams(idDoc, value).subscribe((datos: any) => {
      //si hay respuesta, y no es un 500, comprobamos el origen
        if (datos.status && datos.status != 500) {
          //Cuando el origen NO es pagament,
          if (this._dataService.origen != 'pagament') {
            this._dataService.enviarDatos = value;
            //TODO Esto se debe hacer con ANGULAR
            //Se lanza el popup para introducir teléfono o email para recibir el justificante
            $("#boton_popup").click();
            this.loadSend = false;
          } else {
            //Si el origen es pagament, enviamos los datos
            this._dataService.sendForm(datos)
          }
        } else {
          //Si no hay respuesta, o es un 500, procedemos a mostrar el error
          this._dataService.statusSerices = true;
          this._dataService.drawMessage(datos);
          this.loadSend = false;
        }
      }, (error) => {
        //Escribimos en el log si nos tira un error.
        //TODO Decidir qué hacer con este mensaje, también en Mod1y2
        console.log("ERROR");
        console.log(error);
      });
    } else {
      //Si el formulario no es válido, volvemos a empezar.
      this.formMod3.markAllAsTouched();
      this.loadSend = false;
    }
  }

  //Validar entrada de números, sin permitir decimales
  isNumberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
  }

  onChange(control: string) {
    switch (control) {
      case 'tipoDoc':
        this.formMod3.get('numDoc').setValue('');
        this.formMod3.get('numDoc').markAsUntouched();
        this.formMod3.get('numDoc').enable();
        this.tipodoc = this.formMod3.get('tipoDoc').value.toString();
        console.log('El valor de this.tipodoc es:' + this.tipodoc);

        this.formMod3.controls['numDoc'].clearValidators();
        this.formMod3.controls['numDoc'].setValidators([Validators.required, MyValidations.isInvalidDocIden(this.tipodoc)]);
        break;
    }
  }

  salidaInput(control: string) {

    if (!this.formMod3.get(control).errors) {
      switch (control){
        case 'codBar':

          this.limpiaSalidaInput('codBar');
          if (this.formMod3.get(control).value.toString().length == 24) {
            console.log("Entro en el if");
            console.log(this.formMod3.get(control).value.substr(13, 1));
            switch (this.formMod3.get(control).value.substr(13, 1)) {
              case '1':
                this.formMod3.get('expediente').enable();
                break;
              case '2':
                this.formMod3.get('fecDevengo').enable()
                break;
              case '3':
                this.formMod3.get('expediente').enable();
                this.formMod3.get('fecDevengo').enable();
                break;
              case '4':
                this.formMod3.get('datoEspecifico').enable();
                break;
              case '5':
                this.formMod3.get('expediente').enable();
                this.formMod3.get('datoEspecifico').enable()
                break;
              case '6':
                this.formMod3.get('fecDevengo').enable()
                this.formMod3.get('datoEspecifico').enable()
                break;
              case '7':
                this.formMod3.get('fecDevengo').enable()
                this.formMod3.get('datoEspecifico').enable()
                this.formMod3.get('expediente').enable();
                break;
              case '8':
                this.formMod3.get('tipoDoc').disable();
                this.formMod3.get('numDoc').disable();
                break;
            }
          }
          break;

        case 'numJustificante':
          this.limpiaSalidaInput('numJustificante');
          if (this.formMod3.get(control).value.toString().length == 13) {
            switch (this.formMod3.get(control).value.toString().substr(3, 1)) {
              case '1':
                this.formMod3.get('expediente').enable();
                break;
              case '2':
                this.formMod3.get('fecDevengo').enable()
                break;
              case '3':
                this.formMod3.get('expediente').enable();
                this.formMod3.get('fecDevengo').enable();
                break;
              case '4':
                this.formMod3.get('datoEspecifico').enable();
                break;
              case '5':
                this.formMod3.get('expediente').enable();
                this.formMod3.get('datoEspecifico').enable();
                break;
              case '6':
                this.formMod3.get('fecDevengo').enable();
                this.formMod3.get('datoEspecifico').enable();
                break;
              case '7':
                this.formMod3.get('fecDevengo').enable();
                this.formMod3.get('datoEspecifico').enable();
                this.formMod3.get('expediente').enable();
                break;
              case '8':
                this.formMod3.get('tipoDoc').disable();
                this.formMod3.get('numDoc').disable();
                break;
            }
          }
          break;
      }
    }
  }



  habilitaCodBar() {
    this.formMod3.get('codBar').enable();
    this.formMod3.get('entidad').disable();
    this.formMod3.get('numJustificante').disable();
    this.formMod3.get('importe').disable();
    this.formMod3.get('tipoDoc').disable();
    this.formMod3.get('numDoc').disable();
    this.formMod3.get('fecDevengo').disable();
    this.formMod3.get('expediente').disable();
    this.formMod3.get('datoEspecifico').disable();
    this.formMod3.get('entidad').setValue('')
    this.formMod3.get('numJustificante').setValue('');
    this.formMod3.get('importe').setValue('');
    this.formMod3.get('tipoDoc').setValue('');
    this.formMod3.get('numDoc').setValue('');
    this.formMod3.get('fecDevengo').setValue('');
    this.formMod3.get('expediente').setValue('');
    this.formMod3.get('datoEspecifico').setValue('');
    this.formMod3.markAsUntouched();
  }

  deshabilitaCodBar() {
    this.formMod3.get('codBar').disable();
    this.formMod3.get('entidad').enable();
    this.formMod3.get('numJustificante').enable();
    this.formMod3.get('importe').enable();
    this.formMod3.get('tipoDoc').enable();
    this.formMod3.get('numDoc').enable();
    this.formMod3.get('fecDevengo').disable();
    this.formMod3.get('expediente').disable();
    this.formMod3.get('datoEspecifico').disable();
    this.formMod3.get('codBar').setValue('')
    this.formMod3.markAsUntouched();
  }

  limpiaSalidaInput(control: string){
    console.log('Entrada en limpiasalidainput:'+ control);
    switch (control) {
      case 'codBar':
        this.formMod3.get('entidad').disable();
        this.formMod3.get('numJustificante').disable();
        this.formMod3.get('importe').disable();
        this.formMod3.get('tipoDoc').disable();
        this.formMod3.get('numDoc').disable();
        this.formMod3.get('fecDevengo').disable();
        this.formMod3.get('expediente').disable();
        this.formMod3.get('datoEspecifico').disable();
        break;

      case 'numJustificante':
        this.formMod3.get('codBar').disable()
        this.formMod3.get('fecDevengo').disable();
        this.formMod3.get('expediente').disable();
        this.formMod3.get('datoEspecifico').disable();
        break;
    }
  }

  getErrorMessages(control: string) {
    if (this.formMod3.get(control).errors && this.formMod3.get(control).touched) {
      switch (control) {
        case 'codBar':
          return this.formMod3.get(control).hasError('required') ? 'El código de barras es obligatorio.' :
            this.formMod3.get(control).hasError('isInvalidCodBar') ? 'El código de barras no tiene un formato válido' :
              '';
        case 'entidad':
          return this.formMod3.get(control).hasError('required') ? 'Selecciona una emisora' :
            this.formMod3.get(control).hasError('minlength') || this.formMod3.get(control).hasError('maxlength') ? 'La emisora seleccionada no es válida' :
              '';
        case 'numJustificante':
          return this.formMod3.get(control).hasError('required') ? 'El numJustificante es obligatorio' :
            this.formMod3.get(control).hasError('minlength') || this.formMod3.get(control).hasError('maxlength') ? 'La longitud del número de justificante no es correcta' :
              this.formMod3.get(control).hasError('isInvalidNumJustificante') ? 'El número de justificante no es correcto' :
                '';
        case 'importe':
          return this.formMod3.get(control).hasError('required') ? 'El importe es obligatorio.' :
            this.formMod3.get(control).hasError('pattern') ? 'El importe no tiene un formato válido.' :
              '';
        case 'tipoDoc':
          return this.formMod3.get(control).hasError('required') ? 'Selecciona un tipo de documento de identidad.':
            '';
        case 'numDoc':
          return this.formMod3.get(control).hasError('required') ? 'El documento de identificación es obligatorio.':
            this.formMod3.get(control).hasError('isInvalidDocIden') ? 'El documento de identificación no es válido':
            '';
        case 'fecDevengo':
          return this.formMod3.get(control).hasError('required') ? 'La fecha de devengo es obligatoria' :
            this.formMod3.get(control).hasError('minlength') || this.formMod3.get(control).hasError('maxlength') ? 'La fecha de devengo no es válida' :
              '';
        case 'expediente':
          return this.formMod3.get(control).hasError('required') ? 'El expediente es obligatorio' :
            this.formMod3.get(control).hasError('minlength') || this.formMod3.get(control).hasError('maxlength') ? 'El expediente no es válido' :
              '';
        case 'datoEspecifico':
          return this.formMod3.get(control).hasError('required') ? 'El dato específico es obligatorio' :
            this.formMod3.get(control).hasError('minlength') || this.formMod3.get(control).hasError('maxlength') ? 'El dato específico no es válido' :
              '';
        default :
          return '';
      }
    }
  }
}
