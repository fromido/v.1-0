import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'noimage'
})
export class NoimagePipe implements PipeTransform {
  transform(img: any[]): string {
    if(!img){
      return 'assets/img/no_img.png';
    }
    //borrar cuando tenga bien definio del pipe
      return 'assets/img/boton_doc_ingreso.png';
  }
}
