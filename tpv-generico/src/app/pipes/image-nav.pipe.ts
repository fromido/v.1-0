import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'imageNav'
})
export class ImageNavPipe implements PipeTransform {

  transform(img: any[]): any {
    if(!img){
      return 'assets/img/logo_empresa.png';
    }
    //borrar cuando tenga bien definio del pipe
    //return 'assets/img/boton_doc_ingreso.png';
    return img;
  }

}
