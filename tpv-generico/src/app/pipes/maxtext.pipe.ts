import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'maxtext'
})
export class MaxtextPipe implements PipeTransform {

  transform(text: string, maxText:number): any {
    if(text && (text.length > maxText)){
      return text.substr(0, maxText)+'...';
    }
    return text;
  }

}
