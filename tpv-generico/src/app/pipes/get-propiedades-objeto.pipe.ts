import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'getPropiedadesObjeto'
})

export class GetPropiedadesObjetoPipe implements PipeTransform {

  transform(object: any,propiedad:string): boolean {
    for (let prop in object) {
      if(prop == propiedad){
        return true;
      }
    }
    return false;
  }
}
