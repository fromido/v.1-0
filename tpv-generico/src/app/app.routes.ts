import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import {DocumentoComponent} from "./components/documento/documento.component";
import {PagoComponent} from "./components/pago/pago.component";

const APP_ROUTES: Routes = [
  {path:':origen/home',component:HomeComponent},
  {path:':origen/documento/:id',component:DocumentoComponent},
  {path:':origen/pago_OKKO',component:PagoComponent},
  {path:'',pathMatch:'full',redirectTo:'pagament/home'},
  {path:'**',pathMatch:'full',redirectTo:'pagament/home'}
];
export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
