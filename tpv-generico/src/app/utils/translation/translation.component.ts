import {Component,OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {DataService} from "../../services/data.service";
@Component({
  selector: 'app-translation',
  templateUrl: './translation.component.html'
})
export class TranslationComponent implements OnInit {

  constructor(
    private translate: TranslateService, private _dataService: DataService,
  ) {
    this.translate.setDefaultLang(this._dataService.activeLang);
  }
  ngOnInit() {}

}
