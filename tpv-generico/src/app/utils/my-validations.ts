import {AbstractControl, ValidatorFn} from '@angular/forms'


export class MyValidations {

  static isInvalidCodBar(modalidad: number) {
    return (control: AbstractControl) => {
      const value = control.value;
      switch (modalidad) {
        case 1:
          if (value.toString().substr(0, 5) == '90502' && value.toString().length == 38) {
            return null;
          } else if (value.toString().substr(0, 5) == '90508' && value.toString().length == 48) {
            return null;
          } else {
            return {isInvalidCodBar: true};
          }
        case 2:
          if (value.toString().substr(0, 5) == '90521' && value.toString().length == 42) {
            //console.log("Código de barras válido");
            return null;
          } else if (value.toString().substr(0, 5) == '90522' && value.toString().length == 54) {
            //console.log("Código de barras válido");
            return null;
          } else {
            //console.log("Código de barras no válido");
            return {isInvalidCodBar: true};
          }
        case 3 :
          if (value.toString().substr(0, 5) == '90523' && value.toString().length == 24) {
            console.log("Código de barras válido");
          } else {
            //console.log("Código de barras no válido");
            return {isInvalidCodBar: true}
          }
      }
    }
  }

  static isInvalidIdentificacion(modalidad: number) {
    console.log("Entro en isInvalidIdentificacion");
    console.log("Recojo la modalidad:" + modalidad);
    return (control: AbstractControl) => {
      const value = control.value;

      switch (modalidad) {
        case 1 :
          if (value.toString().length == 7) {
            console.log("Identificación válida");
            return null;
          } else {
            console.log("Identificación no válida");
            return {isInvalidIdentificacion: true};
          }
        case 2 :
          if (value.toString().length == 10) {
            console.log("Identificación válida");
            return null;
          } else {
            console.log("Identificación no válida");
            //strCodBar.setErrors({'invalid': true});
            return {isInvalidIdentificacion: true};
            break;
          }
      }
    }
  }

  static isInvalidDocIden(tipoDoc: string): ValidatorFn {
    return (numDoc: AbstractControl): { [key: string]: boolean } | null => {
      console.log("Entras en isinvaliddociden");
      console.log("Llega tipoDoc : " + tipoDoc);
      console.log("Llega numDoc : " + numDoc.value);

      if (tipoDoc != undefined && numDoc != undefined) {

        if (tipoDoc == 'C' || tipoDoc == 'X' || tipoDoc == 'N') {
          // Comprueba si es un DNI correcto (entre 5 y 8 letras seguidas de la letra que corresponda).
          // Acepta NIEs (Extranjeros con X, Y o Z al principio)

          var numero, letr, letra;
          var expresion_regular_dni = /^[XYZ]?\d{5,8}[A-Z]$/;

          numDoc == numDoc.value.toString().toUpperCase();

          if (expresion_regular_dni.test(numDoc.value) === true) {
            numero = numDoc.value.substr(0, numDoc.value.length - 1);
            numero = numero.replace('X', 0);
            numero = numero.replace('Y', 1);
            numero = numero.replace('Z', 2);
            letr = numDoc.value.substr(numDoc.value.length - 1, 1);
            numero = numero % 23;
            letra = 'TRWAGMYFPDXBNJZSQVHLCKET';
            letra = letra.substring(numero, numero + 1);
            if (letra != letr) {
              console.log('Dni erroneo, la letra del NIF no se corresponde');
              return {isInvalidDocIden: true};
            } else {
              console.log('Dni correcto');
              return null;
            }
          } else {
            console.log('Dni erroneo, formato no válido');
            return {isInvalidDocIden: true};
          }
        } else {
          return null;
        }
      }
    }
  }
}
