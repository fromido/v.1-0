import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import {DocumentoComponent} from "./components/documento/documento.component";

export const routes: Routes = [
  {path:':origen/home',component:HomeComponent},
  {path:':origen/documento/:id',component:DocumentoComponent},
  {path:'',pathMatch:'full',redirectTo:'pagament/home'},
  {path:'**',pathMatch:'full',redirectTo:'pagament/home'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
