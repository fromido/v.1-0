import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
//Translation
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
//routes
import { RouterModule } from "@angular/router";
import { HttpClient,HttpClientModule} from "@angular/common/http";
//rutas importadas
import {APP_ROUTING} from './app.routes';
//COMPONENTES
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { DocumentoComponent } from './components/documento/documento.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import {CardDocComponent} from './components/shared/card-doc/card-doc.component';
import {LoadingComponent} from './components/shared/loading/loading.component';
import {Mod1y2Component} from "./components/mod1y2/mod1y2.component";
import {Mod3Component} from "./components/mod3/mod3.component";
import { MessageComponent } from './components/shared/message/message.component';
import { PopupComponent } from './components/shared/popup/popup.component';
import { TranslationComponent } from './utils/translation/translation.component';
import {PagoComponent} from './components/pago/pago.component';
//PIPES
import { NoimagePipe } from './pipes/noimage.pipe';
import {DomseguroPipe} from './pipes/domseguro.pipe'

import {enableProdMode} from '@angular/core';
import {ReactiveFormsModule} from "@angular/forms";


import { MaxtextPipe } from './pipes/maxtext.pipe';
import { ImageNavPipe } from './pipes/image-nav.pipe';
import {DatePipe} from "@angular/common";
import { GetPropiedadesObjetoPipe } from './pipes/get-propiedades-objeto.pipe';

//libreria de creacion de PDF
import { PdfMakeWrapper } from 'pdfmake-wrapper';
import pdfFonts from "pdfmake/build/vfs_fonts";
import { ErrorComponent } from './components/shared/error/error.component'; // fonts provided for pdfmake

// Set the fonts to use
PdfMakeWrapper.setFonts(pdfFonts);


enableProdMode();

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DocumentoComponent,
    NavbarComponent,
    FooterComponent,
    Mod1y2Component,
    Mod3Component,
    NoimagePipe,
    DomseguroPipe,
    CardDocComponent,
    LoadingComponent,
    MessageComponent,
    TranslationComponent,
    MaxtextPipe,
    PopupComponent,
    ImageNavPipe,
    PagoComponent,
    GetPropiedadesObjetoPipe,
    GetPropiedadesObjetoPipe,
    ErrorComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTING,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => {
          return new TranslateHttpLoader(http);
        },
        deps: [ HttpClient ]
      }
    }),
    ReactiveFormsModule
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
