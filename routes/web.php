<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});*/
/*
Route::get('api/documento/{id}', 'API\DocumentoController@index');
Route::post('api/update/{id}', 'API\AyuntamientoController@update');/*
/* Ruta para devolver el control tras hacer el Pago y mostrar el Resultado */
//Route::any('pago_OKKO', 'API\AyuntamientoController@update');
//Route::get('/formulario2_smartpol', 'API\RedsysController@sendEasyCop');
//Route::post('/formulario2_smartpol', 'API\RedsysController@sendEasyCop');
Route::get('acciones', 'Controller@index');
