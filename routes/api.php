<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::any('formulario2_ventanilla','RedsysController@montarPago');
//Route::any('formulario2_smartpol','RedsysController@montarPago');

Route::group(['prefix'=>'get','middleware'=>'cors'], function(){
    Route::get('/formulario2_smartpol/{codebar}/{idAyuntamiento}/{emisora}', 'API\RedsysController@sendEasyCop');
    Route::get('/ayuntamiento/{id}', 'API\AyuntamientoController@index');
    Route::get('/documento/{id}/{idAyuntamiento}', 'API\DocumentoController@index')->where('id','[0-9]+')->where('idAyuntamiento','[0-9]+');
});

Route::group(['prefix'=>'post','middleware'=>'cors'], function(){
    Route::post('/formulario2_smartpol', 'API\RedsysController@sendEasyCop');
    Route::post('/ayuntamiento', 'API\AyuntamientoController@index');
    Route::post('/documento/{id}', 'API\DocumentoController@sendParams');
    Route::post('/justificante/{idAyuntamiento}', 'API\JustificanteController@informacionJustificante')->where('idAyuntamiento','[0-9]+');
    Route::post('/entidad', 'API\EntidadController@show');
    Route::post('/popup/form/{id}', 'API\DocumentoController@validateFormPopUp');
    /* Ruta para devolver el control tras hacer el Pago y mostrar el Resultado */
    Route::post('/notificacionRedsysok', 'API\RedsysController@update');
    Route::post('/notificacionRedsysko', 'API\RedsysController@updateNotificaciones');
    Route::post('/pago_OKKO', 'API\RedsysController@OKKO');
});

