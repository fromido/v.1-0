<?php

namespace App\Http\Requests;

use App\Models\AyuntamientosIdentificadore;
use App\Models\AyuntamientosTributo;
use App\Models\Docstpv;
use App\Models\DocstpvEmisora;
use App\Models\Modalidadesc60;
use App\Validation\Validator;
use Illuminate\Database\Eloquent\Model;

class ValidatorRequest extends Model {

    /**
     * @todo   Es una variable que indica la validadcion de la modalidad 1 con un codigo de barras de 38 digitos
     * @var array
     * @author David Salgado Diez
     */
    public static $mod1CodRules38 = [
        'codBar' => 'required|numeric|digits:38'
    ];
    /**
     * @todo   Es una variable que indica la validadcion de la modalidad 1 con un codigo de barras de 48 digitos
     * @var array
     * @author David Salgado Diez
     */
    public static $mod1CodRules48 = [
        'codBar' => 'required|numeric|digits:48'
    ];
    /**
     * @todo   Variable array modalidad 1 que guarda las validaciones de los campos que se envian sin ser un codigo de
     *         barras
     * @var array
     * @author David Salgado Diez
     */
    public static $mod1CodRules = [
        'entidad'        => 'required|numeric|digits:6',
        'referencia'     => 'required|numeric|digits:12',
        'identificacion' => 'required|numeric|digits:7',
        'importe'        => 'required|numeric|max:999999',
    ];
    /**
     * @todo   Es una variable que indica la validadcion de la modalidad 2 con un codigo de barras de 42 digitos
     * @var array
     * @author David Salgado Diez
     */
    public static $mod2CodRules42 = [
        'codBar' => 'required|numeric|digits:42'
    ];
    /**
     * @todo   Es una variable que indica la validadcion de la modalidad 2 con un codigo de barras de 44 digitos
     * @var array
     * @author David Salgado Diez
     */
    public static $mod2CodRules54 = [
        'codBar' => 'required|numeric|digits:54'
    ];
    /**
     * @todo   Variable array que guarda las validaciones de los campos que se envian sin ser un codigo de barras
     * @var array
     * @author David Salgado Diez
     */
    public static $mod2CodRules = [
        'entidad'        => 'required|numeric|digits:6',
        'referencia'     => 'required|numeric|digits:12',
        'identificacion' => 'required|numeric|digits:10',
        'importe'        => 'required|numeric|max:999999',
    ];

    /**
     * Variable array que guarda las validaciones de los campos que se envían con código de barras y discriminante 0
     * @var array
     * @author Adrian Tornero Ortiz
     */
    public static $mod3CodRules2408 = [
        'codBar' => 'required|numeric|digits:24'
    ];

    /**
     * Variable array que guarda las validaciones de los campos que se envían con código de barras y discriminante 1
     * @var array
     * @author Adrian Tornero Ortiz
     */
    public static $mod3CodRules241 = [
        'codBar'     => 'required|numeric|digits:24',
        'expediente' => 'required|numeric|digits:12'
    ];

    /**
     * Variable array que guarda las validaciones de los campos que se envían con código de barras y discriminante 2
     * @var array
     * @author Adrian Tornero Ortiz
     */
    public static $mod3CodRules242 = [
        'codBar'     => 'required|numeric|digits:24',
        'fecDevengo' => 'required|numeric|digits:8|fec_devengo',
    ];

    /**
     * Variable array que guarda las validaciones de los campos que se envían con código de barras y discriminante 3
     * @var array
     * @author Adrian Tornero Ortiz
     */
    public static $mod3CodRules243 = [
        'codBar'     => 'required|numeric|digits:24',
        'expediente' => 'required|numeric|digits:12',
        'fecDevengo' => 'required|numeric|digits:8|fec_devengo'
    ];

    /**
     * Variable array que guarda las validaciones de los campos que se envían con código de barras y discriminante 4
     * @var array
     * @author Adrian Tornero Ortiz
     */
    public static $mod3CodRules244 = [
        'codBar'         => 'required|numeric|digits:24',
        'datoEspecifico' => 'required|numeric|digits:20',
    ];

    /**
     * Variable array que guarda las validaciones de los campos que se envían con código de barras y discriminante 5
     * @var array
     * @author Adrian Tornero Ortiz
     */
    public static $mod3CodRules245 = [
        'codBar'         => 'required|numeric|digits:24',
        'expediente'     => 'required|numeric|digits:12',
        'datoEspecifico' => 'required|numeric|digits:20',
    ];

    /**
     * Variable array que guarda las validaciones de los campos que se envían con código de barras y discriminante 6
     * @var array
     * @author Adrian Tornero Ortiz
     */
    public static $mod3CodRules246 = [
        'codBar'         => 'required|numeric|digits:24',
        'fecDevengo'     => 'required|numeric|digits:8|fec_devengo',
        'datoEspecifico' => 'required|numeric|digits:20'
    ];

    /**
     * Variable array que guarda las validaciones de los campos que se envían con código de barras y discriminante 7
     * @var array
     * @author Adrian Tornero Ortiz
     */
    public static $mod3CodRules247 = [
        'codBar'         => 'required|numeric|digits:24',
        'fecDevengo'     => 'required|numeric|digits:8|fec_devengo:',
        'expediente'     => 'required|numeric|digits:12',
        'datoEspecifico' => 'required|numeric|digits:20'
    ];


    /**
     * Variable array que guarda las validaciones de los campos que se envían con número justificante y discriminante 0
     * @var array
     * @author Adrian Tornero Ortiz
     */
    public static $mod3JusRules2408 = [
        'entidad'         => 'required|numeric|digits:6',
        'numJustificante' => 'required|numeric|digits:13|num_justificante',
        'importe'         => 'required|numeric|max:999999'
        //TODO 'tipoDoc' =>
        //TODO 'numDoc' =>
    ];

    /**
     * Variable array que guarda las validaciones de los campos que se envían con número de justificante y
     * discriminante 1
     * @var array
     * @author Adrian Tornero Ortiz
     */
    public static $mod3JusRules241 = [
        'entidad'         => 'required|numeric|digits:6',
        'numJustificante' => 'required|numeric|digits:13',
        'importe'         => 'required|numeric|max:999999',
        'expediente'      => 'required|numeric|digits:12'
    ];

    /**
     * Variable array que guarda las validaciones de los campos que se envían con número de justificante y
     * discriminante 2
     * @var array
     * @author Adrian Tornero Ortiz
     */
    public static $mod3JusRules242 = [
        'entidad'         => 'required|numeric|digits:6',
        'numJustificante' => 'required|numeric|digits:13',
        'importe'         => 'required|numeric|max:999999',
        'fecDevengo'      => 'required|numeric|digits:8|fec_devengo:'
    ];

    /**
     * Variable array que guarda las validaciones de los campos que se envían con número de justificante y
     * discriminante 3
     * @var array
     * @author Adrian Tornero Ortiz
     */
    public static $mod3JusRules243 = [
        'entidad'         => 'required|numeric|digits:6',
        'numJustificante' => 'required|numeric|digits:13',
        'importe'         => 'required|numeric|max:999999',
        'expediente'      => 'required|numeric|digits:12',
        'fecDevengo'      => 'required|numeric|digits:8|fec_devengo'
    ];

    /**
     * Variable array que guarda las validaciones de los campos que se envían con número de justificante y
     * discriminante 4
     * @var array
     * @author Adrian Tornero Ortiz
     */
    public static $mod3JusRules244 = [
        'entidad'         => 'required|numeric|digits:6',
        'numJustificante' => 'required|numeric|digits:13',
        'importe'         => 'required|numeric|max:999999',
        'datoEspecifico'  => 'required|numeric|digits:20',
    ];

    /**
     * Variable array que guarda las validaciones de los campos que se envían con número de justificante y
     * discriminante 5
     * @var array
     * @author Adrian Tornero Ortiz
     */
    public static $mod3JusRules245 = [
        'entidad'         => 'required|numeric|digits:6',
        'numJustificante' => 'required|numeric|digits:13',
        'importe'         => 'required|numeric|max:999999',
        'expediente'      => 'required|numeric|digits:12',
        'datoEspecifico'  => 'required|numeric|digits:20',
    ];

    /**
     * Variable array que guarda las validaciones de los campos que se envían con número de justificante y
     * discriminante 6
     * @var array
     * @author Adrian Tornero Ortiz
     */
    public static $mod3JusRules246 = [
        'entidad'         => 'required|numeric|digits:6',
        'numJustificante' => 'required|numeric|digits:13',
        'importe'         => 'required|numeric|max:999999',
        'fecDevengo'      => 'required|numeric|digits:8|fec_devengo',
        'datoEspecifico'  => 'required|numeric|digits:20'
    ];

    /**
     * Variable array que guarda las validaciones de los campos que se envían con número de justificante y
     * discriminante 7
     * @var array
     * @author Adrian Tornero Ortiz
     */
    public static $mod3JusRules247 = [
        'entidad'         => 'required|numeric|digits:6',
        'numJustificante' => 'required|numeric|digits:13',
        'importe'         => 'required|numeric|max:999999',
        'fecDevengo'      => 'required|numeric|digits:8|fec_devengo',
        'expediente'      => 'required|numeric|digits:12',
        'datoEspecifico'  => 'required|numeric|digits:20'
    ];


    /**
     * @param $code
     * @return array|bool
     * @todo   Funcion encargada de retornor un array para validar modalidad segun la longitud del codigo de barras
     * @author David Salgado Diez
     */
    public static function codeVerificationMod1($code) {
        switch (substr($code, 0, 5)) {
            case '90502':
                return ValidatorRequest::$mod1CodRules38;
            break;
            case '90508':
                return ValidatorRequest::$mod1CodRules48;
            break;
        }

        return false;
    }

    /**
     * @param $code
     * @return array|bool
     * @todo   Funcion encargada de retornor un array para validar modalidad segun la longitud del codigo de barras
     * @author David Salgado Diez
     */
    public static function codeVerificationMod2($code) {
        switch (substr($code, 0, 5)) {
            case '90522':
                return ValidatorRequest::$mod2CodRules54;
            break;
            case '90521':
                return ValidatorRequest::$mod2CodRules42;
            break;
        }

        return false;
    }

    /**
     * @param $code
     * @return array|bool
     * Funcion encargada de retornor un array para validar los datos recibidos según código de barras o número de
     * justificante más el discriminante
     * @author Adrian Tornero Ortiz
     */
    public static function verificationMod3($array) {
        //Comprobamos si lo que recibimos es un código de barras (únicamente por la longitud)
        if (isset($array['codeBar'])) {
            $code = $array['codeBar'];
        }
        if (isset($array['numJustificante'])) {
            $code = $array['numJustificante'];
        }

        if (strlen($code) == 24) {
            switch (substr($array, 15, 1)) {
                case '0':
                    return ValidatorRequest::$mod3CodRules2408;
                break;
                case '1':
                    return ValidatorRequest::$mod3CodRules241;
                break;
                case '2':
                    ValidatorRequest::$mod3CodRules242;
                break;
                case '3':
                    return ValidatorRequest::$mod3CodRules243;
                break;
                case '4':
                    return ValidatorRequest::$mod3CodRules244;
                break;
                case '5':
                    return ValidatorRequest::$mod3CodRules245;
                break;
                case '6':
                    return ValidatorRequest::$mod3CodRules246;
                break;
                case '7':
                    return ValidatorRequest::$mod3CodRules247;
                break;
                case '8':
                    return ValidatorRequest::$mod3CodRules2408;
                break;
            }
            //Comprobamos si lo que recibimos es un número de justificante (únicamente por la longitud)
        }

        if (strlen($code) == 13) {
            switch (substr($code, 3, 1)) {
                case '0':
                    $variable = ValidatorRequest::$mod3JusRules2408;
                    $variable['numJustificante'] = $variable['numJustificante'] . $array['idDoc'];

                    return $variable;
                break;
                case '1':
                    return ValidatorRequest::$mod3JusRules241;
                break;
                case '2':
                    $variable = ValidatorRequest::$mod3JusRules242;
                    $fechaDevengo = substr($array['fecDevengo'], 8, 2) . substr($array['fecDevengo'], 5, 2) . substr(
                            $array['fecDevengo'], 0, 4
                        );
                    $variable['fecDevengo'] = $variable['fecDevengo'] . $fechaDevengo;

                    //dd($variable);
                    return ValidatorRequest::$mod3JusRules242;
                break;
                case '3':
                    return ValidatorRequest::$mod3JusRules243;
                break;
                case '4':
                    return ValidatorRequest::$mod3JusRules244;
                break;
                case '5':
                    return ValidatorRequest::$mod3JusRules245;
                break;
                case '6':
                    return ValidatorRequest::$mod3JusRules246;
                break;
                case '7':
                    return ValidatorRequest::$mod3JusRules247;
                break;
                case '8':
                    return ValidatorRequest::$mod3JusRules2408;
                break;
            }
        }

        return false;
    }

    /**
     * @param $code
     * @param $idDoc
     * @param $param
     * @param $obj
     * @return string|null
     * @todo   Funcion encargada de retornor un el valor que forma o el codigo de barras o el elemento input y verifica
     *         si esta bien formado o no
     * @author David Salgado Diez
     */
    public static function getObjectCodeBar($code, $idDoc, $param, $obj) {
        if (!$code) {
            return null;
        }
        //first por donde empieza en el string que mando y last es la logitud del string que quiero
        $_codeBar = substr($code, $obj['first'], $obj['last']);
        if (!$param) {
            return $_codeBar;
        }
        switch ($param) {
            case 'entidad':
                $documentoEmisora = DocstpvEmisora::where(
                    [
                        ['docs_id', '=', $idDoc],
                        ['emisora_id', '=', $_codeBar]
                    ]
                )->first();
                if ($documentoEmisora && $documentoEmisora != null && $documentoEmisora->emisora_id == $_codeBar) {
                    return $_codeBar;
                }
            break;
            case 'referencia':
                /**
                 * 10 posiciones 2 ultimos digitos de control
                 *
                 * las 2 ultimas en cuaderno c60 (pagina 47 , en este caso con 29,sin el 29. Tiene que dar los 2 ultimos [29])
                 */
                return $_codeBar;
            break;
            case 'identificacion':
                //compruebo porque numero empieza
                if ($idDoc == null) {
                    return $_codeBar;
                }
                switch (substr($_codeBar, 0, 1)) {
                    case '0':
                        /*este caso mod2 con 1 importe cuando empieza por 1*/
                        if (ValidatorRequest::getTributoIdentificadores($_codeBar, $idDoc)) {
                            return $_codeBar;
                        }

                        return null;
                    break;
                    case '1':
                        /*este caso mod2 con 1 importe cuando empieza por 1*/
                        if (ValidatorRequest::getTributo(substr($_codeBar, 1, 3), $idDoc)) {
                            return $_codeBar;
                        }

                        return null;
                    break;
                    case '5':
                    case '9':

                        //en caso de que empiece por 5 o 9 si son 2 importes
                    break;
                }
            break;
            case 'fecha':
                $anio = substr($_codeBar, 0, 2);
                $aniosiguiente = substr($_codeBar, 2, 1);
                $fechaJuliana = substr($_codeBar, -3);
                if ($aniosiguiente == substr($anio, 1, 1)) {
                    if (!is_null(ValidatorRequest::comprobarFechaJuliana($fechaJuliana))) {
                        return $_codeBar;
                    }
                } else {
                    $anio = "20" . $anio;
                    if (intval($anio) == intval(date("Y"))) {
                        return $_codeBar;
                    }
                    if (!is_null(ValidatorRequest::comprobarFechaJuliana($fechaJuliana))) {
                        return $_codeBar;
                    }
                }
            break;
            case 'importe':
                //[0-9]+(\.[0-9][0-9]?)?
                if (is_numeric($_codeBar)) {
                    return $_codeBar;
                }
            break;
            case 'numJustificante':
                //$_codeBar será el numJustificante
                //COMPROBAR ADRIAN
                if (ValidatorRequest::getTributo($_codeBar, $idDoc)) {
                    $objeto = array('numJustificante' => $_codeBar, 'emisora' => substr($code, 6, 11));

                    if (ValidatorRequest::comprobarDC($objeto, 3)) {
                        return $_codeBar;
                    }

                    //validar DATOS ADICIONALES

                }
            break;
        }

        return null;
    }

    ///////////////////////////////////////////////////
    // METODO PARA COMPROBAR LOS DIGITOS DE CONTROL  //
    // DEL CODIGO DE BARRAS EN MODALIDADES           //
    // 90502, 90508, 90521, 90522                    //
    // Y EL DÍGITO DE CONTROL EN NUMJUSTIFICANTE     //
    // DE LA MODALIDAD 3                             //
    ///////////////////////////////////////////////////

    /**
     * @param $objeto
     * @return bool
     */
    function comprobarDC($objeto, $modalidad) {
        $dc = '';
        $referenciaSinDC = '';
        $numJustificanteSinDC = '';

        switch ($modalidad) {
            case 1:
            case 2:
                //@todo REVISAR MOD 1 y 2
                //7 digitos

                $dc = substr($objeto['referencia'], -2);
                $referenciaSinDC = substr($objeto['referencia'], 0, -2);

                $uno = $objeto['emisora'] * 76;
                $dos = $referenciaSinDC * 9;
                $tres = (($objeto['identificacion'] + $objeto['importe']) - 1) * 55;
                $pre = ($uno + $dos + $tres) / 97;
                $pre2 = substr(strrchr($pre, '.'), 1, 2);
                $digitos_control_aux = 99 - $pre2;
                if ($digitos_control_aux == 0) {
                    $digitos_control_aux = 99;
                };
                //echo "Digitos Calc " . $digitos_control_aux;
                // Se comprueba si son iguales o no
                if ($dc == $digitos_control_aux) {
                    return true;
                }
            break;
            /*case 2:
                //10 digitos
                $dc = substr($objeto['referencia'], -2);
                $referenciaSinDC = substr($objeto['referencia'], 0, -2);
            break;*/
            case 3:
                $dc = substr($objeto['numJustificante'], -1);
                $numJustificanteSinDC = substr($objeto['numJustificante'], 0, -1);

                $uno = $objeto['emisora'];
                //Sumamos la emisora y el número de justificante
                $pre = $uno + $numJustificanteSinDC;
                //Calculamos el resto
                $pre2 = $pre % 7;
                if ($dc = $pre2) {
                    return true;
                }
            break;
        }

        return false;
    }

    function comprobarFecDevengo($fecDevengo) {
        //Comprobar que la fecha devengo es una fecha válida
        if (len($fecDevengo == 8)) {
            $dia = substr($fecDevengo, 0, 2);
            $mes = substr($fecDevengo, 2, 2);
            $anio = substr($fecDevengo, 4, 4);
            if (checkdate($mes, $dia, $anio)) {
                return true;
            };
        }

        return false;
    }

    function comprobarNumJustificante($numJustificante, $idDoc) {
        if (ValidatorRequest::getTributo(substr($numJustificante, 0, 3), $idDoc)) {
            if (ValidatorRequest::comprobarDC($numJustificante, 3)) {
                return false;
            }
        }

        return false;
    }

    /**
     * @param $fecha
     * @return bool|null
     * @todo   conpueba si la fecha juliana de hoy es mayor que la fecha juliana que se le envia
     * @author David Salgado
     */
    static function comprobarFechaJuliana($fecha) {
        if ($fecha >= date("z")) {
            return true;
        }

        return null;
    }

    /**
     * @param $data
     * @return int|string
     * @todo   funcion encargada de ver que tipo de envio hay que hacer para poder recibir una notificacion
     * @author David Salgado
     */
    static function getTipoEnvio($data) {
        if (isset($data['type'])) {
            switch ($data['type']) {
                case 'telefono':
                    return 2;
                break;
                case 'email':
                    return 1;
                break;
            }
        }

        return '';
    }

    /**
     * @param $data
     * @return string
     * @todo   funcion encargada de retornar si hay datos para poder notificar
     * @author David Salgado
     */
    static function getDataNotificacion($data, $find) {
        if (isset($data['data']) && isset($data['type'])) {
            if ($data['type'] == $find) {
                return $data['data'];
            }
        }

        return '';
    }


    /**
     * @param $fecha_inicio
     * @param $fecha_fin
     * @param $fecha
     * @return bool
     * @todo   funcion encargada de ver si una fecha esta entre otras dos fechas
     * @author David Salgado
     */
    static function check_in_range($fecha_inicio, $fecha_fin) {
        $fecha_inicio = strtotime($fecha_inicio);
        $fecha_fin = strtotime($fecha_fin);
        $fecha = strtotime(date("yy-m-d"));
        if (($fecha >= $fecha_inicio) && ($fecha <= $fecha_fin)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $codTributo
     * @param $idDoc
     * @return string|null
     * @todo   comprueba si el tributo pertenece a ese documento con ese ayuntamiento
     * @author David Salgado Diez
     */
    public static function getTributo($codTributo, $idDoc) {

        $documento = Docstpv::where('id', '=', $idDoc)->first();

        if ($documento) {
            //los siguientes 3 contrastarlos con datos de la tabla ayuntamientos_tributo (antes estaba en admin_tpv -> entidades_modc60_tributos)
            $tributo = AyuntamientosTributo::where(
                [
                    ['ayuntamiento_id', '=', $documento->ayuntamiento_id],
                    ['codigo', '=', intval($codTributo)]
                ]
            )->first();
            if ($tributo) {
                return $codTributo;
            }
        }

        return null;
    }

    /**
     * @param $codTributo
     * @param $idDoc
     * @return string|null
     * @todo   comprueba si el tributo pertenece a ese documento con ese ayuntamiento
     * @author David Salgado Diez
     */
    public static function getTributoIdentificadores($codTributo, $idDoc) {
        $documento = Docstpv::where('id', '=', $idDoc)->first();
        if ($documento) {
            //los siguientes 3 contrastarlos con datos de la tabla ayuntamientos_tributo (antes estaba en admin_tpv -> entidades_modc60_tributos)

            $tributo = AyuntamientosTributo::where(
                [
                    ['ayuntamiento_id', '=', $documento->ayuntamiento_id],
                    ['codigo', '=', intval(substr($codTributo, 0, 3))]
                ]
            )->first();
            if ($tributo) {
                $ayuntamientoIdentificador = AyuntamientosIdentificadore::where('tributo_id', '=', $tributo->id)->first(
                );
                $ejercicio = substr($codTributo, 3, 2);
                $remesa = substr($codTributo, 5, 2);
                if ($ayuntamientoIdentificador &&
                    ($ayuntamientoIdentificador->ejercicio == $ejercicio && $ayuntamientoIdentificador->remesa == $remesa) &&
                    self::check_in_range(
                        $ayuntamientoIdentificador->fechainicio, $ayuntamientoIdentificador->fechafin
                    )) {
                    return $codTributo;
                }
                //return AyuntamientosIdentificadore::where('tributo_id', '=', $tributo->id)->first();
            }
        }

        return null;
    }

    /**
     * @param      $preCodeBar
     * @param null $modalidad
     * @return string|null
     * @todo   retorna un codigo cpr dependiendo de tipo numerico que le llege
     * @author David Salgado Diez
     */
    public static function getCprct($preCodeBar, $modalidad = null) {
        switch ($preCodeBar) {
            case '90502':
                return '9050299';
            case '90508':
                return '9050299';
            case '90521':
            case '90522':
                return '9052180';
            case '90523':
                return '9052378';
        }
        if ($modalidad) {
            $mod = Modalidadesc60::where('id', $modalidad)->first();
            if ($mod) {
                return $mod->cpr;
            }

            return null;
        }

        return null;
    }

    /**
     * @param $importe
     * @return mixed
     * @todo funcion encargada de tranformar un importe para poderlo mandar por redsys
     */
    public static function importeRedsys($importe) {
        $importe = str_replace(",", "", $importe);
        $importe = str_replace(".", "", $importe);
        if (strlen($importe) < 7) {
            $string0 = '';
            for ($i = strlen($importe); $i < 7; $i++) {
                $string0 = '0' . $string0;
            }
            $importe = $string0 . $importe;
        }

        return $importe;
    }

    //////////////////////////////////////////////////////////////
    // Función para comprobar si el Tipo de Documento es válido //
    //////////////////////////////////////////////////////////////
    public function DocumentoEsValido($tipoDocumento, $documento) {
        // Si el tipo de documento o el documento están vacíos, se devuelve error
        if (($tipoDocumento == '') || ($documento == '')) {
            return "ko";
        }
        // Si la longitud es mayor que 9, se devuelve error
        if (strlen($documento) > 9) {
            return "ko";
        }
        // Si comienza por dos letras, se devuelve error
        if ((ctype_alpha(substr($documento, 0, 1))) && (ctype_alpha(substr($documento, 1, 1)))) {
            return "ko";
        }
        // Si el documento es distino de Pasaporte, si tiene letras en el interior, se devuelve error
        if ($tipoDocumento != 'P') {
            for ($i = 1; $i < strlen($documento) - 1; $i++) {
                if (ctype_alpha(substr($documento, $i, 1))) {
                    return "ko";
                }
            }
        }
        // Se estructura el Documento
        $letraInicial = '';
        $docNumeroStr = '';
        $letraFinal = '';
        //ADRI -> NECESITO ESTA FUNCIÓN
        $this->EstructurarDNI($documento, $letraInicial, $docNumeroStr, $letraFinal);
        //echo "Letra Inicial: " . $letraInicial . "<br>";
        //echo "Documento: " . $docNumeroStr . "<br>";
        //echo "Letra Final: " . $letraFinal . "<br>";

        // Por el tipo de Documento, se hacen las Comprobaciones
        // NIF
        if ($tipoDocumento == "N") {
            //echo "Entro por nif" . "<br>";
            if ((($letraInicial == "M") || ($letraInicial == "L") || ($letraInicial == "K")) && ($letraFinal == "")) {
                //echo "KO por letra inicial M, L o K sin Letra Final" . "<br>";
                return "ko";
            } elseif ((($letraInicial == "") && ($letraInicial == "L")) || ($letraFinal != $this->CalcularLetraDni(
                        $docNumeroStr
                    ))) {
                //echo "KO por letra final distinta de la Calculada" . "<br>";
                return "ko";
            } elseif ($letraInicial != "") {
                //echo "Ko por letra inicial distinca de vacia" . "<br>";
                return "ko";
            }
            //echo "Salgo de nif" . "<br>";
            // CIF
        } elseif ($tipoDocumento == "C") {
            //echo "Entro por cif" . "<br>";
            // Se comprueba la letra Inicial
            if (($letraInicial != "A") && ($letraInicial != "B") && ($letraInicial != "C") &&
                ($letraInicial != "D") && ($letraInicial != "E") && ($letraInicial != "F") &&
                ($letraInicial != "G") && ($letraInicial != "H") && ($letraInicial != "N") &&
                ($letraInicial != "P") && ($letraInicial != "Q") && ($letraInicial != "S") &&
                ($letraInicial != "J") && ($letraInicial != "U") && ($letraInicial != "V") &&
                ($letraInicial != "R") && ($letraInicial != "W")) {
                //echo "Letra distinta de las seleccionadas" . "<br>";
                return "ko";
            } elseif (cifDigito($documento) != substr($documento, strlen($documento) - 1, 1)) {
                //echo "Cif Digito Calculado distinto" . "<br>";
                return "ko";
            }
            // NIE
        } elseif ($tipoDocumento == "T") {
            // Se comprueba la Letra Inicial
            if (($letraInicial != "X") && ($letraInicial != "Y") && ($letraInicial != "Z")) {
                return "ko";
            } else {
                if ($letraFinal == "") {
                    return "ko";
                } elseif (($letraFinal == "X") && ($this->CalcularLetraDni("0" . $docNumeroStr) != $letraFinal)) {
                    return "ko";
                } elseif (($letraFinal == "Y") && ($this->CalcularLetraDni("1" . $docNumeroStr) != $letraFinal)) {
                    return "ko";
                } elseif (($letraFinal == "Z") && ($this->CalcularLetraDni("2" . $docNumeroStr) != $letraFinal)) {
                    return "ko";
                }
            }
        } elseif ($tipoDocumento == "P") {
            return "ok";
        } elseif ($tipoDocumento == "O") {
            return "ok";
        } else {
            return "ko";
        }

        //echo "Resultado: ok" . "<br>";
        return "ok";
    }

    /*public function montaCodigoBarras(
        $cprct,
        $emisora,
        $referencia,
        $identificacion,
        $importe,
        $fechaLimite,
        $entFinalista,
        &$codbar
    ) {
        //echo "Valor de Cpr: " . $cprct;
        // Se monta el Código de Barras
        //echo "Entro en montar código de Barras";
        if ($cprct == '9050299') {
            $codbar = '90508' . $entFinalista . $fechaLimite . $emisora . $referencia . $identificacion . str_pad(
                    $importe, 8, "0", STR_PAD_LEFT
                );

            return 'ok';
        } elseif ($cprct == '9052180') {
            $codbar = '90521' . $emisora . $referencia . $identificacion . str_pad(
                    $importe, 8, "0", STR_PAD_LEFT
                ) . '0';

            return 'ok';
        } elseif ($cprct == '9052378') {
            $codbar = '90523' . $emisora . $identificacion;

            return 'ok';
        }

        return 'ko';
    }*/

    /**
     * @param $boolean   {boolean}
     * @param $modalidad {number}
     * @param $value     {string | Object}
     * @todo funcion encargada de retornar un array con los paramaretros para verificar el Digito de control
     */
    //@TODO Revisar para comentar o eliminar. NO SE UTILIZA
    public function getParametrosDC($boolean, $modalidad, $value) {
        $paramsDC = array(
            'emisora'        => null,
            'referencia'     => null,
            'identificacion' => null,
            'importe'        => null,
        );
        switch ($modalidad) {
            case 1:
                if ($boolean) {
                    switch (strlen($value)) {
                        case 38:
                            $paramsDC['emisora'] = substr($value, 6, 6);
                            $paramsDC['referencia'] = substr($value, 12, 12);
                            $paramsDC['identificacion'] = substr($value, 24, 7);
                            $paramsDC['importe'] = substr($value, 31, 8);
                        break;
                        case 48:
                            $paramsDC['emisora'] = substr($value, 15, 6);
                            $paramsDC['referencia'] = substr($value, 21, 12);
                            $paramsDC['identificacion'] = substr($value, 33, 7);
                            $paramsDC['importe'] = substr($value, 40, 8);
                        break;
                    }
                } else {
                    $paramsDC['emisora'] = $value['entidad'];
                    $paramsDC['referencia'] = $value['referencia'];
                    $paramsDC['identificacion'] = $value['identificacion'];
                    $paramsDC['importe'] = str_replace(".", "", $value['importe']);
                }
            break;
            case 2:
                if ($boolean) {
                    switch (strlen($value)) {
                        case 42:
                            $paramsDC['emisora'] = substr($value, 0, 6);
                            $paramsDC['referencia'] = substr($value, 6, 12);
                            $paramsDC['identificacion'] = substr($value, 18, 10);
                            $paramsDC['importe'] = substr($value, 28, 8);
                        break;
                        case 54:
                            /*$paramsDC['emisora'] = $this->getObjectCodeBar($value, null, null, ['first' => 0, 'last' => 6]);
                            $paramsDC['referencia'] = $this->getObjectCodeBar($value, null, null, ['first' => 6, 'last' => 12]);
                            $paramsDC['identificacion'] = $this->getObjectCodeBar($value, null, null, ['first' => 18, 'last' => 10]);
                            $paramsDC['importe'] = $this->getObjectCodeBar($value, null, null, ['first' => 28, 'last' => 8]);

                            $paramsDC['emisora'] = substr($value, 0, 6);
                            $paramsDC['referencia'] = substr($value, 6, 14);
                            $paramsDC['identificacion'] = substr($value, 20, 9);
                            $paramsDC['importe'] = substr($value, 40, 8);*/
                        break;
                    }
                } else {
                    $paramsDC['emisora'] = $value['entidad'];
                    $paramsDC['referencia'] = $value['referencia'];
                    $paramsDC['identificacion'] = $value['identificacion'];
                    $paramsDC['importe'] = str_replace(".", "", $value['importe']);
                }
            break;
            case 3:
                if ($boolean) {
                } else {
                    $paramsDC['emisora'] = $value['entidad'];
                    $paramsDC['referencia'] = $value['referencia'];
                    $paramsDC['identificacion'] = $value['identificacion'];
                    $paramsDC['importe'] = str_replace(".", "", $value['importe']);
                }
            break;
        }

        return $paramsDC;
    }

    public function getDataCodeBar($data, $idDoc) {
        $paramentos = array();
        switch (strlen($data['codBar'])) {
            case 24:
                //ADRIAN Desglose codBar Modalidad 3
                $paramentos['emisora'] = $this->getObjectCodeBar(
                    $data['codBar'], $idDoc, 'entidad', ['first' => 6, 'last'=>11]
                );
                $paramentos['numJustificante']=$this->getObjectCodeBar(
                    $data['codBar'], $idDoc, 'numJustificante', ['first' => 12, 'last'=>24]
                );
                $paramentos['importe'] = $this->importeRedsys($data['importe']);

                if(isset($data['tipoDoc'])){
                    $paramentos['tipoDoc'] = $data['tipoDoc'];
                }

                if(isset($data['numDoc'])){
                    $paramentos['numDoc'] = $data['numDoc'];
                }

                if(isset($data['fecDevengo'])){
                    $paramentos['fecDevengo'] = $data['fecDevengo'];
                }

                if(isset($data['expediente'])){
                    $paramentos['expediente'] = $data['expediente'];
                }

                if(isset($data['datoespecifico'])){
                    $paramentos['datoespecifico'] = $data['datoespecifico'];
                }

            break;
            case 38:
                //le quito 5, hay que sumarle '90502'
                //33
                $paramentos['emisora'] = $this->getObjectCodeBar(
                    $data['codBar'], $idDoc, 'entidad', ['first' => 5, 'last' => 6]
                );
                $paramentos['referencia'] = $this->getObjectCodeBar(
                    $data['codBar'], $idDoc, 'referencia', ['first' => 12, 'last' => 12]
                );
                $paramentos['identificacion'] = $this->getObjectCodeBar(
                    $data['codBar'], $idDoc, 'identificacion', ['first' => 25, 'last' => 7]
                );
                $paramentos['fecha'] = "ok";
                $paramentos['importe'] = $this->getObjectCodeBar(
                    $data['codBar'], $idDoc, 'importe', ['first' => 33, 'last' => 5]
                );
            break;
            case 42:
                //90521
                $paramentos['emisora'] = $this->getObjectCodeBar(
                    $data['codBar'], $idDoc, 'entidad', ['first' => 5, 'last' => 6]
                );
                $paramentos['referencia'] = $this->getObjectCodeBar(
                    $data['codBar'], $idDoc, 'referencia', ['first' => 11, 'last' => 12]
                );
                $paramentos['identificacion'] = $this->getObjectCodeBar(
                    $data['codBar'], $idDoc, 'identificacion', ['first' => 23, 'last' => 10]
                );
                $paramentos['fecha'] = $this->getObjectCodeBar(
                    $data['codBar'], $idDoc, 'fecha', ['first' => 27, 'last' => 6]
                );
                $paramentos['importe'] = $this->getObjectCodeBar(
                    $data['codBar'], $idDoc, 'importe', ['first' => 33, 'last' => 8]
                );
            break;
            case 48:
                //formato largo 90508 modalidad 1
                $paramentos['emisora'] = $this->getObjectCodeBar(
                    $data['codBar'], $idDoc, 'entidad', ['first' => 15, 'last' => 6]
                );
                $paramentos['referencia'] = $this->getObjectCodeBar(
                    $data['codBar'], $idDoc, 'referencia', ['first' => 21, 'last' => 12]
                );
                $paramentos['identificacion'] = $this->getObjectCodeBar(
                    $data['codBar'], $idDoc, 'identificacion', ['first' => 33, 'last' => 7]
                );
                $paramentos['fecha'] = null;
                if ($paramentos['identificacion']) {
                    //cambia la fecha por fecha juliana
                    $paramentos['fecha'] = date("z");
                }
                $paramentos['importe'] = $this->getObjectCodeBar(
                    $data['codBar'], $idDoc, 'importe', ['first' => 40, 'last' => 8]
                );
                //modalidad1
            break;
            case 54:
                //modalidad 2 90522
                $paramentos['emisora'] = $this->getObjectCodeBar(
                    $data['codBar'], $idDoc, 'entidad', ['first' => 5, 'last' => 6]
                );
                $paramentos['referencia'] = $this->getObjectCodeBar(
                    $data['codBar'], $idDoc, 'referencia', ['first' => 11, 'last' => 14]
                );
                $paramentos['identificacion'] = $this->getObjectCodeBar(
                    $data['codBar'], $idDoc, 'identificacion', ['first' => 25, 'last' => 9]
                );
                //todo obtener fecha para ver que importe coger
                $paramentos['fecha'] = $this->getObjectCodeBar(
                    $paramentos['identificacion'], $idDoc, 'fecha', ['first' => 4, 'last' => 6]
                );
                $paramentos['importe'] = $this->getObjectCodeBar(
                    $data['codBar'], $idDoc, 'importe', ['first' => 34, 'last' => 8]
                );
                /**
                 * todo COMPROBAR SI LA IDENTIFICACION HAY QUE  VALIDADRLA SEGUN LOS ATRIBUTOS
                 */
                if ($paramentos['fecha']==false) {
                    $paramentos['identificacion'] = $this->getObjectCodeBar(
                        $data['codBar'], $idDoc, 'identificacion', ['first' => 42, 'last' => 4]
                    );
                    $paramentos['fecha'] = $this->getObjectCodeBar(
                        $paramentos['identificacion'], $idDoc, 'fecha', ['first' => 1, 'last' => 4]
                    );
                    $paramentos['importe'] = $this->getObjectCodeBar(
                        $data['codBar'], $idDoc, 'importe', ['first' => 46, 'last' => 8]
                    );
                }
                //modalidad2
            break;
            default:
                $paramentos['emisora']=null;
        }
        $paramentos['idDoc']= $idDoc;
        $paramentos['tipoenvio']= $this->getTipoEnvio($data);
        $paramentos['email']= $this->getDataNotificacion($data, 'email');
        $paramentos['telefono']= $this->getDataNotificacion($data, 'telefono');
        return $paramentos;

    }


}
