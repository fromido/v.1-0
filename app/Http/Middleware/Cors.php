<?php

namespace App\Http\Middleware;

use Closure;

class Cors {
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        return $next($request)
            ->header('Access-Control-Allow-Methods', ' GET, PUT, POST, DELETE, OPTIONS')
            ->header('Access-Control-Allow-Headers', 'X-Auth-Token ,content-type,Authorization,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Origin,Accept,X-Requested-With,withcredentials');
    }
}
