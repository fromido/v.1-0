<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Docstpv;
use App\Models\DocstpvEmisora;
use Illuminate\Http\Request;

class EntidadController extends Controller
{
    /**
     * Display the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $response = ['entidadNotAvailable'=> true];

        $data = $request->all();
        if((isset($data['id']) && $data['id'] != null) && (isset($data['id_documento']) && $data['id_documento'] != null)){
            //entidades -> dividido en dos ( docs_tpv y docs_tpv_emisora ), los documentos tienen emisoras asociadas y no los ayuntamientos

            $documentoEmisora = DocstpvEmisora::where([
                                                    ['docs_id', '=', $data['id_documento']],
                                                    ['emisora_id', '=', $data['id'] ]
                                    ])->first();
            if($documentoEmisora){
                $response = [null];
            };
        }

        /**
         * docstpv_emisoras tiene un id_emisora
         * con el id_emisora vamos a ayuntamientos emisora y se lista con el parametro comercio_id se lista ayuntamientos_comerciostpv
         */

        return $response;
    }
}
