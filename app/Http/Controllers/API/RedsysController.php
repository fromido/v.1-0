<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\ValidatorRequest;
use App\Libraries\RedsysAPI;
use App\models\Acciones;
use App\Models\Ayuntamiento;
use App\Models\Entornos;
use App\Models\AyuntamientosUrl;
use App\Models\Modulo;
use App\Models\Notificaciones;
use App\Models\TiposUrl;
use App\Models\Url;
use App\Models\Webservice;
use App\Redsys;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use function Composer\Autoload\includeFile;

class RedsysController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function OKKO(Request $request) {
        // Se crea Objeto
        $miObj = new RedsysAPI;
        // Se comprueban que lleguen los Parámetros
        if (!empty($_GET)) {
            //URL DE RESP. ONLINE
            // SE toman los Parámetros desde el GET
            //$version = $_GET["Ds_SignatureVersion"];
            //$datos = $_GET["Ds_MerchantParameters"];
            //$signatureRecibida = $_GET["Ds_Signature"];
            $version = $request->get("Ds_SignatureVersion");
            $datos = $request->get("Ds_MerchantParameters");
            $signatureRecibida = $request->get("Ds_Signature");

            // Se toaman los datos del MerchantData
            $decodec = $miObj->decodeMerchantParameters($datos);
            //obejto configuracion donde

            // Clave recuperada de CANALES en TEST
            //$kc = 'sq7HjrUOBfKmC576ILgskD5srU870gJ7';
            // Clave recuperada de CANALES en PRODUCCION
            $kc = 'JT+CHkxsBP2SCXnVK0lg52F+WOod+grS';

            // Se construye la Firma
            $firma = $miObj->createMerchantSignatureNotif($kc, $datos);

            $referencia = '';

            // Si la firma es igual a la Firma que se ha recibido
            if ($firma === $signatureRecibida) {
                $codigo_respuesta = (int)$miObj->getParameter('Ds_Response');
                $datos_comercio = $miObj->getParameter('Ds_MerchantData');
                echo "Respuesta: " . $codigo_respuesta;
                // Se extrae la Referencia de los Datos de Comercio
                $chorizo = explode("#", base64_decode($datos_comercio));
                //echo $chorizo[0] . " " . $chorizo[4] . " " . $chorizo[5];
                $origen = explode("origen=", $chorizo[0]);
                $codbar = explode("codbar=", $chorizo[4]);
                $idioma = explode("idioma=", $chorizo[5]);
                //echo " Codbar : " . $codbar[1];
                if (count($codbar) > 0) {
                    //echo $codbar[1];
                    $cpr = substr($codbar[1], 0, 5);
                    //echo " CPR " . $cpr;
                    if ($cpr == '90502') {
                        $referencia = substr($codbar[1], 11, 12);
                    } elseif ($cpr == '90508') {
                        $referencia = substr($codbar[1], 21, 12);
                    } elseif ($cpr == '90521') {
                        $referencia = substr($codbar[1], 11, 12);
                    } elseif ($cpr == '90522') {
                        $referencia = substr($codbar[1], 11, 12);
                    } elseif ($cpr == '90523') {
                        $referencia = substr($codbar[1], 11, 13);
                    }
                }
                // Si la Respuesta es correcta
                if ($codigo_respuesta == 0) {
                    // Operacion correcta
                    $mensaje_salida = "<div class='alert alert-success' role='alert'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span><span class='sr-only'>Ok:</span>&nbsp;&nbsp;" . trans(
                            'textos.pago_OKKO_mensaje_OK'
                        ) . "</div>";
                    $mensaje_salida_mail = "OK: " . trans('textos.pago_OKKO_mensaje_OK');
                } else {
                    echo "Entro en operacion Fallida: " . $codigo_respuesta;
                    // Si tenemos el Código de Error, lo mostramos. Si no, mostramos un KO
                    if (trans('errores_tpv' . $codigo_respuesta) != '') {
                        $mensaje_salida_mail = "ERROR: " . trans('textos.pago_OKKO_mensaje_KO') . " " . trans(
                                'errores_tpv.' . $codigo_respuesta
                            );
                        $mensaje_salida = "<div class='alert alert-danger' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span><span class='sr-only'>Error:</span>&nbsp;&nbsp;" . trans(
                                'textos.pago_OKKO_mensaje_KO'
                            ) . trans('errores_tpv.' . $codigo_respuesta) . "</div>";
                    } else {
                        $mensaje_salida_mail = "ERROR: " . trans('textos.pago_OKKO_mensaje_KO');
                        $mensaje_salida = "<div class='alert alert-danger' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span><span class='sr-only'>Error:</span>&nbsp;&nbsp;" . trans(
                                'textos.pago_OKKO_mensaje_KO'
                            ) . trans('errores_tpv.error') . "</div>";
                    }
                }

            } else {
                //echo "FIRMA KO";
                $mensaje_salida = "<div class='alert alert-danger' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span><span class='sr-only'>Error:</span>&nbsp;&nbsp;" . trans(
                        'textos.pago_OKKO_firma'
                    ) . "</div>";
                $mensaje_salida_mail = "ERROR: " . trans('textos.pago_OKKO_firma');
            }


        } else {
            $mensaje_salida = "<div class='alert alert-danger' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span><span class='sr-only'>Error:</span>&nbsp;&nbsp;" . trans(
                    'textos.pago_OKKO_mensaje_KO_Notif'
                ) . "</div>";
            $mensaje_salida_mail = "ERROR: " . trans('textos.pago_OKKO_mensaje_KO_Notif');
        }

        // Se devuelve la Vista
        return view(
            'pago_OKKO', [
                'referencia'          => $referencia,
                'mensaje_salida'      => $mensaje_salida,
                'mensaje_salida_mail' => $mensaje_salida_mail
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        //ESTA PARTE HABRA QUE MODIFICARLA PARA TRATAR CORRECTAMENTE COMO TRATAR LOS MODULOS Y SU CONFIGURACION
        //$file = fopen('log_redsysController_' . date('dmY') . '.txt', "a");

        $modulos = Modulo::where('nombre', 'TPV')->first();
        $accion = new Acciones();
        if($modulos->id){
            $accion->id_app =$modulos->id;
        }else{
            $accion->id_app = 1;
        }
        $accion->nombre_accion = 'Notificaciones Redsys';
        $accion->mensaje = 'Entramos en la llamada desde <b style="color:green">InotificacionSIS</b><br>';
        try{
            if ($request && isset($request->all()['data'])) {
                if (isset($request->all()['data'])) {
                    $notificacion = null;
                    $dataAll = $request->all()["data"];
                    /****************************/
                    /****************************/
                    /****************************/
                    //$dataAll=json_decode($dataAll,TRUE);
                    /****************************/
                    /****************************/
                    /****************************/
                    $dataMerchanData = $dataAll['Ds_MerchantData'][0];

                    $data = explode("#", base64_decode($dataMerchanData));
                    $hora_minutos = (60 * ((int)substr($dataAll['Hora'][0], 0, 2))) + (int)substr(
                            $dataAll['Hora'][0], 3, 2
                        );
                    //en este array se guardaran los datos que se le envian a redsys
                    $datosEnvioRedsys = array();
                    foreach ($data as $param) {
                        $temp_array = explode("=", $param);
                        if (isset($temp_array[0]) && isset($temp_array[1])) {
                            $datosEnvioRedsys[$temp_array[0]] = $temp_array[1];
                        }
                    }
                    $path = app_path() . "\\errores_tpv\\errores_tpv.php";
                    $errores_tpv = '';
                    include $path;
                    $idioma = '';
                    if (isset($datosEnvioRedsys['idioma'])) {
                        $idioma = $datosEnvioRedsys['idioma'];
                        switch ($datosEnvioRedsys['idioma']) {
                            case 'es':
                                $errores_tpv = $errores_tpv_es;
                            break;
                            case 'va':
                                $errores_tpv = $errores_tpv_vlc;
                            break;
                            default:
                                $errores_tpv = $errores_tpv_es;
                        }
                    }

                    $id_empresa = '';
                    if (isset($datosEnvioRedsys['empresa'])) {
                        $ayuntamiento = Ayuntamiento::where('id', $datosEnvioRedsys['empresa'])->first();
                        if($ayuntamiento){
                            $id_empresa=$ayuntamiento->id_empresa;
                            $accion->id_ayuntamiento = $ayuntamiento->id_empresa;
                        }
                    }

                    if (isset($dataAll['Ds_Response']) && (int)$dataAll['Ds_Response'][0] == 0) {
                        $accion->mensaje =  $accion->mensaje . 'Resultado del Response <b style="color:blue">'.(int)$dataAll['Ds_Response'][0].'</b><br>';
                        $mensaje_salida_mail = "OK: " . isset($errores_tpv['pago_OKKO_mensaje_OK']) ? $errores_tpv['pago_OKKO_mensaje_OK'] : '';
                        $respuesta = 'OK';
                    } else {
                        //$mensaje_salida = "<div class='alert alert-danger' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span><span class='sr-only'>Error:</span>&nbsp;&nbsp;".Lang::get('textos.pago_OKKO_mensaje_KO').Lang::get('errores_tpv.'.$codigo_respuesta)."</div>";
                        $respuesta = isset($dataAll['Ds_Response']) ? (int)$dataAll['Ds_Response'][0] : '';
                        $accion->mensaje =  $accion->mensaje . 'Resultado del Response <b style="color:red">'.$respuesta.'</b><br>';
                        $mensaje_salida_mail = "ERROR: " . isset($errores_tpv['pago_OKKO_mensaje_OK']) ? $errores_tpv['pago_OKKO_mensaje_OK'] : '' . " " . isset($errores_tpv[$respuesta]) ? $errores_tpv[$respuesta] : ' ERROR';
                        $respuesta = 'KO';
                    }

                    $tipoenvio = '';
                    if (isset($datosEnvioRedsys['tipoenvio'])) {
                        $tipoenvio = $datosEnvioRedsys['tipoenvio'];
                    }
                    $origen = '';
                    if (isset($datosEnvioRedsys['origen'])) {
                        $origen = $datosEnvioRedsys['origen'];
                    }
                    $email = '';
                    if (isset($datosEnvioRedsys['email'])) {
                        $email = $datosEnvioRedsys['email'];
                    }
                    $telefono = '';
                    if (isset($datosEnvioRedsys['telefono'])) {
                        $telefono = $datosEnvioRedsys['telefono'];
                    }

                    $fErrorInterno = false;

                    //todo CAMBIAR NOMBRE DE VARIABLE A UNA CON MAS SENTIDO
                    $datos = Notificaciones::getNotificaciones($datosEnvioRedsys);

                    if($datos == false){
                        $accion->status = 'KO';
                        $accion->mensaje =  $accion->mensaje . 'ERROR el documento ya ha sido <b style="color:red">COBRADO</b><br>';
                        $accion->save();
                        return  'KO';
                    }

                    if(isset($datosEnvioRedsys['origen'])){
                        switch ($datosEnvioRedsys['origen']) {
                            case 'formulario2_ventanilla':
                                $accion->mensaje =  $accion->mensaje . 'Entra dentro de <b style="color:blue">formulario Ventanilla</b><br>';
                                //https://sanantoniodebenageber.gestiominicipal.es/formulario2_ventanilla?CODBAR='codigodebarras'&ID=idpagoonline
                                $response = Webservice::pagosOnline($id_empresa,$dataAll,$datosEnvioRedsys,$datos);
                                if($response == 'KO'){
                                    $accion->mensaje =  $accion->mensaje . 'ERROR en <b style="color:blue">pagos online</b><br>';
                                    $accion->status = $response;
                                    $accion->save();
                                    return $response;
                                }
                                $accion->mensaje =  $accion->mensaje . 'Accion realizada en el webservice <b style="color:blue">pagosOnline</b><br>';
                            break;
                            case 'tributaria':
                                $accion->mensaje =  $accion->mensaje . 'Entra dentro de  <b style="color:blue">Tributaria</b><br>';
                                $response = Webservice::paralizaCuota($id_empresa,$datosEnvioRedsys,$datos);
                                if($response == 'KO'){
                                    $accion->mensaje =  $accion->mensaje . 'ERROR en <b style="color:blue">paralizar cueta en</b><br>';
                                    $accion->status = $response;
                                    $accion->save();
                                    return $response;
                                }
                                $accion->mensaje =  $accion->mensaje . 'Accion realizada en el webservice <b style="color:blue">paralizaCuota</b><br>';
                            break;
                            default:
                                $accion->mensaje =  $accion->mensaje . 'El origen de la peticion es  <b style="color:blue">'.$datosEnvioRedsys['origen'].'</b><br>';
                                break;
                        }
                    }
                    //fwrite($file, "datosEnvioRedsys: " . json_encode($datosEnvioRedsys) . PHP_EOL);
                    try {
                        $notificacion = new Notificaciones();
                        $notificacion->codigo_ayuntamiento = isset( $datosEnvioRedsys['empresa']) ?  $datosEnvioRedsys['empresa'] : '';
                        $notificacion->fecha = isset($dataAll['Fecha'][0]) ?  $dataAll['Fecha'][0] : '';
                        $notificacion->hora = $hora_minutos;
                        $notificacion->importe = isset($dataAll['Ds_Amount'][0]) ?  $dataAll['Ds_Amount'][0] : '';
                        $notificacion->moneda = isset($dataAll['Ds_Currency'][0]) ?  $dataAll['Ds_Currency'][0] : '';
                        $notificacion->numero_pedido = isset($dataAll['Ds_Order'][0]) ?  $dataAll['Ds_Order'][0] : '';
                        $notificacion->codigo_fuc = isset($dataAll['Ds_MerchantCode'][0]) ?  $dataAll['Ds_MerchantCode'][0] : '';
                        $notificacion->terminal = isset($dataAll['Ds_Terminal'][0]) ?  $dataAll['Ds_Terminal'][0] : '';
                        $notificacion->tipo_tarjeta = '';
                        $notificacion->codigo_respuesta = isset($dataAll['Ds_Response'][0]) ?  $dataAll['Ds_Response'][0] : '';
                        $notificacion->datos_comercio = isset($dataAll['Ds_MerchantData'][0]) ?  $dataAll['Ds_MerchantData'][0] : '';
                        $notificacion->pago_seguro = isset($dataAll['Ds_SecurePayment'][0]) ?  $dataAll['Ds_SecurePayment'][0] : '';
                        $notificacion->tipo_operacion = isset($dataAll['Ds_TransactionType'][0]) ?  $dataAll['Ds_TransactionType'][0] : '';
                        $notificacion->pais = isset($dataAll['Ds_Card_Country'][0]) ?  $dataAll['Ds_Card_Country'][0] : '';
                        $notificacion->codigo_autorizacion = isset($dataAll['Ds_AuthorisationCode'][0]) ?  $dataAll['Ds_AuthorisationCode'][0] : '';
                        $notificacion->idioma = isset($dataAll['Ds_ConsumerLanguage'][0]) ?  $dataAll['Ds_ConsumerLanguage'][0] : '';
                        $dataRefNumJus = '';
                        if(isset($datosEnvioRedsys['referencia'])){
                            $dataRefNumJus= $datosEnvioRedsys['referencia'];
                        }
                        if(isset($datosEnvioRedsys['numJustificante'])){
                            $dataRefNumJus= $datosEnvioRedsys['numJustificante'];
                        }
                        $notificacion->referencia =  $dataRefNumJus;
                        $notificacion->identificacion = isset($datosEnvioRedsys['identificacion']) ?  $datosEnvioRedsys['identificacion'] : '';
                        $notificacion->emisora = isset($datosEnvioRedsys['emisora']) ?  $datosEnvioRedsys['emisora'] : '';
                        if(isset($datosEnvioRedsys['codbar'])){
                            $notificacion->barcode = $datosEnvioRedsys['codbar'];
                            $notificacion->referencia = $this->ReferenciaDesdeCodbar($datosEnvioRedsys['codbar']);
                            $notificacion->emisora = $this->EmisoraDesdeCodbar($datosEnvioRedsys['codbar']);
                            $notificacion->identificacion = $this->identificacionDesdeCodbar($datosEnvioRedsys['codbar']);
                        }

                        $notificacion->save();
                        $accion->mensaje =  $accion->mensaje . '<b style="color:blue">Inserta</b> notificacion<br>';
                        $respuesta = 'OK';
                        $accion->status = $respuesta;
                        $accion->save();
                    } catch (Exception $exc) {
                        $accion->mensaje =  $accion->mensaje . '<b style="color:red">ERROR en la insercion en notificaciones</b><br>';
                        $respuesta = 'KO';
                        $accion->status = $respuesta;
                        $accion->save();
                        $mensaje_salida_mail = "ERROR: " . $errores_tpv['pago_OKKO_mensaje_KO'];
                    } catch (Throwable $er) {
                        $accion->mensaje =  $accion->mensaje . '<b style="color:red">ERROR en la insercion en notificaciones</b><br>';
                        $respuesta = 'KO';
                        $accion->status = $respuesta;
                        $accion->save();
                        $mensaje_salida_mail = "ERROR: " . $errores_tpv['pago_OKKO_mensaje_KO'];
                    } catch (\Illuminate\Database\QueryException $e) {
                        $mensaje_salida_mail = strval($e->errorInfo[count($e->errorInfo)-1]);
                        $respuesta = 'KO';
                        $accion->status = $respuesta;
                        $accion->mensaje =  $accion->mensaje . 'ERROR en la insercion en notificaciones<b style="color:red">'.$mensaje_salida_mail.'</b><br>';
                        $accion->save();
                    }
                }else{
                    $accion->mensaje =  $accion->mensaje . 'No existe <b style="color:red"> data en el Request</b><br>';
                    $respuesta='KO';
                    $accion->status = $respuesta;
                    $accion->save();
                }
            }else{
                $respuesta='KO';
                $accion->status = $respuesta;
                $accion->save();
            }
        } catch (Exception $exc) {
            $accion->mensaje =  $accion->mensaje . 'No existe <b style="color:red"> '.json_encode($exc).'</b><br>';
            $respuesta='KO';
            $accion->status = $respuesta;
            $accion->save();
        }
        return $respuesta;
    }

    public function updateNotificaciones(Request $request) {
        //ESTA PARTE HABRA QUE MODIFICARLA PARA TRATAR CORRECTAMENTE COMO TRATAR LOS MODULOS Y SU CONFIGURACION
        $modulos = Modulo::where('nombre', 'TPV')->first();
        $accion = new Acciones();
        if($modulos->id){
            $accion->id_app =$modulos->id;
        }
        $accion->nombre_accion = 'Notificaciones Redsys';
        $accion->mensaje = 'Entramos en la llamada desde <b style="color:green">InotificacionSIS</b><br>';
        $accion->mensaje =  $accion->mensaje . '<b style="color:red">No coinciden las claves enviadas para validar la notificacion</b><br>';
        if ($request) {
            $dataAll = $request->all();
            if (isset($dataAll['data'])) {
                $dataAll = json_decode($dataAll['data']);
                try {
                    $notificacion = new Notificaciones();
                    $notificacion->fecha = isset($dataAll->fecha) ?  $dataAll->fecha : '';
                    $notificacion->datos_comercio = isset($dataAll->datos_comercio) ?  $dataAll->datos_comercio : '';
                    $notificacion->save();
                    $respuesta = 'OK';
                    $accion->mensaje =   $accion->mensaje . '<b style="color:green">Inserta</b> notificacion<br>';
                    $accion->status = $respuesta;
                    $accion->save();
                    return true;
                } catch (Exception $exc) {
                    $mensaje_salida_mail = "ERROR  ".$exc;
                    $respuesta = 'KO';
                    $accion->mensaje =   $accion->mensaje . '<b style="color:red">Error en la insercion </b>de notificacion<br>';
                    $accion->status = $respuesta;
                    $accion->save();
                } catch (Throwable $er) {
                    $mensaje_salida_mail = "ERROR: ".$er;
                    $respuesta = 'KO';
                    $accion->mensaje =   $accion->mensaje . '<b style="color:red">Error en la insercion </b>de notificacion<br>';
                    $accion->status = $respuesta;
                    $accion->save();
                } catch (\Illuminate\Database\QueryException $e) {
                    $mensaje_salida_mail = strval($e->errorInfo[count($e->errorInfo)-1]);
                    $respuesta = 'KO';
                    $accion->mensaje =   $accion->mensaje . '<b style="color:red">Error en la insercion </b>de notificacion<br>';
                    $accion->status = $respuesta;
                    $accion->save();
                }

            }
        }
        return false;
    }

    //////////////////////////////////////////////////////
    /// Método para formar el pago de RedSYS           ///
    //////////////////////////////////////////////////////
    public function montarPago(Request $request) {
        // SE recogen por GET los dos parámetros
        if ((isset($_GET['CODBAR'])) && (isset($_GET['ID']))) {
            // Se guardan los Parámetros
            $codbar = $request->get('CODBAR');
            $id_online = $request->get('ID');
            // Se llama a la función cargar los Datos del Pago
            $version = '';
            $params = '';
            $signature = '';
            $ruta_tpv = '';
            if ($this->cargarPago($id_online, $codbar, $version, $params, $signature, $ruta_tpv) == 'ok') {
                // Se muestra el Formulario la primera vez
                return view('formulario2_ventanilla', ['version' => $version, 'params' => $params, 'signature' => $signature, 'ruta_tpv' => $ruta_tpv]);
            }
            else {
                return view('formulario2_ventanilla', ['version' => '', 'params' => '', 'signature' => '', 'ruta_tpv' => ''])
                    ->withErrors(array('error' => trans('textos.errores_ventanilla_param_redsys')));
            }
        } else {
            return view('formulario2_ventanilla', ['version' => '', 'params' => '', 'signature' => '', 'ruta_tpv' => ''])
                ->withErrors(array('error' => trans('textos.errores_ventanilla_param')));
        }
    }


    ///////////////////////////////////////////////////////////////////
    // Función para obtener la Referencia desde el Código de Barras  //
    ///////////////////////////////////////////////////////////////////
    function ReferenciaDesdeCodbar($codbar_aux) {
        $cpr = substr($codbar_aux, 0, 5);
        switch ($cpr) {
            case '90502':
            case '90521':
            case '90522':
                return substr($codbar_aux, 11, 12);
            break;
            case '90508':
                return substr($codbar_aux, 21, 12);
            break;
            case '90523':
                return substr($codbar_aux, 11, 13);
            break;
        }

        return '';
    }
    ///////////////////////////////////////////////////////////////////
    // Función para obtener la Emisora desde el Código de Barras     //
    ///////////////////////////////////////////////////////////////////
    function EmisoraDesdeCodbar($codbar_aux) {
        $cpr = substr($codbar_aux, 0, 5);
        switch ($cpr) {
            case '90502':
            case '90521':
            case '90522':
            case '90523':
                return substr($codbar_aux, 5, 6);
            break;
            case '90508':
                return substr($codbar_aux, 15, 6);
            break;

        }

        return '';
    }
    ///////////////////////////////////////////////////////////////////
    // Función para obtener la identificaicion desde el Código de Barras  //
    ///////////////////////////////////////////////////////////////////
    function identificacionDesdeCodbar($codbar_aux) {
        $cpr = substr($codbar_aux, 0, 5);
        switch ($cpr) {
            case '90502'://modalidad 1
                return substr($codbar_aux, 24, 7);
            break;
            case '90521'://modalidad 2
                return substr($codbar_aux, 24, 10);
            break;
            case '90523'://modalidad 3
                return substr($codbar_aux, 12, 13);
            break;
            case '90508'://modalidad 1
                return substr($codbar_aux, 34, 7);
            break;
            case '90522'://modalidad 2 con recargo
                return substr($codbar_aux, 12, 13);
            break;
        }
        return '';
    }
    public function sendEasyCop($codebar,$idAyuntamiento,$emisora) {
        if(isset($codebar) && isset($idAyuntamiento) && isset($emisora)){

            $entorno = Entornos::where('nombre', env('APP_ENTORNO'))->first();
            $entorno->getData('TPV-Generico');

            $data['codBar']=$codebar;
            $validatorRequest = new ValidatorRequest();
            $accion = new Acciones();
            if($entorno->app){
                $accion->id_app =$entorno->app->id;
            }else{
                $accion->id_app = 1;
            }
            $accion->nombre_accion = 'Pago a traves de EasyCop';
            $accion->mensaje ='Asigna valores que se <b style="color:green">envian</b><br>';

            $accion->id_ayuntamiento = $idAyuntamiento;
            $origen = 'pagament';
            $redsys = new Redsys();

            $paramentos = $validatorRequest->getDataCodeBar($data,null);
            $paramentos['codbar']=$codebar;
            $paramentos['idioma']='es';
            $paramentos['emisora']=$emisora;
            $paramentos['origen']=$origen;
            $paramentos['empresa']= $idAyuntamiento;

            $accion->mensaje =$accion->mensaje. 'Se crea una array de <b style="color:green">parametros</b> y se asigna un objeto <b style="color:green">Redsys</b><br>';


            $notificacion = Notificaciones::where('barcode',$codebar)->first();
            if($notificacion){
                $accion->mensaje =$accion->mensaje. 'El codigo de barras ya ha sido  <b style="color:red">PAGADO</b><br>';
                $accion->status = 'KO';
                $accion->save();
                return response()->json(
                    [
                        'message' => 'Este pago ya ha sido efectuado. Contacte con su administración.',
                        'data'    => null,
                        'status'  => 500
                    ], 200
                );
            }
            $accion->mensaje =$accion->mensaje. 'El codigo de barras NO ya ha sido  <b style="color:green">PAGADO</b><br>';
            $accion->status = 'ok';
            $accion->save();

            $codificacion = $redsys->codificacionDatos($paramentos);
            $accion->mensaje =$accion->mensaje. 'Se codifican los y se envian parametros <b style="color:green">parametros</b><br>';
            if ($codificacion != '') {
                $accion->mensaje =$accion->mensaje. 'Envio a <b style="color:green">Redsys</b><br>';
                $accion->status = 'ok';
                $accion->save();
                $objPost = $redsys->enviarPago($codificacion, $paramentos);
                if(!$objPost){
                    return $objPost;
                }
                $objPost['status'] = 200;
                return view('sendform',
                    [
                        'url' => $objPost['ruta_tpv'],
                        'Ds_SignatureVersion' => $objPost['Ds_SignatureVersion'],
                        'Ds_MerchantParameters' => $objPost['Ds_MerchantParameters'],
                        'Ds_Signature' => $objPost['Ds_Signature']
                    ]);
            }
        }
    }
}
