<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Apps;
use App\Models\DocstpvEntidades;
use App\Models\Entornos;
use App\Models\Imagene;
use App\Models\Modulo;
use App\Models\ModulosConfiguracion;
use App\Models\TiposImagene;
use App\Models\Url;
use Illuminate\Http\Request;
use App\Models\Ayuntamiento;
use App\Models\EntidadesBancaria;

class AyuntamientoController extends Controller {
    /**
     * @param Request $request
     * @return array
     * @uses   Funcion que obtiene a partir de la url el tpv correspondiente.
     * @author dsalgado@grupo-mcg.es
     */
    public function index(Request $request) {
        $ayun = array();
        $dataUrl = $request->all();
        //obtenemos la url con la que entramos a la paguina
        //en el caso de accede desde  http://sanantoniodebenageber.gestionmunicipal.es/pagament/documento/69
        //solamente cogera http://sanantoniodebenageber.gestionmunicipal.es
        if (!isset($dataUrl['url'])) {
            return $ayun;
        }
        $url_ayun = Url::where('url', 'like', '%' . $dataUrl['url'] . '%')->first();
        //en el caso de no encontrar la url retorna un array vacio
        if (!$url_ayun) {
            return $ayun;
        }
        //a traves de la url del ayuntamiento obtenemos el ayuntamiento
        $ayuntamientos = $url_ayun->ayuntamientos;
        if (!$ayuntamientos) {
            return $ayun;
        }
        //cogemos el primer ayuntamiento que tenemos. Casi con total seguridad solamente devolvera un ayuntamiento
        //lo forzamos a 0 para curarnos en salud
        $ayuntamiento = $ayuntamientos[0];

        if ($ayuntamiento) {
            //obtenemos todas las rutas de las imagenes de los ayuntamientos
            $ayuntamiento->ayuntamientos_imgs;
            //obtenemos el tipo de imagen
            $tipoImagen = TiposImagene::where('descripcion', 'Escudo')->first();
            //obtenemos las propiedades del tpv ( url, tipo de aplicacion)
            $modulos = Modulo::where('nombre', 'TPV')->first();
            $imgRutaGenerica = [];
            foreach ($ayuntamiento->ayuntamientos_imgs as $i) {
                $imagen = Imagene::where('id', '=', $i->imagen_id)->where('tipoimagen_id', '=', $tipoImagen->id)->first(
                );
                if ($imagen) {
                    $imgRutaGenerica[$imagen->resolucion] = $modulos->modulos_configuracion->rutaraiz . '' . $modulos->modulos_configuracion->rutaimagenes . '' . $imagen->ruta;
                    /*$imgRutaGenerica = $modulos->modulos_configuracion->rutaraiz . '' . $modulos->modulos_configuracion->rutaimagenes . '' . end($imagenesArray);*/
                }
            }
            if ($ayuntamiento) {
                //obtenemos los idiomas del ayuntamiento
                $languages = $ayuntamiento->idiomas;
                foreach ($languages as $l) {
                    //seteamos los lenguajes para dejarlos limpios de propiedades de laravel
                    if (isset($l->pivot)) {
                        $l->ayuntamiento_id = $l->pivot->ayuntamiento_id;
                        $l->idioma_id = $l->pivot->idioma_id;
                        $l->idiomadefecto = $l->pivot->idiomadefecto;
                        unset($l->pivot);
                    }
                }
                //formamos el objeto ayuntamiento con los campos que nosotros queremos
                foreach ($languages as $language) {
                    if (!array_key_exists('id', $ayun)) {
                        $ayun['id'] = $ayuntamiento->id;
                        $ayun['email'] = $ayuntamiento->email;
                        $ayun['id_empresa'] = $ayuntamiento->id_empresa;
                        $ayun['id_ente'] = $ayuntamiento->id_ente;
                        $ayun['url_pago'] = $ayuntamiento->urlpago;
                        $ayun['codtributoliq'] = $ayuntamiento->codtributoliq;
                        $ayun['emisoraliq'] = $ayuntamiento->emisoraliq;
                        $ayun['ordinal'] = $ayuntamiento->ordinal;
                        $ayun['grupoapuntes'] = $ayuntamiento->grupoapuntes;
                        $ayun['diasmod2'] = $ayuntamiento->diasmod2;
                        $ayun['fecanu'] = $ayuntamiento->fecanu;
                        $ayun['tipodocautoliq'] = $ayuntamiento->tipodocautoliq;
                        $ayun['autoliqareca'] = $ayuntamiento->autoliqareca;
                        $ayun['tieneOVT'] = $ayuntamiento->tieneOVT;
                        $ayun['permiteCobroManual'] = $ayuntamiento->permiteCobroManual;
                        $ayun['img'] = $imgRutaGenerica;
                    }
                    if ($language->idiomadefecto == 1) {
                        $ayun['idiomadefecto'] = $language->nombre;
                    }
                    //seguimos seteando nuestro objeto ayuntamiento
                    //lo tratamos para nuestra aplicacion en concreto
                    $ayun['idioma'][$language->nombre] = $language;
                    //obtenemos los documentos que estan ligados a ese ayuntamiento
                    $docs_temp = $ayuntamiento->idiomas_documentos($language->id);
                    //array temporal que solo sirve para meter documentos que tienen modalidad
                    $array_push_temp=array();
                    foreach ($docs_temp as $key => $d) {
                        if (!is_null($d)) {
                            if ($d->pivot->pivotParent->modalidad_id != null) {
                                //estructuramos esos documentos
                                $d->estructurar($d);
                                $d->entidades = $d->entidadesBancarias($d);
                                array_push($array_push_temp,$d);
                            }
                        }
                    }
                    //seguimos metiendo documentos por idioma al ayuntamiento
                    $ayun['docs'][$language->nombre] = $array_push_temp;
                    $ayun['nif'] = $ayuntamiento->nif;
                    if ($language->nombre == 'es') {
                        $ayun['idioma'][$language->nombre]['nombre_ayuntamiento'] = $ayuntamiento->nombreES;
                    } else {
                        $ayun['idioma'][$language->nombre]['nombre_ayuntamiento'] = $ayuntamiento->nombre;
                    }
                    $ayun["direccion_ayuntamiento"] = $ayuntamiento->direccion;
                    //------ CAMBIAR FORMA DE OBTENER LOS PARAMETROS SEGUN EL IDIOMA
                    if ($language->nombre == 'es') {
                        $ayun["nombre"] = $ayuntamiento->nombreES;
                    }
                }
            }
        }
        //retorna el ayuntamiento seteado
        return $ayun;
    }
}
