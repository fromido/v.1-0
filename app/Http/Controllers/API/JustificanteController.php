<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Notificaciones;
use Illuminate\Http\Request;

class JustificanteController extends Controller
{
    public function informacionJustificante($idAyuntamiento, Request $request) {
        $data = $request->all();
        $response = [
            'message' => 'Faltan datos a enviar',
            'data'    => null,
            'status'  => 500
        ];
        if((!isset($data['dsSignatureVersion']) &&  !isset($data['dsMerchantParameters']) && !isset($data['dsSignature'])) ||  $idAyuntamiento == null){
            return response()->json(
                $response, 200
            );
        }
        $dsSignature = $data['dsSignature'];
        $dsSignatureVersion = $data['dsSignatureVersion'];
        $dsMerchantParameters = $data['dsMerchantParameters'];
        $dataSend = false;

        if(isset($dsMerchantParameters['Ds_MerchantData'])){
            $dataSend = explode("#", base64_decode($dsMerchantParameters['Ds_MerchantData']));
        }

        if(!$dataSend){
            return response()->json(
                $response, 200
            );
        }

        $datosEnvioRedsys = array();
        $campoArray=null;
        $campoTabla = null;
        foreach ($dataSend as $param) {
            $temp_array = explode("=", $param);
            if (isset($temp_array[0]) && isset($temp_array[1])) {
                if(($temp_array[0] == 'codbar' || $temp_array[0] == 'referencia' || $temp_array[0] == 'numJustificante') && $campoArray == null){
                    $campoArray = $temp_array[0];
                    switch ($temp_array[0]){
                        case 'codbar':
                            $campoTabla = "barcode";
                        break;
                        case 'referencia':case 'numJustificante':
                            $campoTabla = "referencia";
                        break;
                    }
                }
                $datosEnvioRedsys[$temp_array[0]] = $temp_array[1];
            }
        }

        if($campoArray && $campoTabla){
            $notificacion = Notificaciones::where($campoTabla,$datosEnvioRedsys[$campoArray])->first();
            if($notificacion && $notificacion->tipo_operacion == 0){
                $datosEnvioRedsys['fecha'] = $notificacion->fecha;
                return  response()->json(['message' => 'Todo correcto', 'data'    => $datosEnvioRedsys, 'status'  => 200], 200);
            }
        }
        return  response()->json(['message' => 'Datos enviados erroneos', 'data'    => null, 'status'  => 500], 200);
    }
}
