<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\ValidatorRequest;
use App\Models\Ayuntamiento;
use App\Models\AyuntamientosUrl;
use App\Models\Docstpv;
use App\Models\Notificaciones;
use App\Models\TiposUrl;
use App\Models\Url;
use App\Models\DocstpvEmisora;
use App\Redsys;
use Illuminate\Support\Facades\Validator;
use \Illuminate\Http\Response;
use Illuminate\Http\Request;

class DocumentoController extends Controller {

    /**
     * @param $id
     * @param $idAyuntamiento
     * @function funcion encargada de retornar un todos los datas de un documento segun el ayuntamiento
     * @return array|bool
     */
    public function index($id, $idAyuntamiento) {
        //obtiene el ayuntamiento a partir de id del ayuntamiento
        $ayuntamiento = Ayuntamiento::where('id', $idAyuntamiento)->first();
        $documento = false;
        if ($ayuntamiento) {
            $documento = $ayuntamiento->docstpvs_by_id($id)->first();
            //$documento = Docstpv::where('id', $id)->first();
            if (!$documento) {
                return null;
            }
            $documento->modalidadesc60;
            $documento->docstpv_emisoras;

            $temp_data = array();
            //retornar solamente el idioma que se requiere
            foreach ($documento->idiomas as $d) {
                //$d->estructurar($documento->languages($d->id));
                //$d->estructurar($d);
                $temp_data[$d->nombre] = $d->estructurar($documento->languages($d->id));
                //array_push($temp_data, $d->estructurar($documento->languages($d->id)));
            }
            unset($documento->idiomas);
            $documento->idiomas = $temp_data;
        }

        return $documento;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function sendParams($idDoc, Request $request) {
        //$validatorRequest = new ValidatorRequest();
        //$data = $request->all();
        //$data+=['idDoc'=>$idDoc];
        //dd($data);
        //$validator = Validator::make($data,$validatorRequest->verificationMod3($data));
        //dd($validator);

        //creamos la variable de respuesta si ha ocurrido algun error y que tipo de error
        $response = response()->json(
            [
                'message' => 'Datos enviados erroneos',
                'data'    => null,
                'status'  => 500
            ], 200
        );
        $flagCod = false;
        if ($idDoc && $request->all()) {
            //buscamos el documento que hacemos POST
            $documento = Docstpv::where('id', $idDoc)->first();
            //creo una variable con el objeto ValidatorRequest para tener todas las variables y funciones de ese Request en el DocumentosController
            $validatorRequest = new ValidatorRequest();
            //CUANDO ES CODIGO DE BARRAS SOLAMENTE
            /**
             * array:2 [
           e  * "cbCodBar" => true
             * "codBar" => "905021111111111111111111111111111111111123456789078968"
             * "idAyuntamiento" => 52
             * ]
             */
            //CUANDO SE ENVIAN EL RESTO DE DATOS DEL FORMULARIO
            /*
             array:5 [
                    "cbCodBar" => false
                    "entidad" => "465515"
                    "referencia" => "123412412431"
                    "identificacion" => "1234123423"
                    "importe" => "99.99"
                    "idAyuntamiento" => 52
                    ]
             */
            $data = $request->all();
            //inicializamos el validador
            $validator = null;
            $email = '';
            //comprobamos si a marcago el codigotiene 54 modalidad 2 de barras o no
            $flagCod = (array_key_exists('cbCodBar', $data)) ? $data['cbCodBar'] : false;
            //en esta variable introduciremos (si viene codigo de barras) el que le toca por modalidad
            $preCodeBar = '';
            $cprct = '';

            $idioma = $data['idioma'];
            $origen = $data['origen'];
            //este primer switch servira para validar con las validaciones de laravel segun el ValidatorRequest
            switch ($documento->modalidad_id) {
                case 1:
                    if ($flagCod && $flagCod == 'true') {
                        $validator = Validator::make($data, $validatorRequest->codeVerificationMod1($data['codBar']));
                        //delete lo 5 primeros numeros
                        $preCodeBar = substr($data['codBar'], 0, 5);
                        $cprct = $validatorRequest->getCprct($preCodeBar);
                        /*$paramentosDC = $validatorRequest->getParametrosDC(
                            true, $documento->modalidad_id, $data['codBar']
                        );*/
                    } else {
                        //eliminar esta eliminacion de la coma antes de pasar por el validator
                        $data['importe'] = str_replace(",", "", $data['importe']);
                        //$paramentosDC = $validatorRequest->getParametrosDC(false, $documento->modalidad_id, $data);
                        $validator = Validator::make($data, $validatorRequest::$mod1CodRules);
                    }
                    if ($cprct == '') {
                        $cprct = $validatorRequest->getCprct(null, 1);
                    }
                    //variable con los datos del request
                break;
                case 2:
                    //Validator::make($data, $rules);
                    if ($flagCod && $flagCod == 'true') {
                        $validator = Validator::make($data, $validatorRequest->codeVerificationMod2($data['codBar']));
                        //delete 1 numero
                        if (substr($data['codBar'], 0, 5) == '90521' || substr($data['codBar'], 0, 5) == '90522') {
                            $preCodeBar = substr($data['codBar'], 0, 5);
                        } else {
                            $preCodeBar = substr($data['codBar'], 0, 1);
                        }
                        $cprct = $validatorRequest->getCprct($preCodeBar);
                    } else {
                        //eliminar esta eliminacion de la coma antes de pasar por el validator
                        $data['importe'] = str_replace(",", "", $data['importe']);
                        $validator = Validator::make($data, $validatorRequest::$mod2CodRules);
                    }
                    if ($cprct == '') {
                        $cprct = $validatorRequest->getCprct(null, 2);
                    }
                    //variable con los datos del request
                break;
                case 3:
                    $data+=['idDoc'=>$idDoc];
                    if ($flagCod && $flagCod == 'true'){
                            $validator = Validator::make($data,$validatorRequest->verificationMod3($data));
                            //ADRI - ENTIENDO QUE AQUÍ TENDRÉ QUE VALIDAR, EN FUNCIÓN DEL CÓDIGO DISCRIMINANTE, MEDIANTE LLAMADA A FUNCIÓN GETOBJECTCODEBAR
                    } else {
                        $validator = Validator::make($data,$validatorRequest->verificationMod3($data));
                        $data['codBar'] = $data['numJustificante'];
                    }
                    if ($cprct == '') {
                        $cprct = $validatorRequest->getCprct(null, 3);
                    }
                break;
            }
            $paramentos = null;
            if ($validator && !$validator->fails()) {
                //con este if obtendremos los datos de emisora, referencia ,identificacion e importe si nos lo mandan a traves de un codeBar
                //parametros para comprobar el digito de control
                /*esta variable nos sirve para saber si un pago se ha realizado o no */
                $notificacion = null;
                if ($flagCod && $flagCod == 'true') {
                    $paramentos = $validatorRequest->getDataCodeBar($data,$idDoc);

                    $paramentos['codbar']=$data['codBar'];
                    $paramentos['idioma']=$idioma;
                    $paramentos['precodebar']=$preCodeBar;
                    $paramentos['origen']=$origen;
                    $paramentos['empresa']= $documento->ayuntamiento_id;

                    $notificacion = Notificaciones::where('barcode',$paramentos['codbar'])->first();
                } else {
                    switch ($documento->modalidad_id) {
                        case 1:
                        case 2:
                            //insertar el campo codbar , habra que formarlo con los campos que se introducen
                            $paramentos = [
                                'precodebar'     => '',
                                'idioma'         => $idioma,
                                'emisora'        => $data['entidad'],
                                'referencia'     => $data['referencia'],
                                'identificacion' => $data['identificacion'],
                                'fecha'          => $validatorRequest->getObjectCodeBar($data['identificacion'], $idDoc, 'fecha', ['first' => 4, 'last' => 6]),
                                'importe'        => $validatorRequest->importeRedsys($data['importe']),
                                'origen'         => $origen,
                                'tipoenvio'      => $validatorRequest->getTipoEnvio($data),
                                'email'          => $validatorRequest->getDataNotificacion($data, 'email'),
                                'telefono'       => $validatorRequest->getDataNotificacion($data, 'telefono'),
                                'empresa'       => $documento->ayuntamiento_id,
                                'idDoc'       => $idDoc
                            ];
                            //referencia , identificacion y emisora
                        $notificacion = Notificaciones::where('referencia',$paramentos['referencia'])->first();
                        break;
                        case 3:
                            //ADRIAN AQUÍ RECIBIMOS EL RESTO DE CAMPOS, SINO SE NOS ENVÍA CÓDIGO DE BARRAS
                            //numJustificante es como referencia en modalidad 1 y 2
                            $paramentos = [
                                'idioma' => $idioma,
                                'emisora' => $data['entidad'],
                                'numJustificante' => $validatorRequest->comprobarNumJustificante($data['numJustificante'],$data['numJustificante']),
                                'importe' => $validatorRequest->importeRedsys($data['importe']),
                                'docIden' => $validatorRequest->DocumentoEsValido($data['tipoDoc'], $data['numDoc']),
                                'fecDevengo' => $validatorRequest->comprobarFecDevengo($data['fecDevengo']),
                                'expediente' => $data['expediente'],
                                'datoEspecifico' => $data['datoEspecifico'],
                                'idDoc' => $idDoc
                            ];
                            $notificacion = Notificaciones::where('referencia',$paramentos['numJustificante'])->first();
                        break;
                    }
                }

                //BORRAR ESTA ASIGNACION DE PARAMETRO
                //comprueba los fallos que ha habido en la formacion de los parametros

                if($notificacion){
                    return response()->json(
                        [
                            'message' => 'Este pago ya ha sido efectuado. Contacte con su administración.',
                            'data'    => null,
                            'status'  => 500
                        ], 200
                    );
                }
                if (!is_null($paramentos) && $cprct != '') {
                    foreach ($paramentos as $k => $param) {
                        if (is_null($param)) {
                            if ($flagCod) {
                                return response()->json(
                                    [
                                        'message' => 'Error en el codigo de barras -> ' . $k,
                                        'data'    => null,
                                        'status'  => 500
                                    ], 200
                                );
                            }

                            return response()->json(
                                [
                                    'message' => 'Error en ' . $k,
                                    'data'    => null,
                                    'status'  => 500
                                ], 200
                            );
                        }
                    }
                } else {
                    return $response;
                }
                if (!$validatorRequest->comprobarDC($paramentos, $documento->modalidad_id)) {
                    return response()->json(
                        [
                            'message' => 'Error en el digito de control',
                            'data'    => null,
                            'status'  => 500
                        ], 200
                    );
                }
                if ($paramentos) {
                    $redsys = new Redsys();
                    $obj = $redsys::$codificacion;
                    $tipoUrl = TiposUrl::where('nombre', '=', $data['origen'])->first();
                    $ayuntamiento_url = AyuntamientosUrl::where(
                        'ayuntamiento_id', '=', $documento->ayuntamiento_id
                    )->get();
                    $url = null;
                    if ($tipoUrl && $ayuntamiento_url) {
                        foreach ($ayuntamiento_url as $u) {
                            if ($url_flag = Url::where(
                                [['id', '=', $u->url_id], ['tipourl_id', '=', $tipoUrl->id]]
                            )->first()) {
                                $url = $url_flag;
                            }
                        }
                    }
                    $codificacion = $redsys->codificacionDatos($paramentos);
                    if ($codificacion != '') {
                        $objPost = $redsys->enviarPago($codificacion, $paramentos, $documento->ayuntamiento_id);
                        $objPost['status'] = 200;
                        return $objPost;
                    }
                }
            }
            $response = response()->json(
                [
                    'message' => 'Datos enviados erroneos',
                    'data'    => null,
                    'status'  => 500
                ], 200
            );
            $err = $validator->errors();
            foreach ($err as $k => $e) {
                var_dump($k);
                var_dump($e);
            }
            $response = $err;
        }
        return $response;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function validateFormPopUp($idDoc, Request $request) {
        $response = [];
        $dato = [];
        $data = $request->all();
        if ((isset($data['type']) && $data['type'] != null) && (isset($data['data']) && $data['data'] != null)) {
            $validatorRequest = new ValidatorRequest();
            $validator = null;
            switch ($data['type']) {
                case 'telefono':
                    $validator = Validator::make(
                        ["telefono" => $data['data']], ['telefono' => 'required|numeric|digits:9']
                    );
                    unset($data['check']);
                break;
                case 'email':
                    $validator = Validator::make(["email" => $data['data']], ['email' => 'required|email']);
                    unset($data['check']);
                break;
            }
            if ($validator && !$validator->fails()) {
                return $this->sendParams($idDoc, $request);
            };
            $err = $validator->errors();
            foreach ($err->messages() as $k => $e) {
                switch ($k) {
                    case 'telefono':
                        $response['telefono'] = true;
                    break;
                    case 'correo':
                        $response['correo'] = true;
                    break;
                }
            }
        }
        $response = response()->json(
            [
                'message' => 'Datos enviados erroneos',
                'data'    => $response,
                'status'  => 500
            ], 500
        );

        return $response;
    }
}
