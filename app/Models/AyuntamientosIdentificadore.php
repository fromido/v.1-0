<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AyuntamientosIdentificadore
 * 
 * @property int $id
 * @property int $tributo_id
 * @property string $ejercicio
 * @property string $remesa
 * @property Carbon $fechainicio
 * @property Carbon $fechafin
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * 
 * @property AyuntamientosTributo $ayuntamientos_tributo
 *
 * @package App\Models
 */
class AyuntamientosIdentificadore extends Model
{
	protected $table = 'ayuntamientos_identificadores';

	protected $casts = [
		'tributo_id' => 'int'
	];

	protected $dates = [
		'fechainicio',
		'fechafin'
	];

	protected $fillable = [
		'tributo_id',
		'ejercicio',
		'remesa',
		'fechainicio',
		'fechafin'
	];

	public function ayuntamientos_tributo()
	{
		return $this->belongsTo(AyuntamientosTributo::class, 'tributo_id');
	}
}
