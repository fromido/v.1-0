<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Comunidade
 * 
 * @property int $id
 * @property int $idcomunidadine
 * @property string $nombre
 * 
 * @property Collection|Provincia[] $provincias
 *
 * @package App\Models
 */
class Comunidade extends Model
{
	protected $table = 'comunidades';
	public $timestamps = false;

	protected $casts = [
		'idcomunidadine' => 'int'
	];

	protected $fillable = [
		'idcomunidadine',
		'nombre'
	];

	public function provincias()
	{
		return $this->hasMany(Provincia::class, 'comunidad_id');
	}
}
