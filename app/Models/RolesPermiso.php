<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class RolesPermiso
 * 
 * @property int $rol_id
 * @property int $permiso_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * 
 * @property Permiso $permiso
 * @property Role $role
 *
 * @package App\Models
 */
class RolesPermiso extends Model
{
	protected $table = 'roles_permisos';
	public $incrementing = false;

	protected $casts = [
		'rol_id' => 'int',
		'permiso_id' => 'int'
	];

	public function permiso()
	{
		return $this->belongsTo(Permiso::class);
	}

	public function role()
	{
		return $this->belongsTo(Role::class, 'rol_id');
	}
}
