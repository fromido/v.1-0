<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Role
 * 
 * @property int $id
 * @property string $nombre
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $descripcion
 * 
 * @property Collection|Permiso[] $permisos
 * @property Collection|Usuario[] $usuarios
 *
 * @package App\Models
 */
class Role extends Model
{
	protected $table = 'roles';

	protected $fillable = [
		'nombre',
		'descripcion'
	];

	public function permisos()
	{
		return $this->belongsToMany(Permiso::class, 'roles_permisos', 'rol_id')
					->withTimestamps();
	}

	public function usuarios()
	{
		return $this->belongsToMany(Usuario::class, 'roles_usuarios', 'rol_id')
					->withPivot('ayuntamiento_id')
					->withTimestamps();
	}
}
