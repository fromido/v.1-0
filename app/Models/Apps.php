<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Apps extends Model
{
    protected $table = 'apps';

    protected $fillable = [
        'id_entorno'
    ];

    public function deleteNulls()
    {
        foreach($this->getAttributes() as $key => $value){
            if(!$value){
                unset($this[$key]);
            }
        }
    }
}
