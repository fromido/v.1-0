<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Notificaciones
 *
 * @property int $id
 * @property string $codigo_ayuntamiento
 * @property string $fecha
 * @property string $hora
 * @property int $importe
 * @property int $moneda
 * @property string $numero_pedido
 * @property string $codigo_fuc
 * @property string $terminal
 * @property int $codigo_respuesta
 * @property string $datos_comercio
 * @property int $pago_seguro
 * @property string $tipo_operacion
 * @property string $pais
 * @property string $codigo_autorizacion
 * @property string $idioma
 * @property string $referencia
 * @property string $emisora
 * @property string $barcode
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */

class Notificaciones extends Model
{
    protected $table = 'notificaciones';
    public $timestamps = false;

    protected $casts = [
        'codigo_ayuntamiento' => 'varchar',
        'barcode' => 'varchar'
    ];

    protected $fillable = [
        'codigo_ayuntamiento',
        'fecha',
        'hora',
        'importe',
        'moneda',
        'numero_pedido',
        'codigo_fuc',
        'terminal',
        'tipo_tarjeta',
        'codigo_respuesta',
        'datos_comercio',
        'pago_seguro',
        'tipo_operacion',
        'pais',
        'codigo_autorizacion',
        'idioma',
        'referencia',
        'identificacion',
        'emisora',
        'barcode'
    ];

    public static function getNotificaciones($data)
    {

        $notificacion = null;

        $dataReturn = array( 'codbar' => '',
                         'referencia' => '',
                         'numJustificante' => '',
                         'emisora' => '',
                         'identificacion' => '');

        if (isset($data['codbar'])) {
            $dataReturn['codbar'] = $data['codbar'];
            $notificacion = Notificaciones::where('barcode', $dataReturn['codbar']);
            $dataReturn['referencia']= Notificaciones::ReferenciaDesdeCodbar($dataReturn['codbar']);
            $dataReturn['emisora']= Notificaciones::EmisoraDesdeCodbar($dataReturn['codbar']);
        }
        if (isset($data['referencia'])) {
            $dataReturn['referencia']=$data['referencia'];
            if($notificacion){
                $notificacion = $notificacion->where('referencia', $dataReturn['referencia']);
            }else{
                $notificacion = Notificaciones::where('referencia', $dataReturn['referencia']);
            }
        }
        if (isset($data['numJustificante'])) {
            $dataReturn['numJustificante']= $data['numJustificante'];
            if($notificacion){
                $notificacion = $notificacion->where('numJustificante',$dataReturn['numJustificante']);
            }else{
                $notificacion = Notificaciones::where('numJustificante',$dataReturn['numJustificante']);
            }

        }
        if (isset($data['emisora'])) {
            $dataReturn['emisora']= $data['emisora'];
            if($notificacion){
                $notificacion = $notificacion->where('emisora', $dataReturn['emisora']);
            }else{
                $notificacion = Notificaciones::where('emisora', $dataReturn['emisora']);
            }

        }
        if (isset($data['identificacion'])) {
            $dataReturn['identificacion']= $data['identificacion'];
            if($notificacion){
                $notificacion = $notificacion->where('identificacion',$dataReturn['identificacion']);
            }else{
                $notificacion = Notificaciones::where('identificacion',$dataReturn['identificacion']);
            }
        }
        if($notificacion){
            $notificacion = $notificacion->first();
            if($notificacion){
                return false;
            }
        }
        return $dataReturn;
    }

    ///////////////////////////////////////////////////////////////////
    // Función para obtener la Emisora desde el Código de Barras     //
    ///////////////////////////////////////////////////////////////////
    public static function EmisoraDesdeCodbar($codbar_aux) {
        $cpr = substr($codbar_aux, 0, 5);
        switch ($cpr) {
            case '90502':
            case '90521':
            case '90522':
            case '90523':
                return substr($codbar_aux, 5, 6);
            break;
            case '90508':
                return substr($codbar_aux, 15, 6);
            break;

        }

        return '';
    }

    ///////////////////////////////////////////////////////////////////
    // Función para obtener la Referencia desde el Código de Barras  //
    ///////////////////////////////////////////////////////////////////
    public static function ReferenciaDesdeCodbar($codbar_aux) {
        $cpr = substr($codbar_aux, 0, 5);
        switch ($cpr) {
            case '90502':
            case '90521':
            case '90522':
                return substr($codbar_aux, 11, 12);
            break;
            case '90508':
                return substr($codbar_aux, 21, 12);
            break;
            case '90523':
                return substr($codbar_aux, 11, 13);
            break;
        }

        return '';
    }
}
