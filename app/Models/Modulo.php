<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Modulo
 *
 * @property int $id
 * @property string $nombre
 * @property string $descripcion
 * @property bool $general
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property ModulosConfiguracion $modulos_configuracion
 *
 * @package App\Models
 */
class Modulo extends Model
{
	protected $table = 'modulos';
    protected $primaryKey = 'id';

	protected $casts = [
		'general' => 'bool'
	];

	protected $fillable = [
		'nombre',
		'descripcion',
		'general'
	];

	public function modulos_configuracion()
	{
		return $this->hasOne(ModulosConfiguracion::class);
	}
}
