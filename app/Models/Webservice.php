<?php

namespace App\Models;

use App\Models\Ayuntamiento;
use Illuminate\Database\Eloquent\Model;

class Webservice extends Model {
    //

    public static function pagosOnline($id_empresa, $dataAll, $datosEnvioRedsys, $datos) {
        $id_online = isset($dataAll['Ds_Order'][0]) ? (int)$dataAll['Ds_Order'][0] : '';
        ///////////////////////////////////////////////////////
        // Se llama al Servicio que actualiza RCPagoOnline   //
        ///////////////////////////////////////////////////////
        $ayuntamiento = Ayuntamiento::where('id', $id_empresa)->first();
        $resp_redSys = '';
        if (isset($dataAll['Ds_Response']) && isset($dataAll['Ds_Response'][0])) {
            $resp_redSys = (int)$dataAll['Ds_Response'][0];
        }
        // Se montan los Parámetros
        $entidad = 0;
        $method = 'PUT';
        $data = array(
            "IdPagoOnline" => $id_online,
            "Resultado"    => $resp_redSys,
            "Origen"       => "ventanilla",
            "Idioma"       => $datosEnvioRedsys['idioma'],
            "IdEmpresa"    => $datosEnvioRedsys['empresa'],
            "IdEnte"       => $entidad,
            "Referencia"   => $datos['referencia'],
            "Emisora"      => $datos['emisora']
        );
        $data_string = json_encode($data);

        // Se monta la Petición
        $url = 'http://10.254.0.26:9001/PagosOnline/v1/903/' . $id_online;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_NOBODY, false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
            $ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string)
            )
        );
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
        // Se toma el Código http

        $response = curl_exec($ch);
        $error = curl_error($ch);

        $result = array(
            'header'     => '',
            'body'       => '',
            'curl_error' => '',
            'http_code'  => '',
            'last_url'   => ''
        );

        if ($error != "") {
            $result['curl_error'] = $error;
        } else {
            $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            $result['header'] = substr($response, 0, $header_size);
            $result['body'] = substr($response, $header_size);
            $result['http_code'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $result['last_url'] = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
        }

        // Se toma el Código http
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        //fwrite($file, "despues de la funciuon" . PHP_EOL);

        // Si el resultado es erróneo
        $respuesta = 'KO';
        if ($http_code == 200 && $result['body'] == 'OK') {
            $respuesta = 'OK';
        } else {
            $respuesta = 'KO';
        }
        curl_close($ch);

        return $respuesta;
    }


    public static function paralizaCuota($id_empresa, $datosEnvioRedsys, $datos) {
        // Se montan los Parámetros
        $ayuntamiento = Ayuntamiento::where('id', $id_empresa)->first();
        $method = 'POST';
        $datosTributaria = array(
            "idEmpresa"     => $datosEnvioRedsys['empresa'],
            "referenciaDoc" => $datos['referencia'],
            "Emisora"       => $datos['emisora'],
            "codigo"        => "PT",
            "motivoPar"     => "Pago con tarjeta",
            "origen"        => $datosEnvioRedsys['origen']
        );
        $datosTributariaString = json_encode($datosTributaria);

        // DEFINIR EN BASE DE DATOS CORRECTAMENTE ESTOS CAMPOS

        $url = 'http://10.254.0.26:9029/paralizaCuota/v1';
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_NOBODY, false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $datosTributariaString);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
            $ch, CURLOPT_HTTPHEADER,
            array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($datosTributariaString)
            )
        );
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);

        $response = curl_exec($ch);
        $error = curl_error($ch);

        $result = array(
            'header'     => '',
            'body'       => '',
            'curl_error' => '',
            'http_code'  => '',
            'last_url'   => ''
        );

        if ($error != "") {
            $result['curl_error'] = $error;
        } else {
            $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            $result['header'] = substr($response, 0, $header_size);
            $result['body'] = substr($response, $header_size);
            $result['http_code'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $result['last_url'] = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
        }

        // Se toma el Código http
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        //fwrite($file, "despues de la funciuon" . PHP_EOL);

        // Si el resultado es erróneo
        $respuesta = 'KO';
        if ($http_code == 200 && $result['body'] == 'OK') {
            $respuesta = 'OK';
        } else {
            $respuesta = 'KO';
        }
        curl_close($ch);

        return $respuesta;


    }

}
