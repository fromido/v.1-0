<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Modalidadesc60
 * 
 * @property int $id
 * @property string $tipo
 * @property string $cpr
 * 
 * @property Collection|Docstpv[] $docstpvs
 * @property Collection|Modalidadesc60Codigobarra[] $modalidadesc60_codigobarras
 *
 * @package App\Models
 */
class Modalidadesc60 extends Model
{
	protected $table = 'modalidadesc60';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int'
	];

	protected $fillable = [
		'tipo',
		'cpr'
	];

	public function docstpvs()
	{
		return $this->hasMany(Docstpv::class, 'modalidad_id');
	}

	public function modalidadesc60_codigobarras()
	{
		return $this->hasMany(Modalidadesc60Codigobarra::class, 'modalidad_id');
	}
}
