<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Provincia
 * 
 * @property int $id
 * @property int $idprovinciaine
 * @property string $nombre
 * @property int $comunidad_id
 * 
 * @property Comunidade $comunidade
 * @property Collection|Poblacione[] $poblaciones
 *
 * @package App\Models
 */
class Provincia extends Model
{
	protected $table = 'provincias';
	public $timestamps = false;

	protected $casts = [
		'idprovinciaine' => 'int',
		'comunidad_id' => 'int'
	];

	protected $fillable = [
		'idprovinciaine',
		'nombre',
		'comunidad_id'
	];

	public function comunidade()
	{
		return $this->belongsTo(Comunidade::class, 'comunidad_id');
	}

	public function poblaciones()
	{
		return $this->hasMany(Poblacione::class);
	}
}
