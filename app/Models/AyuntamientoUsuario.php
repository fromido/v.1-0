<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AyuntamientoUsuario
 * 
 * @property int $id
 * @property int $ayuntamiento_id
 * @property int $usuario_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * 
 * @property Ayuntamiento $ayuntamiento
 * @property Usuario $usuario
 *
 * @package App\Models
 */
class AyuntamientoUsuario extends Model
{
	protected $table = 'ayuntamiento_usuario';

	protected $casts = [
		'ayuntamiento_id' => 'int',
		'usuario_id' => 'int'
	];

	protected $fillable = [
		'ayuntamiento_id',
		'usuario_id'
	];

	public function ayuntamiento()
	{
		return $this->belongsTo(Ayuntamiento::class);
	}

	public function usuario()
	{
		return $this->belongsTo(Usuario::class);
	}
}
