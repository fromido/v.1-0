<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Array_;

/**
 * Class Idioma
 *
 * @property int $id
 * @property string $nombre
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $descripcion
 *
 * @property Collection|Ayuntamiento[] $ayuntamientos
 * @property Collection|Docstpv[] $docstpvs
 * @property Collection|Usuario[] $usuarios
 *
 * @package App\Models
 */
class Idioma extends Model
{
	protected $table = 'idiomas';

	protected $fillable = [
		'nombre',
		'descripcion'
	];

	public function ayuntamientos()
	{
		return $this->belongsToMany(Ayuntamiento::class, 'ayuntamiento_idiomas')
					->withPivot('idiomadefecto')
					->withTimestamps();
	}

	public function docstpvs()
	{
		return $this->belongsToMany(Docstpv::class, 'docstpv_idiomas', 'idioma_id', 'docs_id')
					->withPivot('descripcion', 'textonopago')
					->withTimestamps();
	}

	public function usuarios()
	{
		return $this->hasMany(Usuario::class);
	}
	public function estructurar($idioma){
        if(isset($idioma->pivot)){
            $idioma->docs_id = $idioma->pivot->docs_id;
            $idioma->idioma_id = $idioma->pivot->idioma_id;
            $idioma->descripcion_doc = $idioma->pivot->descripcion;
            $idioma->textonopago = $idioma->pivot->textonopago;
            $idioma->nombre_doc = $idioma->pivot->nombre;
            $idioma->ayuda_doc = $idioma->pivot->ayuda;
            $idioma->img_doc = $idioma->pivot->img;

            unset($idioma->pivot);
        }
        return $idioma;
    }

    public function entidades()
    {
        //return $this->belongsTo(EntidadesBancaria::class,'entidad_id');
    }

    public function entidadesBancarias($doc_idioma){
        $imagenesArray = array();
        if(isset($doc_idioma->docs_id)){
            $documentosEntidades =DocstpvEntidades::where('docstpv_id', '=', $doc_idioma->docs_id)->get();
            foreach ($documentosEntidades as $d){
                array_push($imagenesArray, $d->entidadesBancarias);
            }
        }
        return $imagenesArray;
    }

}
