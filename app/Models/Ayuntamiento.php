<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Ayuntamiento
 *
 * @property int $id
 * @property string $codigo
 * @property string $nombreES
 * @property string $nombre
 * @property string $direccion
 * @property string $email
 * @property int $id_empresa
 * @property int $id_ente
 * @property string $urlpago
 * @property int $codtributoliq
 * @property int $emisoraliq
 * @property string $ordinal
 * @property string $nif
 * @property string $grupoapuntes
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property int $diasmod2
 * @property Carbon $fecanu
 * @property string $tipodocautoliq
 * @property bool $autoliqareca
 * @property bool $tieneOVT
 * @property bool $permiteCobroManual
 *
 * @property Collection|Idioma[] $idiomas
 * @property Collection|Usuario[] $usuarios
 * @property Collection|AyuntamientosComerciostpv[] $ayuntamientos_comerciostpvs
 * @property Collection|AyuntamientosDoc[] $ayuntamientos_docs
 * @property Collection|AyuntamientosImg[] $ayuntamientos_imgs
 * @property Collection|AyuntamientosTributo[] $ayuntamientos_tributos
 * @property Collection|Url[] $urls
 * @property Collection|Docstpv[] $docstpvs
 * @property Collection|RolesUsuario[] $roles_usuarios
 *
 * @package App\Models
 */
class Ayuntamiento extends Model
{
	protected $table = 'ayuntamientos';

	protected $casts = [
		'id_empresa' => 'int',
		'id_ente' => 'int',
		'codtributoliq' => 'int',
		'emisoraliq' => 'int',
		'diasmod2' => 'int',
		'autoliqareca' => 'bool',
		'tieneOVT' => 'bool',
		'permiteCobroManual' => 'bool'
	];

	protected $dates = [
		'fecanu'
	];

	protected $fillable = [
		'codigo',
		'nombreES',
		'nombre',
		'direccion',
		'email',
		'id_empresa',
		'id_ente',
		'urlpago',
		'codtributoliq',
		'emisoraliq',
		'ordinal',
		'nif',
		'grupoapuntes',
		'diasmod2',
		'fecanu',
		'tipodocautoliq',
		'autoliqareca',
		'tieneOVT',
		'permiteCobroManual'
	];

	public function idiomas()
	{
		return $this->belongsToMany(Idioma::class, 'ayuntamiento_idiomas')
					->withPivot('idiomadefecto')
					->withTimestamps();
	}

	public function usuarios()
	{
		return $this->belongsToMany(Usuario::class)
					->withPivot('id')
					->withTimestamps();
	}

	public function ayuntamientos_comerciostpvs()
	{
		return $this->hasMany(AyuntamientosComerciostpv::class);
	}

	public function ayuntamientos_docs()
	{
		return $this->hasMany(AyuntamientosDoc::class);
	}

	public function ayuntamientos_imgs()
	{
		return $this->hasMany(AyuntamientosImg::class);
	}

	public function ayuntamientos_tributos()
	{
		return $this->hasMany(AyuntamientosTributo::class);
	}

	public function urls()
	{
		return $this->belongsToMany(Url::class, 'ayuntamientos_urls')
					->withTimestamps();
	}

	public function docstpvs()
	{
		return $this->hasMany(Docstpv::class);
	}

    public function docstpvs_by_id($id) {
        return $this->docstpvs()->where('id','=', $id);
    }


	public function roles_usuarios()
	{
		return $this->hasMany(RolesUsuario::class);
	}

    public function idiomas_documentos($idioma){
	    $docs = $this->docstpvs()->get();
        $ayun = array();
	    foreach ($docs as $d){
            array_push($ayun, $d->languages($idioma));
        }
        return $ayun;
    }
}
