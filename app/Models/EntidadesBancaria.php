<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EntidadesBancaria
 *
 * @property int $id
 * @property string $CIF
 * @property string $nombre
 * @property string $web
 * @property string $img_logo
 * @property bool $status
 *
 * @package App\Models
 */
class EntidadesBancaria extends Model
{
	protected $table = 'entidades_bancarias';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int',
		'status' => 'bool'
	];

	protected $fillable = [
		'CIF',
		'nombre',
		'web',
		'img_logo',
		'status'
	];

    public function docstpv_entidades()
    {
        return $this->belongsToMany(Docstpv::class, 'docstpv_entidades')
            ->withPivot('entidad_id')
            ->withTimestamps();
    }

}
