<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DocstpvIdioma
 * 
 * @property int $docs_id
 * @property int $idioma_id
 * @property string $descripcion
 * @property string $textonopago
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * 
 * @property Docstpv $docstpv
 * @property Idioma $idioma
 *
 * @package App\Models
 */
class DocstpvIdioma extends Model
{
	protected $table = 'docstpv_idiomas';
	public $incrementing = false;

	protected $casts = [
		'docs_id' => 'int',
		'idioma_id' => 'int'
	];

	protected $fillable = [
		'descripcion',
		'textonopago'
	];

	public function docstpv()
	{
		return $this->belongsTo(Docstpv::class, 'docs_id');
	}

	public function idioma()
	{
		return $this->belongsTo(Idioma::class);
	}
}
