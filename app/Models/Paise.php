<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Paise
 * 
 * @property int $id
 * @property int $idpaisine
 * @property string $nombre
 * @property int $continente_id
 * 
 * @property Continente $continente
 *
 * @package App\Models
 */
class Paise extends Model
{
	protected $table = 'paises';
	public $timestamps = false;

	protected $casts = [
		'idpaisine' => 'int',
		'continente_id' => 'int'
	];

	protected $fillable = [
		'idpaisine',
		'nombre',
		'continente_id'
	];

	public function continente()
	{
		return $this->belongsTo(Continente::class);
	}
}
