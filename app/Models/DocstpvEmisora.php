<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DocstpvEmisora
 * 
 * @property int $docs_id
 * @property int $emisora_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * 
 * @property Docstpv $docstpv
 * @property AyuntamientosEmisora $ayuntamientos_emisora
 *
 * @package App\Models
 */
class DocstpvEmisora extends Model
{
	protected $table = 'docstpv_emisora';
	public $incrementing = false;

	protected $casts = [
		'docs_id' => 'int',
		'emisora_id' => 'int'
	];

	public function docstpv()
	{
		return $this->belongsTo(Docstpv::class, 'docs_id');
	}

	public function ayuntamientos_emisora()
	{
		return $this->belongsTo(AyuntamientosEmisora::class, 'emisora_id');
	}
}
