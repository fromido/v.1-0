<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TiposUrl
 * 
 * @property int $id
 * @property string $nombre
 * @property string $descripcion
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * 
 * @property Collection|Url[] $urls
 *
 * @package App\Models
 */
class TiposUrl extends Model
{
	protected $table = 'tipos_urls';

	protected $fillable = [
		'nombre',
		'descripcion'
	];

	public function urls()
	{
		return $this->hasMany(Url::class, 'tipourl_id');
	}
}
