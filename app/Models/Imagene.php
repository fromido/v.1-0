<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Imagene
 * 
 * @property int $id
 * @property string $ruta
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property int $tipoimagen_id
 * @property string $resolucion
 * 
 * @property TiposImagene $tipos_imagene
 * @property Collection|AyuntamientosImg[] $ayuntamientos_imgs
 *
 * @package App\Models
 */
class Imagene extends Model
{
	protected $table = 'imagenes';

	protected $casts = [
		'tipoimagen_id' => 'int'
	];

	protected $fillable = [
		'ruta',
		'tipoimagen_id',
		'resolucion'
	];

	public function tipos_imagene()
	{
		return $this->belongsTo(TiposImagene::class, 'tipoimagen_id');
	}

	public function ayuntamientos_imgs()
	{
		return $this->hasMany(AyuntamientosImg::class, 'imagen_id');
	}
}
