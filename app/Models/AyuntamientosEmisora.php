<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AyuntamientosEmisora
 * 
 * @property int $id_emisora
 * @property int $comercio_id
 * @property string $descripcion
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * 
 * @property AyuntamientosComerciostpv $ayuntamientos_comerciostpv
 * @property Collection|DocstpvEmisora[] $docstpv_emisoras
 *
 * @package App\Models
 */
class AyuntamientosEmisora extends Model
{
	protected $table = 'ayuntamientos_emisora';
	protected $primaryKey = 'id_emisora';
	public $incrementing = false;

	protected $casts = [
		'id_emisora' => 'int',
		'comercio_id' => 'int'
	];

	protected $fillable = [
		'comercio_id',
		'descripcion'
	];

	public function ayuntamientos_comerciostpv()
	{
		return $this->belongsTo(AyuntamientosComerciostpv::class, 'comercio_id');
	}

	public function docstpv_emisoras()
	{
		return $this->hasMany(DocstpvEmisora::class, 'emisora_id');
	}
}
