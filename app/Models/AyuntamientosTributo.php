<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AyuntamientosTributo
 * 
 * @property int $id
 * @property int $ayuntamiento_id
 * @property string $codigo
 * @property string $descripcion
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * 
 * @property Ayuntamiento $ayuntamiento
 * @property Collection|AyuntamientosIdentificadore[] $ayuntamientos_identificadores
 *
 * @package App\Models
 */
class AyuntamientosTributo extends Model
{
	protected $table = 'ayuntamientos_tributo';

	protected $casts = [
		'ayuntamiento_id' => 'int'
	];

	protected $fillable = [
		'ayuntamiento_id',
		'codigo',
		'descripcion'
	];

	public function ayuntamiento()
	{
		return $this->belongsTo(Ayuntamiento::class);
	}

	public function ayuntamientos_identificadores()
	{
		return $this->hasMany(AyuntamientosIdentificadore::class, 'tributo_id');
	}
}
