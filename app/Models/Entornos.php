<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Entornos extends Model
{
    protected $table = 'entornos';

    public function apps($value)
    {
        return $this->hasMany(Apps::class,'id_entorno')->where('descripcion','=',$value)->first();
    }

    public function getData($value){
        $app = $this->apps($value);
        if($app){
            $app->deleteNulls();
            $this->app = $app;
        }
    }
}
