<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Usuario
 * 
 * @property int $id
 * @property string $nombre
 * @property string $email
 * @property string $usuario
 * @property string $password
 * @property int $idioma_id
 * @property string $dni
 * @property string $telefono
 * @property bool $admin
 * @property string $remember_token
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $fecanu
 * 
 * @property Idioma $idioma
 * @property Collection|Ayuntamiento[] $ayuntamientos
 * @property Collection|Role[] $roles
 *
 * @package App\Models
 */
class Usuario extends Model
{
	protected $table = 'usuarios';

	protected $casts = [
		'idioma_id' => 'int',
		'admin' => 'bool'
	];

	protected $dates = [
		'fecanu'
	];

	protected $hidden = [
		'password',
		'remember_token'
	];

	protected $fillable = [
		'nombre',
		'email',
		'usuario',
		'password',
		'idioma_id',
		'dni',
		'telefono',
		'admin',
		'remember_token',
		'fecanu'
	];

	public function idioma()
	{
		return $this->belongsTo(Idioma::class);
	}

	public function ayuntamientos()
	{
		return $this->belongsToMany(Ayuntamiento::class)
					->withPivot('id')
					->withTimestamps();
	}

	public function roles()
	{
		return $this->belongsToMany(Role::class, 'roles_usuarios', 'usuario_id', 'rol_id')
					->withPivot('ayuntamiento_id')
					->withTimestamps();
	}
}
