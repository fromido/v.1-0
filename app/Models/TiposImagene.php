<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TiposImagene
 * 
 * @property int $id
 * @property string $descripcion
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * 
 * @property Collection|AyuntamientosDoc[] $ayuntamientos_docs
 * @property Collection|Imagene[] $imagenes
 *
 * @package App\Models
 */
class TiposImagene extends Model
{
	protected $table = 'tipos_imagenes';

	protected $fillable = [
		'descripcion'
	];

	public function ayuntamientos_docs()
	{
		return $this->hasMany(AyuntamientosDoc::class, 'tipoimagendefecto_id');
	}

	public function imagenes()
	{
		return $this->hasMany(Imagene::class, 'tipoimagen_id');
	}
}
