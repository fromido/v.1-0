<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class RolesUsuario
 * 
 * @property int $ayuntamiento_id
 * @property int $usuario_id
 * @property int $rol_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * 
 * @property Ayuntamiento $ayuntamiento
 * @property Role $role
 * @property Usuario $usuario
 *
 * @package App\Models
 */
class RolesUsuario extends Model
{
	protected $table = 'roles_usuarios';
	public $incrementing = false;

	protected $casts = [
		'ayuntamiento_id' => 'int',
		'usuario_id' => 'int',
		'rol_id' => 'int'
	];

	public function ayuntamiento()
	{
		return $this->belongsTo(Ayuntamiento::class);
	}

	public function role()
	{
		return $this->belongsTo(Role::class, 'rol_id');
	}

	public function usuario()
	{
		return $this->belongsTo(Usuario::class);
	}
}
