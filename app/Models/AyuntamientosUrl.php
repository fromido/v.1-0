<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AyuntamientosUrl
 * 
 * @property int $ayuntamiento_id
 * @property int $url_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * 
 * @property Ayuntamiento $ayuntamiento
 * @property Url $url
 *
 * @package App\Models
 */
class AyuntamientosUrl extends Model
{
	protected $table = 'ayuntamientos_urls';
	public $incrementing = false;

	protected $casts = [
		'ayuntamiento_id' => 'int',
		'url_id' => 'int'
	];

	public function ayuntamiento()
	{
		return $this->belongsTo(Ayuntamiento::class);
	}

	public function url()
	{
		return $this->belongsTo(Url::class);
	}
}
