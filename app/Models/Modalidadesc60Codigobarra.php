<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Modalidadesc60Codigobarra
 * 
 * @property int $id
 * @property int $modalidad_id
 * @property string $codBar
 * @property string $descripcion
 * 
 * @property Modalidadesc60 $modalidadesc60
 *
 * @package App\Models
 */
class Modalidadesc60Codigobarra extends Model
{
	protected $table = 'modalidadesc60_codigobarras';
	public $timestamps = false;

	protected $casts = [
		'modalidad_id' => 'int'
	];

	protected $fillable = [
		'modalidad_id',
		'codBar',
		'descripcion'
	];

	public function modalidadesc60()
	{
		return $this->belongsTo(Modalidadesc60::class, 'modalidad_id');
	}
}
