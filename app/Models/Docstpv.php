<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Docstpv
 *
 * @property int $id
 * @property int $ayuntamiento_id
 * @property string $ruta
 * @property int $modalidad_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property Ayuntamiento $ayuntamiento
 * @property Modalidadesc60 $modalidadesc60
 * @property Collection|DocstpvEmisora[] $docstpv_emisoras
 * @property Collection|Idioma[] $idiomas
 *
 * @package App\Models
 */
class Docstpv extends Model
{
	protected $table = 'docstpv';

	protected $casts = [
		'ayuntamiento_id' => 'int',
		'modalidad_id' => 'int'
	];

	protected $fillable = [
		'ayuntamiento_id',
		'ruta',
		'modalidad_id'
	];

	public function ayuntamiento()
	{
		return $this->belongsTo(Ayuntamiento::class);
	}

	public function modalidadesc60()
	{
		return $this->belongsTo(Modalidadesc60::class, 'modalidad_id');
	}

	public function docstpv_emisoras()
	{
		return $this->hasMany(DocstpvEmisora::class, 'docs_id');
	}

	public function idiomas()
	{
		return $this->belongsToMany(Idioma::class, 'docstpv_idiomas', 'docs_id')
					->withPivot('descripcion', 'textonopago')
					->withTimestamps();
	}


    /**
     * @param $id
     * @return mixed
     */
    public function languages($id)
    {
        return $this->belongsToMany(Idioma::class, 'docstpv_idiomas', 'docs_id')->where("idioma_id",$id)
            ->withPivot('descripcion', 'textonopago','ayuda','nombre','img')
            ->withTimestamps()->first();
    }

}
