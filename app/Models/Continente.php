<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Continente
 * 
 * @property int $id
 * @property int $idcontinenteine
 * @property string $nombre
 * 
 * @property Collection|Paise[] $paises
 *
 * @package App\Models
 */
class Continente extends Model
{
	protected $table = 'continentes';
	public $timestamps = false;

	protected $casts = [
		'idcontinenteine' => 'int'
	];

	protected $fillable = [
		'idcontinenteine',
		'nombre'
	];

	public function paises()
	{
		return $this->hasMany(Paise::class);
	}
}
