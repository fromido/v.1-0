<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AyuntamientosImg
 * 
 * @property int $ayuntamiento_id
 * @property int $imagen_id
 * @property bool $imagendefecto
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * 
 * @property Ayuntamiento $ayuntamiento
 * @property Imagene $imagene
 *
 * @package App\Models
 */
class AyuntamientosImg extends Model
{
	protected $table = 'ayuntamientos_img';
	public $incrementing = false;

	protected $casts = [
		'ayuntamiento_id' => 'int',
		'imagen_id' => 'int',
		'imagendefecto' => 'bool'
	];

	protected $fillable = [
		'imagendefecto'
	];

	public function ayuntamiento()
	{
		return $this->belongsTo(Ayuntamiento::class);
	}

	public function imagene()
	{
		return $this->belongsTo(Imagene::class, 'imagen_id');
	}
}
