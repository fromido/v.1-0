<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AyuntamientosDoc
 * 
 * @property int $ayuntamiento_id
 * @property int $tipodoc_id
 * @property int $plantilla_id
 * @property int $tipoimagendefecto_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * 
 * @property Ayuntamiento $ayuntamiento
 * @property Plantilla $plantilla
 * @property TiposDocumento $tipos_documento
 * @property TiposImagene $tipos_imagene
 *
 * @package App\Models
 */
class AyuntamientosDoc extends Model
{
	protected $table = 'ayuntamientos_docs';
	public $incrementing = false;

	protected $casts = [
		'ayuntamiento_id' => 'int',
		'tipodoc_id' => 'int',
		'plantilla_id' => 'int',
		'tipoimagendefecto_id' => 'int'
	];

	protected $fillable = [
		'plantilla_id',
		'tipoimagendefecto_id'
	];

	public function ayuntamiento()
	{
		return $this->belongsTo(Ayuntamiento::class);
	}

	public function plantilla()
	{
		return $this->belongsTo(Plantilla::class);
	}

	public function tipos_documento()
	{
		return $this->belongsTo(TiposDocumento::class, 'tipodoc_id');
	}

	public function tipos_imagene()
	{
		return $this->belongsTo(TiposImagene::class, 'tipoimagendefecto_id');
	}
}
