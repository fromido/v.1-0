<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Url
 * 
 * @property int $id
 * @property string $url
 * @property int $tipourl_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * 
 * @property TiposUrl $tipos_url
 * @property Collection|Ayuntamiento[] $ayuntamientos
 *
 * @package App\Models
 */
class Url extends Model
{
	protected $table = 'urls';

	protected $casts = [
		'tipourl_id' => 'int'
	];

	protected $fillable = [
		'url',
		'tipourl_id'
	];

	public function tipos_url()
	{
		return $this->belongsTo(TiposUrl::class, 'tipourl_id');
	}

	public function ayuntamientos()
	{
		return $this->belongsToMany(Ayuntamiento::class, 'ayuntamientos_urls')
					->withTimestamps();
	}
}
