<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ModulosConfiguracion
 * 
 * @property int $modulo_id
 * @property string $ipservidor
 * @property string $rutaraiz
 * @property string $rutaimagenes
 * @property string $rutaplantillas
 * @property string $ipservicios
 * @property string $formulariopagotpv
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * 
 * @property Modulo $modulo
 *
 * @package App\Models
 */
class ModulosConfiguracion extends Model
{
	protected $table = 'modulos_configuracion';
	protected $primaryKey = 'modulo_id';
	public $incrementing = false;

	protected $casts = [
		'modulo_id' => 'int'
	];

	protected $fillable = [
		'ipservidor',
		'rutaraiz',
		'rutaimagenes',
		'rutaplantillas',
		'ipservicios',
		'formulariopagotpv'
	];

	public function modulo()
	{
		return $this->belongsTo(Modulo::class);
	}
}
