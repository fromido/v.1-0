<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AyuntamientosComerciostpv
 * 
 * @property int $id
 * @property int $ayuntamiento_id
 * @property string $codigoComercio
 * @property string $claveComercio
 * @property string $entidad
 * @property string $sucursal
 * @property string $dc
 * @property string $cuenta
 * @property string $nombre
 * @property int $terminal
 * @property int $moneda
 * @property bool $tipo_trans
 * @property string $url_Notificacion
 * @property string $url_OKKO
 * @property string $version_Alg
 * @property string $Url_OKKO_Ventanilla
 * @property string $Email_Contacto
 * @property string $Ruta_DestinoC60
 * @property string $Url_Base
 * @property string $Nombre_Publico
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * 
 * @property Ayuntamiento $ayuntamiento
 * @property Collection|AyuntamientosEmisora[] $ayuntamientos_emisoras
 *
 * @package App\Models
 */
class AyuntamientosComerciostpv extends Model
{
	protected $table = 'ayuntamientos_comerciostpv';

	protected $casts = [
		'ayuntamiento_id' => 'int',
		'terminal' => 'int',
		'moneda' => 'int',
		'tipo_trans' => 'bool'
	];

	protected $fillable = [
		'ayuntamiento_id',
		'codigoComercio',
		'claveComercio',
		'entidad',
		'sucursal',
		'dc',
		'cuenta',
		'nombre',
		'terminal',
		'moneda',
		'tipo_trans',
		'url_Notificacion',
		'url_OKKO',
		'version_Alg',
		'Url_OKKO_Ventanilla',
		'Email_Contacto',
		'Ruta_DestinoC60',
		'Url_Base',
		'Nombre_Publico'
	];

	public function ayuntamiento()
	{
		return $this->belongsTo(Ayuntamiento::class);
	}

	public function ayuntamientos_emisoras()
	{
		return $this->hasMany(AyuntamientosEmisora::class, 'comercio_id');
	}
}
