<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Permiso
 * 
 * @property int $id
 * @property string $nombre
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $descripcion
 * 
 * @property Collection|Role[] $roles
 *
 * @package App\Models
 */
class Permiso extends Model
{
	protected $table = 'permisos';

	protected $fillable = [
		'nombre',
		'descripcion'
	];

	public function roles()
	{
		return $this->belongsToMany(Role::class, 'roles_permisos', 'permiso_id', 'rol_id')
					->withTimestamps();
	}
}
