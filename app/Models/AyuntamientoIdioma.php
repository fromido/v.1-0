<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AyuntamientoIdioma
 * 
 * @property int $ayuntamiento_id
 * @property int $idioma_id
 * @property bool $idiomadefecto
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * 
 * @property Ayuntamiento $ayuntamiento
 * @property Idioma $idioma
 *
 * @package App\Models
 */
class AyuntamientoIdioma extends Model
{
	protected $table = 'ayuntamiento_idiomas';
	public $incrementing = false;

	protected $casts = [
		'ayuntamiento_id' => 'int',
		'idioma_id' => 'int',
		'idiomadefecto' => 'bool'
	];

	protected $fillable = [
		'idiomadefecto'
	];

	public function ayuntamiento()
	{
		return $this->belongsTo(Ayuntamiento::class);
	}

	public function idioma()
	{
		return $this->belongsTo(Idioma::class);
	}
}
