<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Poblacione
 * 
 * @property int $id
 * @property int $idpoblacionine
 * @property int $dc
 * @property string $nombre
 * @property int $provincia_id
 * 
 * @property Provincia $provincia
 *
 * @package App\Models
 */
class Poblacione extends Model
{
	protected $table = 'poblaciones';
	public $timestamps = false;

	protected $casts = [
		'idpoblacionine' => 'int',
		'dc' => 'int',
		'provincia_id' => 'int'
	];

	protected $fillable = [
		'idpoblacionine',
		'dc',
		'nombre',
		'provincia_id'
	];

	public function provincia()
	{
		return $this->belongsTo(Provincia::class);
	}
}
