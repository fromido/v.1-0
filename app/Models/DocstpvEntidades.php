<?php


namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DocstpvEntidades
 *
 * @property int $docstpv_id
 * @property int $entidad_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property Docstpv $docstpv
 * @property AyuntamientosEmisora $entidadesBancarias
 *
 * @package App\Models
 */

class DocstpvEntidades extends Model {
    protected $table = 'docstpv_entidades';


    protected $casts = [
        'docstpv_id' => 'int',
        'entidad_id' => 'int'
    ];
    protected $fillable = [
        'entidad_id'
    ];

    public function docstpv()
    {
        return $this->belongsTo(Docstpv::class, 'docs_id');
    }

    public function entidadesBancarias()
    {
        return $this->belongsTo(EntidadesBancaria::class, 'entidad_id');
    }
}
