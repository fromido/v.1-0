<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TiposDocumento
 * 
 * @property int $id
 * @property string $descripcion
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * 
 * @property Collection|AyuntamientosDoc[] $ayuntamientos_docs
 *
 * @package App\Models
 */
class TiposDocumento extends Model
{
	protected $table = 'tipos_documentos';

	protected $fillable = [
		'descripcion'
	];

	public function ayuntamientos_docs()
	{
		return $this->hasMany(AyuntamientosDoc::class, 'tipodoc_id');
	}
}
