<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Acciones extends Model
{
    protected $table = 'acciones';

    protected $casts = [
        'id_ayuntamiento' => 'int',
        'id_app' => 'int',
    ];

    protected $fillable = [
        'id_ayuntamiento',
        'id_app',
        'nombre_accion',
        'mensaje',
        'status'
    ];
}
