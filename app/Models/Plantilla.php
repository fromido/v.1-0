<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Plantilla
 * 
 * @property int $id
 * @property string $ruta
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * 
 * @property Collection|AyuntamientosDoc[] $ayuntamientos_docs
 *
 * @package App\Models
 */
class Plantilla extends Model
{
	protected $table = 'plantillas';

	protected $fillable = [
		'ruta'
	];

	public function ayuntamientos_docs()
	{
		return $this->hasMany(AyuntamientosDoc::class);
	}
}
