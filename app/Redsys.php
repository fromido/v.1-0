<?php

namespace App;

use App\Libraries\RedsysAPI;
use App\models\Acciones;
use App\Models\Ayuntamiento;
use App\Models\AyuntamientosComerciostpv;
use App\Models\AyuntamientosEmisora;
use App\Models\Entornos;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redirect;

class Redsys extends Model {
    public static $codificacion = [
        'origen'           => '',
        'tipojustificante' => '',
        'tipoenvio'        => '',
        'email'            => '',
        'telefono'         => '',
        'codbar'           => '',
        'idioma'           => 'es',
        'nif'              => '',
        'fechadevengo'     => '',
        'datoespecifico'   => '',
    ];
    /**
     * @param  $objeto
     * @param bool $y
     * @return string
     * @author David Salgado
     */
    public function codificacionDatos($objeto, $y = false) {
        $codificacion = '';
        if (array_key_exists('origen', $objeto) && $objeto['origen'] != '') {
            $codificacion .= "origen=" . $this->getOrigen($objeto['origen']);
        }
        if (array_key_exists('tipoenvio', $objeto) && $objeto['tipoenvio'] != '') {
            $codificacion .= "#tipoenvio=" . $objeto['tipoenvio'];
        }
        if (array_key_exists('email', $objeto) && $objeto['email'] != '') {
            $codificacion .= "#email=" . $objeto['email'];
        }
        if (array_key_exists('telefono', $objeto) && $objeto['telefono'] != '') {
            $codificacion .= "#telefono=" . $objeto['telefono'];
        }
        if (array_key_exists('codbar', $objeto) && $objeto['codbar'] != '') {
            //en teoria el el objeto parametros
            $codificacion .= "#codbar=" . $objeto['codbar'];
        }
        if (array_key_exists('emisora', $objeto) && $objeto['emisora'] != '') {
            //en teoria el el objeto parametros
            $codificacion .= "#emisora=" . $objeto['emisora'];
        }
        if (array_key_exists('referencia', $objeto) && $objeto['referencia'] != '') {
            //en teoria el el objeto parametros
            $codificacion .= "#referencia=" . $objeto['referencia'];
        }
        if (array_key_exists('identificacion', $objeto) && $objeto['identificacion'] != '') {
            //en teoria el el objeto parametros
            $codificacion .= "#identificacion=" . $objeto['identificacion'];
        }
        if (array_key_exists('idioma', $objeto) && $objeto['idioma'] != '') {
            //'es' o 'vlc'
            $codificacion .= "#idioma=" . $objeto['idioma'];
        }
        if (array_key_exists('empresa', $objeto) && $objeto['empresa'] != '') {
            $codificacion .= "#empresa=" . $objeto['empresa'];
        }
        if (array_key_exists('idDoc', $objeto) && $objeto['idDoc'] != '') {
            $codificacion .= "#idDoc=" . $objeto['idDoc'];
        }
        if ($y) {
            if (array_key_exists('nif', $objeto) && $objeto['nif'] != '') {
                $codificacion .= "#nif=" . $objeto['nif'];
            }
            if (array_key_exists('fechadevengo', $objeto) && $objeto['fechadevengo'] != '') {
                $codificacion .= "#fechadevengo=" . $objeto['fechadevengo'];
            }
            if (array_key_exists('expediente', $objeto) && $objeto['expediente'] != '') {
                $codificacion .= "#expediente=" . $objeto['expediente'];
            }
            if (array_key_exists('datoespecifico', $objeto) && $objeto['datoespecifico'] != '') {
                $codificacion .= "#datoespecifico=" . $objeto['datoespecifico']."#";
            }
        } else {
            $codificacion .= "#nif=#fechadevengo=#expediente=#datoespecifico=#";
        }
        return base64_encode($codificacion);
    }

    ////////////////////////////////////////////////
    // FUNCIÓN PARA SACAR EL ORIGEN               //
    // DESDE EL CHORIZO DE PAGO                   //
    ////////////////////////////////////////////////
    /// HEREDADA DE ANTERIOR PROYECTO TPV
    public function descodificarDatpos($codificacion) {
        $codbar_aux = explode("#", base64_decode($codificacion));
        $codbar_aux2 = explode("origen=", $codbar_aux[0]);
        $origen = $codbar_aux2[1];

        return $origen;
    }

    /**
     * @todo funcion encargada de ver el origen de la peticion web
     * @param $origen
     * @return string
     * @author David Salgado Diez
     */
    public function getOrigen($origen) {
        switch ($origen) {
            case 'pagament':
                return 'websin';
            case 'formulario2_ovt':
                return 'websinOVT';
            case 'formulario2_ventanilla':
                return 'websinOVT';
            case 'ppv':
                $origen = 'puntoverde';
            default:
                return 'websin';
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///// FUNCIONES PARA EL PAGO EN REDSYS                                                                  //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function enviarPago($codificacion, $params) {

        $entorno = Entornos::where('nombre', env('APP_ENTORNO'))->first();
        $entorno->getData('TPV-Generico');

        $accion = new Acciones();
        if($entorno->app){
            $accion->id_app =$entorno->app->id;
        }else{
            $accion->id_app = 1;
        }
        $accion->nombre_accion = 'Enviar Pago';
        //CREACION DE VARIABLES PARA HACER LA PETICION A REDSYS
        $fuc = '';
        $terminal = '';
        $moneda = '';
        $trans = '';
        $url = '';
        $urlOKKO = '';
        $kc = '';
        $ruta_tpv = '';
        $version = '';
        $signature = '';
        $origen ='pagament';
        $accion->mensaje ='Asigna valores que se <b style="color:green">envian a Redsys</b><br>';
        if(isset($params['origen']) && $params['origen'] !=''){
            $origen =$params['origen'];
            $accion->mensaje =$accion->mensaje.'Asigna el origen en <b style="color:green">'.$origen.'</b><br>';
        }
        // Se incluye la librería apiRedsys
        // Se crea Objeto
        $miObj = new RedsysAPI;
        $ayuntamiento = Ayuntamiento::where('id','=',$params['empresa'])->first();
        $ayuntamiento->urls;
        $urlOKKO ='';
        foreach ($ayuntamiento->urls as $temp_url){
            if($temp_url->tipos_url->nombre == 'TPV'){
                $urlOKKO = $temp_url->url;
            }
        }
        $data = AyuntamientosEmisora::where([['id_emisora', '=', $params['emisora']]])->first();
        $accion->mensaje =$accion->mensaje.'Asigna el origen en <b style="color:green">'.$origen.'</b><br>';
        if (isset($data) && !$data) {
            return null;
        }
        $aytoComercio = $data->ayuntamientos_comerciostpv;
        // Codigo FUC
        if ((isset($aytoComercio->codigoComercio)) && ($aytoComercio->codigoComercio != '')) {
            $fuc = $aytoComercio->codigoComercio;
        }
        // Terminal
        if ((isset($aytoComercio->terminal)) && ($aytoComercio->terminal > 0)) {
            $terminal = str_pad($aytoComercio->terminal, 3, "0", STR_PAD_LEFT);
        }
        // Moneda
        if ((isset($aytoComercio->moneda)) && ($aytoComercio->moneda > 0)) {
            $moneda = $aytoComercio->moneda;
        }
        // Tipo_Trans
        if (isset($aytoComercio->tipo_trans)) {
            $trans = $aytoComercio->tipo_trans;
            if(!$trans){
                $trans=0;
            }
        }
        // Url_Notificacion
        if ((isset($aytoComercio->url_Notificacion)) && ($aytoComercio->url_Notificacion != '')) {
            $url = $aytoComercio->url_Notificacion;
        }
        // Url_OKKO
        if ((isset($aytoComercio->url_OKKO)) && ($aytoComercio->url_OKKO != '')) {
            //echo "Entro en Url_OKKO <br>";
            switch ($this->descodificarDatpos($codificacion)) {
                case 'puntoverde':
                    $urlOKKO .= "_pv";
                break;
                case 'websinOVT':
                    $urlOKKO .= "_ovt";
                break;
            }
        }
        // Versión Algoritmo
        if ((isset($aytoComercio->version_Alg)) && ($aytoComercio->version_Alg != '')) {
            $version = $aytoComercio->version_Alg;
        }
        // Clave Comercio
        if ((isset($aytoComercio->claveComercio)) && ($aytoComercio->claveComercio != '')) {
            $kc = $aytoComercio->claveComercio;
        }
        $accion->mensaje =$accion->mensaje.'<b style="color:green">Llena todas las variables</b><br>';

        // Ruta TPV

        $ruta_tpv = $entorno->app->url_redsys;
        $urlOKKO.=$origen.$entorno->app->url_pagookko_redsys;
        $url=$entorno->app->url_notificacion_redsys;

        // Valores de entrada
        $id = time();
        $desc = $params['referencia'];
        $amount = $params['importe'];
        // Se Rellenan los campos
        $miObj->setParameter("DS_MERCHANT_AMOUNT", $amount);
        $miObj->setParameter("DS_MERCHANT_ORDER", strval($id));
        $miObj->setParameter("DS_MERCHANT_MERCHANTCODE", $fuc);
        $miObj->setParameter("DS_MERCHANT_CURRENCY", $moneda);
        $miObj->setParameter("DS_MERCHANT_TRANSACTIONTYPE", $trans);
        $miObj->setParameter("DS_MERCHANT_TERMINAL", $terminal);
        $miObj->setParameter("DS_MERCHANT_MERCHANTURL", $url);
        $miObj->setParameter("DS_MERCHANT_PRODUCTDESCRIPTION", $desc);
        $miObj->setParameter("DS_MERCHANT_MERCHANTDATA", $codificacion);
        $miObj->setParameter("DS_MERCHANT_PAYMETHODS", "C");
        $miObj->setParameter("DS_MERCHANT_URLOK", $urlOKKO);
        $miObj->setParameter("DS_MERCHANT_URLKO", $urlOKKO);

        $accion->mensaje =$accion->mensaje.'Se guardan datos  <b style="color:green">en el objeto</b><br>';

        // Se generan los parámetros de la petición

        $parametros = $miObj->createMerchantParameters();
        $signature = $miObj->createMerchantSignature($kc);
        $dataForPost = null;
        if($ruta_tpv && $version && $parametros && $signature){
            $dataForPost = array(
                'ruta_tpv'   => $ruta_tpv,
                'Ds_SignatureVersion'   => $version,
                'Ds_MerchantParameters' => $parametros,
                'Ds_Signature'          => $signature,
            );
        }

        $method = 'POST';
        $data_string = json_encode($dataForPost);
        $accion->mensaje =$accion->mensaje.'Se realiza el envio a la <b style="color:green">plataforma de pago</b><br>';
        $accion->status = 'OK';
        $accion->save();

        // Se monta la Petición
        $url = $ruta_tpv;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_NOBODY, false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
        $result = curl_exec($ch);

        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($http_code != 200) {
            $respuesta = 'KO';
            $fErrorInterno = true;
        }
        curl_close($ch);

       return $dataForPost;
    }
}
