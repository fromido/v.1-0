<?php
/**
 * Created by PhpStorm.
 * User: fernando
 * Date: 9/12/2016
 * Time: 10:42
 */
namespace App\Libraries;

use DB;

class ApiAppPago
{
    var $idempresa = 903;
    var $idente = 0;
    var $bbddamin = 'adminTPV';
    var $bbddemp = 'tpv46903';

    //////////////////////////////
    // FUNCIONES DE VALIDACIÓN  //
    //////////////////////////////

    // Función para comprobar la Emisora
    public function comprobarEmisora($emisora)
    {
        // SE hace una consulta a la base de datos
        $select = 'select emisora from entidades where idempresa = ' . $this->idempresa . ' and idente = ' . $this->idente . ' and emisora = :codigo';
        //echo "Select emisora: " . $select;
        //echo " Emisora: " . $emisora;
        //$emisoras = DB::connection('adminTPV')->select($select, ['codigo' => $emisora]);
        $emisoras = DB::connection($this->bbddamin)->select($select, ['codigo' => $emisora]);
        if ((isset($emisoras)) && (count($emisoras) > 0)) {
            $emisbbdd = $emisoras[0];
            if ($emisora == $emisbbdd->emisora) {
                return 'ok';
            } else {
                return 'ko';
            }
        } else {
            return 'ko';
        }
    }

    // Función para comprobar el CPR
    public function comprobarCPR($emisora, $cprct)
    {
        // Se toman los Códigos de Procedimiento de Recaudación
        $select = 'select IdModalidadC60 from entidades_modc60 where IdEmpresa = ' . $this->idempresa . ' and IdEnte = ' . $this->idente . ' and Emisora = :codigo and IdModalidadC60 = :cprct';
        //echo " CPR: " .$cpr;
        //echo " Emisora: " . $emisora;
        //$cprs = DB::connection('adminTPV')->select($select, ['codigo' => $emisora, 'cprct' => $cprct]);
        $cprs = DB::connection($this->bbddamin)->select($select, ['codigo' => $emisora, 'cprct' => $cprct]);
        if ((isset($cprs)) && (count($cprs) > 0)) {
            //echo "Entro en cpr";
            $cprbbdd = $cprs[0];
            if ($cprct == $cprbbdd->IdModalidadC60) {
                return 'ok';
            }
        } else {
            return 'ko';
        }
    }

    // Función para comprobar si el Tributo está dado de alta en la Modalidad
    public function comprobarTributoMod ($emisora, $cprct, $tributo)
    {
        // Se busca el Tributo
        $select = 'select TributoC60 from entidades_modc60_tributos where IdEmpresa = ' . $this->idempresa . ' and IdEnte = ' . $this->idente . ' and Emisora = :codigo and IdModalidadC60 = :cprct and TributoC60 = :tributo';
        //echo "Select: " . $select . "<br>";
        //echo "Codigo: " . $cprct . "<br>";
        //echo "Tributo: " . $tributo . "<br>";
        //$tributos = DB::connection('adminTPV')->select($select, ['codigo' => $emisora, 'cprct' => $cprct, 'tributo' => $tributo]);
        $tributos = DB::connection($this->bbddamin)->select($select, ['codigo' => $emisora, 'cprct' => $cprct, 'tributo' => $tributo]);
        if ((isset($tributos)) && (count($tributos) > 0)) {
            $tribbbdd = $tributos[0];
            if ($tributo == $tribbbdd->TributoC60) {
                //echo 'Entro en ok';
                return 'ok';
            } else {
                //echo 'Entro en ko';
                return 'ko';
            }
        } else {
            //echo 'Entro en ko';
            return 'ko';
        }
    }

    // Función para comprobar los DC de las Modalidad 90502, 90508, 90521, 90522
    public function comprobarDC($emisora, $referencia, $identificacion, $importe, $dc)
    {
        //echo "Emisora " . $emisora;
        //echo "Referencia " . $referencia;
        //echo "Identificacion " . $identificacion;
        //echo "Importe " . $importe;
        //echo "DC " . $dc;
        // Se comprueba el Importe Auxiliar
        //$importe_aux = substr($importe,0,strlen($importe)-2) . '.' . substr($importe,strlen($importe)-2,2);
        //echo "Importe Aux " . $importe_aux;
        $uno  = $emisora*76;
        $dos  = $referencia*9;
        $tres = (($identificacion+$importe)-1)*55;
        $pre = ($uno + $dos + $tres)/97;
        $pre2 = substr(strrchr($pre, '.'), 1, 2);
        $digitos_control_aux = 99-$pre2;
        if ($digitos_control_aux==0) { $digitos_control_aux = 99; };
        //echo "Digitos Calc " . $digitos_control_aux;
        // Se comprueba si son iguales o no
        if ($dc == $digitos_control_aux) {
            return 'ok';
        } else {
            return 'ko';
        }
    }

    // Función para comprobar la Identificación y
    // tomar el Importe a pagar con la Fecha Actual
    public function tomarImporte($identImporte1, $identImporte2, $importe1, $importe2, &$identPagar, &$importePagar)
    {
        // Obtenemos la fecha de Hoy
        $juliana_hoy = gregoriantojd(date('n'), date('j'), date('Y'));
        //echo " Juliana Hoy " . $juliana_hoy;

        // Se obtiene la Fecha Límite del Importe 1
        $juliana_inicio_documento_imp1 = gregoriantojd(1, 1, "20".substr($identImporte1, 4, 2))-1;
        $juliana_limite_imp1 = $juliana_inicio_documento_imp1+substr($identImporte1, 7, 3);
        //echo " Juliana Año " . $juliana_inicio_documento_imp1;
        //echo " Juliana Imp1 " . $juliana_limite_imp1;

        // Se obtiene la Fecha Límite del Importe 2
        $juliana_inicio_documento_imp2 = gregoriantojd(1, 1, "20".substr($identImporte2, 4, 2))-1;
        $juliana_limite_imp2 = $juliana_inicio_documento_imp2+substr($identImporte2, 7, 3);
        //echo " Juliana Año " . $juliana_inicio_documento_imp2;
        //echo " Juliana Imp2 " . $juliana_limite_imp2;

        if ($juliana_limite_imp1 >= $juliana_hoy) {
            $identPagar = $identImporte1;
            $importePagar = $importe1;
            return 'ok';
        } elseif ($juliana_limite_imp2 >= $juliana_hoy) {
            $identPagar = $identImporte2;
            $importePagar = $importe2;
            return 'ok';
        } else {
            return 'ko';
        }
    }

    // Comprobar Fecha límite de Pago en la Modalidad 1 respecto a la Fecha Actual
    public function comprobarFechaLimPagoMod1($emisora, $cprct, $tributo, $ejercicio, $remesa, &$fechaLimite, &$entFinalista) {
        // Datetime now
        $fecha = date("Y-m-d");

        // Se toman los Códigos de Procedimiento de Recaudación
        $select = 'select TributoC60, FechaFinPago, EntFinalista from entidades_modc60_tributos_ppago where (IdEmpresa = 903) and (IdEnte = 0) and (Emisora = :codigo) and (IdModalidadC60 = :cprct) and (TributoC60 = :tributo) and (Ejercicio = :ejercicio) and (Remesa = :remesa) and (:fecha between FechaInicioPago and FechaFinPago)';
        //$tributos = DB::connection('adminTPV')->select($select, ['codigo' => $emisora, 'cprct' => $cprct, 'tributo' => $tributo, 'ejercicio' => $ejercicio, 'remesa' => $remesa, 'fecha' => $fecha]);
        $tributos = DB::connection($this->bbddamin)->select($select, ['codigo' => $emisora, 'cprct' => $cprct, 'tributo' => $tributo, 'ejercicio' => $ejercicio, 'remesa' => $remesa, 'fecha' => $fecha]);
        //echo "TRib. " . $tributo;
        //echo " Emisora " . $emisora;
        //echo " Cpr " . $cprct;
        //echo " ejercic. " . $ejercicio;
        //echo " Remes " . $remesa;
        //echo " Fecha " . $fecha;
        $fechaLimite = '';
        $entFinalista = '0000';
        if ((isset($tributos)) && (count($tributos) > 0)) {
            $tribbbdd = $tributos[0];
            //echo "Trib enc: " . $tribbbdd->TributoC60;
            if ($tributo == $tribbbdd->TributoC60) {
                $fechaLimiteTime = strtotime($tribbbdd->FechaFinPago);
                $fechaLimite = date("dmy", $fechaLimiteTime);
                $entFinalista = $tribbbdd->EntFinalista;
                return 'ok';
            } else {
                return 'ko';
            }
        } else {
            return 'ko';
        }
    }

    // Función para comprobar la Fecha Límite de Pago que aparece en el
    // Código de Barras de la Modalidad 2 respecto a la fecha actual
    public function comprobarFechaLimPagoMod2($identificacion) {
        //echo "Entro en comprobarFecha Lim Pago Mod 2";
        // Obtenemos la fecha de Hoy
        $juliana_hoy = gregoriantojd(date('n'), date('j'), date('Y'));

        // obtenemos la fecha juliana límite del documento
        $juliana_inicio_documento = gregoriantojd(1, 1, "20".substr($identificacion, 4, 2))-1;
        $ano_inicio = "20". substr($identificacion, 4, 2);
        $ano_fin = "20". substr($identificacion, 4, 1). substr($identificacion, 6, 1);
        if ($ano_inicio > $ano_fin) {
            $ano_fin = $ano_fin + 10;
        }
        $juliana_limite = $juliana_inicio_documento + (365*($ano_fin-$ano_inicio)) + substr($identificacion, 7 ,3);
        //$juliana_limite = $juliana_inicio_documento+substr($identificacion, 7, 3);

        if ($juliana_limite >= $juliana_hoy) {
            //echo "Entro en ok";
            return 'ok';
        } else {
            return 'ko';
        }

    }

    ///////////////////////////////////////////////
    // METODO PARA COMPROBAR SI ESTA PAGADO      //
    // DEL CODIGO DE BARRAS EN MODALIDAD 1       //
    ///////////////////////////////////////////////
    public function comprobarNoPagado ($referencia) {
        //echo "Entro en comprobar no pagado" . PHP_EOL;
        //echo "Referencia: " . $referencia . PHP_EOL;
        //$ids = DB::connection('tpv46054')->select('SELECT id FROM notificaciones WHERE codigo_respuesta < 100 AND referencia= :referencia', ['referencia' => $referencia]);
        $ids = DB::connection($this->bbddemp)->select('SELECT id FROM notificaciones WHERE codigo_respuesta < 100 AND referencia= :referencia', ['referencia' => $referencia]);
        if ((isset($ids)) && (count($ids) > 0)) {
            $id = $ids[0];
            if ($id->id > 0) {
                //echo "Devuelvo Ko No Pagado";
                return 'ko';
            } else {
                //echo "Devuelvo ok No Pagado";
                return 'ok';
            }
        } else {
            return 'ok';
        }
    }


    ///////////////////////////////////////////////////
    // Función para comprobar el Dígito de Control   //
    // Función para comprobar el Dígito de Control   //
    // en la Modalidad 90523                         //
    ///////////////////////////////////////////////////
    public function comprobarDCMod3($emisora, $justificanteSinDC, $dc)
    {
        //echo "Emisora " . $emisora . "<br>";
        //echo "Justificante " . $justificanteSinDC . "<br>";
        //echo "DC " . $dc . "<br>";
        // Se suma el Justificante y la emisora
        $uno = $justificanteSinDC + $emisora;
        //echo "Valor de uno: " . $uno . "<br>";
        $dccalc = intval(fmod($uno, 7.00));
        //echo "DC Calculado: " . $dccalc;
        // Se comprueba si son iguales o no
        if ($dc == $dccalc) {
            //echo "Devuelvo ok";
            return 'ok';
        } else {
            return 'ko';
        }
    }

    //////////////////////////////////////////////////////////
    // Función para comprobar que el Campo DocIdentidad     //
    // es correcto si tiene que ser obligatorio             //
    //////////////////////////////////////////////////////////
    public function comprobarDocIdentidad($y, $tipodocidentidad, $docidentidad)
    {
        //echo "Entro doc identidad con $y igual a " . $y;
        if ((isset($y)) && ($y >= 0) && ($y <= 8)) {
            switch ($y) {
                case 0: // No hay dato específico
                case 1: // Expediente (12p)
                case 2: // Fecha de devengo (8p) DDMMAAAA
                case 3: // Expediente (12p) y Fecha de devengo (8p)
                case 4: // Dato específico (20p)
                case 5: // Expediente (12p) y Dato específico (20p)
                case 6: // Fecha de devengo (8p) y Dato específico (20p)
                case 7: // Fecha devengo (8p), Dato Específico (20p) y Expediente (12p)
                    // Comprobar Dni
                    if ($this->DocumentoEsValido($tipodocidentidad, $docidentidad) == 'ok') {
                        //echo "Devuelvo ok";
                        return 'ok';
                    } else {
                        return 'ko';
                    }
                    break;
                case 8: // No se captura NIF ni datos opcionales
                    return 'ok';
                    break;
            }
        } else {
            return 'ko';
        }
    }

    //////////////////////////////////////////////////////////
    // Función para comprobar que el Campo FechaDevengo     //
    // es correcto si tiene que ser obligatorio             //
    //////////////////////////////////////////////////////////
    public function comprobarFechaDevengo($y, $fechaDevengo)
    {
        if ((isset($y)) && ($y >= 0) && ($y <= 8)) {
            switch ($y) {
                case 0: // No hay dato específico
                case 1: // Expediente (12p)
                    //echo "Devuelvo Fecha Devengo ok";
                    return 'ok';
                    break;
                case 2: // Fecha de devengo (8p) DDMMAAAA
                case 3: // Expediente (12p) y Fecha de devengo (8p)
                    // Comprobar que se ha introducido la Fecha de Devengo
                    if ((isset($fechaDevengo)) && (strlen($fechaDevengo) == 8)) {
                        $dia = (int)substr($fechaDevengo, 0, 2);
                        $mes = (int)substr($fechaDevengo, 2, 2);
                        $anyo = (int)substr($fechaDevengo, 4, 4);
                        if (checkdate($mes, $dia, $anyo)) {
                            return 'ok';
                        } else {
                            return 'ko';
                        }
                    } else {
                        return 'ko';
                    }
                    break;
                case 4: // Dato específico (20p)
                case 5: // Expediente (12p) y Dato específico (20p)
                    return 'ok';
                    break;
                case 6: // Fecha de devengo (8p) y Dato específico (20p)
                case 7: // Fecha devengo (8p), Dato Específico (20p) y Expediente (12p)
                    // Comprobar que se ha introducido la Fecha de Devengo
                    if ((isset($fechaDevengo)) && (strlen($fechaDevengo) == 8)) {
                        $dia = (int)substr($fechaDevengo, 0, 2);
                        $mes = (int)substr($fechaDevengo, 2, 2);
                        $anyo = (int)substr($fechaDevengo, 4, 4);
                        if (checkdate($mes, $dia, $anyo)) {
                            return 'ok';
                        } else {
                            return 'ko';
                        }
                    } else {
                        return 'ko';
                    }
                    break;
                case 8: // No se captura NIF ni datos opcionales
                    return 'ok';
                    break;
            }
        } else {
            return 'ko';
        }
    }

    //////////////////////////////////////////////////////////
    // Función para comprobar que el Campo Expediente       //
    // es correcto si tiene que ser obligatorio             //
    //////////////////////////////////////////////////////////
    public function comprobarExpediente($y, $expediente)
    {
        //echo "Entro en Expediente";
        if ((isset($y)) && ($y >= 0) && ($y <= 8)) {
            switch ($y) {
                case 0: // No hay dato específico
                    //echo "Devuelvo Expediente ok";
                    return 'ok';
                    break;
                case 1: // Expediente (12p)
                    // Comprobar que se ha introducido un valor en el Expediente
                    if ((isset($expediente)) && ($expediente != '')) {
                        return 'ok';
                    } else {
                        return 'ko';
                    }
                    break;
                case 2: // Fecha de devengo (8p) DDMMAAAA
                    return 'ok';
                    break;
                case 3: // Expediente (12p) y Fecha de devengo (8p)
                    // Comprobar que se ha introducido un valor en el Expediente
                    if ((isset($expediente)) && ($expediente != '')) {
                        return 'ok';
                    } else {
                        return 'ko';
                    }
                    break;
                case 4: // Dato específico (20p)
                    return 'ok';
                    break;
                case 5: // Expediente (12p) y Dato específico (20p)
                    // Comprobar que se ha introducido un valor en el Expediente
                    if ((isset($expediente)) && ($expediente != '')) {
                        return 'ok';
                    } else {
                        return 'ko';
                    }
                    break;
                case 6: // Fecha de devengo (8p) y Dato específico (20p)
                    return 'ok';
                    break;
                case 7: // Fecha devengo (8p), Dato Específico (20p) y Expediente (12p)
                    // Comprobar que se ha introducido un valor en el Expediente
                    if ((isset($expediente)) && ($expediente != '')) {
                        return 'ok';
                    } else {
                        return 'ko';
                    }
                    break;
                case 8: // No se captura NIF ni datos opcionales
                    return 'ok';
                    break;
            }
        } else {
            return 'ko';
        }
    }

    //////////////////////////////////////////////////////////
    // Función para comprobar que el Campo Expediente       //
    // es correcto si tiene que ser obligatorio             //
    //////////////////////////////////////////////////////////
    public function comprobarDatoEspecifico($y, $datoespecifico)
    {
        //echo "Entro en datoespecifico";
        if ((isset($y)) && ($y >= 0) && ($y <= 8)) {
            switch ($y) {
                case 0: // No hay dato específico
                case 1: // Expediente (12p)
                case 2: // Fecha de devengo (8p) DDMMAAAA
                case 3: // Expediente (12p) y Fecha de devengo (8p)
                    //echo "Entro en opción 3" . "<br>";
                    return 'ok';
                    break;
                case 4: // Dato específico (20p)
                case 5: // Expediente (12p) y Dato específico (20p)
                case 6: // Fecha de devengo (8p) y Dato específico (20p)
                case 7: // Fecha devengo (8p), Dato Específico (20p) y Expediente (12p)
                    // Comprobar que se ha introducido el Dato específico
                    if ((isset($datoespecifico)) && ($datoespecifico != '')) {
                        return 'ok';
                    } else {
                        return 'ko';
                    }
                    break;
                case 8: // No se captura NIF ni datos opcionales
                    return 'ok';
                    break;
            }
        } else {
            return 'ko';
        }
    }


    //////////////////////////////////////////////////////////////
    // Función para comprobar si el Tipo de Documento es válido //
    //////////////////////////////////////////////////////////////
    public function DocumentoEsValido($tipoDocumento, $documento)
    {
        //echo "Tipo: " . $tipoDocumento . '<br>';
        //echo "Documento: " . $documento . '<br>';
        // Si el tipo de documento o el documento están vacíos, se devuelve error
        if (($tipoDocumento == '') || ($documento == '')) {
            return "ko";
        }
        // Si la longitud es mayor que 9, se devuelve error
        if (strlen($documento) > 9) {
            return "ko";
        }
        // Si comienza por dos letras, se devuelve error
        if ((ctype_alpha(substr($documento, 0, 1))) && (ctype_alpha(substr($documento, 1, 1)))) {
            return "ko";
        }
        // Si el documento es distino de Pasaporte, si tiene letras en el interior, se devuelve error
        if ($tipoDocumento != 'P') {
            for ($i = 1; $i < strlen($documento)-1; $i++) {
                if (ctype_alpha(substr($documento, $i, 1))) {
                    return "ko";
                }
            }
        }
        // Se estructura el Documento
        $letraInicial = '';
        $docNumeroStr = '';
        $letraFinal = '';
        $this->EstructurarDNI($documento, $letraInicial, $docNumeroStr, $letraFinal);
        //echo "Letra Inicial: " . $letraInicial . "<br>";
        //echo "Documento: " . $docNumeroStr . "<br>";
        //echo "Letra Final: " . $letraFinal . "<br>";

        // Por el tipo de Documento, se hacen las Comprobaciones
        // NIF
        if ($tipoDocumento == "N") {
            //echo "Entro por nif" . "<br>";
            if ((($letraInicial == "M") || ($letraInicial == "L") || ($letraInicial == "K")) && ($letraFinal == "")) {
                //echo "KO por letra inicial M, L o K sin Letra Final" . "<br>";
                return "ko";
            } elseif ((($letraInicial == "") && ($letraInicial == "L")) || ($letraFinal != $this->CalcularLetraDni($docNumeroStr))) {
                //echo "KO por letra final distinta de la Calculada" . "<br>";
                return "ko";
            } elseif ($letraInicial != "") {
                //echo "Ko por letra inicial distinca de vacia" . "<br>";
                return "ko";
            }
            //echo "Salgo de nif" . "<br>";
            // CIF
        } elseif ($tipoDocumento == "C") {
            //echo "Entro por cif" . "<br>";
            // Se comprueba la letra Inicial
            if (($letraInicial != "A") && ($letraInicial != "B") && ($letraInicial != "C") &&
                ($letraInicial != "D") && ($letraInicial != "E") && ($letraInicial != "F") &&
                ($letraInicial != "G") && ($letraInicial != "H") && ($letraInicial != "N") &&
                ($letraInicial != "P") && ($letraInicial != "Q") && ($letraInicial != "S") &&
                ($letraInicial != "J") && ($letraInicial != "U") && ($letraInicial != "V") &&
                ($letraInicial != "R") && ($letraInicial != "W")) {
                //echo "Letra distinta de las seleccionadas" . "<br>";
                return "ko";
            } elseif (cifDigito($documento) != substr($documento, strlen($documento)-1, 1)) {
                //echo "Cif Digito Calculado distinto" . "<br>";
                return "ko";
            }
            // NIE
        } elseif ($tipoDocumento == "T") {
            // Se comprueba la Letra Inicial
            if (($letraInicial != "X") && ($letraInicial != "Y") && ($letraInicial != "Z")) {
                return "ko";
            } else {
                if ($letraFinal == "") {
                    return "ko";
                } elseif (($letraFinal == "X") && ($this->CalcularLetraDni("0".$docNumeroStr) != $letraFinal)) {
                    return "ko";
                } elseif (($letraFinal == "Y") && ($this->CalcularLetraDni("1".$docNumeroStr) != $letraFinal)) {
                    return "ko";
                } elseif (($letraFinal == "Z") && ($this->CalcularLetraDni("2".$docNumeroStr) != $letraFinal)) {
                    return "ko";
                }
            }
        } elseif ($tipoDocumento == "P") {
            return "ok";
        } elseif ($tipoDocumento == "O") {
            return "ok";
        } else {
            return "ko";
        }
        //echo "Resultado: ok" . "<br>";
        return "ok";
    }

    ///////////////////////////////////////
    // Función para estructurar un Dni   //
    ///////////////////////////////////////
    public function EstructurarDNI($documento, &$cif, &$dni, &$nif)
    {
        //echo "Entro en  EstructurarDni" . '<br>';
        // Si no hay documento, devuelve vacío
        if ($documento == '') {
            $cif = '';
            $dni = '';
            $nif = '';
            return 'ok';
        }
        // Se transforma a mayúscula los caracteres
        $documento = strtoupper($documento);
        //echo "Documento: " . $documento . '<br>';
        // Si el primer dígito es un carácter
        if (ctype_alpha(substr($documento, 0, 1))) {
            //echo "Entro por 1º Digito Alfanúmerico" . '<br>';
            $cif = substr($documento, 0, 1);
            // Si el último dígito es un carácter
            if (ctype_alpha(substr($documento, strlen($documento)-1, 1))) {
                $dni = substr($documento, 1, strlen($documento)-2);
                $nif = substr($documento, substr($documento, strlen($documento)-1, 1));
                // Si el último dígito no es un carácter -- CIF Normal
            } else {
                $dni = substr($documento, 1, strlen($documento)-1);
                $nif = "";
            }
            // Si el primer dígito no es un carácter, no es un Cif
        } else {
            //echo "Entro por 1º Digito Numérico" . '<br>';
            $cif = "";
            // Si el último dígito es un carácter
            if (ctype_alpha(substr($documento, strlen($documento)-1, 1))) {
                //echo "Entro por último Digito Alfanúmerico" ."<br>";
                $dni = substr($documento, 0, strlen($documento)-1);
                $nif = substr($documento, strlen($documento)-1, 1);
                // Si el último dígito no es un carácter -- CIF Normal
            } else {
                //echo "Entro por último Dígito Numérico";
                $dni = $documento;
                $nif = '';
            }
        }
        //echo "Cif: " . $cif . '<br>';
        //echo "Dni: " . $dni . '<br>';
        //echo "Nif: " . $nif . '<br>';
    }

    ////////////////////////////////////////////////
    // Función para calcular la letra del DNI     //
    ////////////////////////////////////////////////
    public function CalcularLetraDni($docNumeroStr)
    {

        //echo "Entro a calcular la letra del Dni " . "<br>";
        //echo "Doc. Identidad: |" . $docNumeroStr . "|<br>";
        // Tabla de letras para comparar
        $tablaLetras = "TRWAGMYFPDXBNJZSQVHLCKE";
        // Se inicializa la Variable
        $letraDni = "";

        // Si el Número de Documento es vacío, se devuelve vacío
        if (trim($docNumeroStr) == "") {
            //echo "Doc Identidad vacio" . "<br>";
            return $letraDni;
        }

        // Si no es numérico, se devuelve vacío
        if (ctype_digit($docNumeroStr)) {
            //echo "Doc. Identidad correcto " . "<br>";
        } else {
            //echo "Doc Identidad con caracteres alfanumericos" . "<br>";
            return $letraDni;
        }

        // Se toma el Numero
        $numero = (int)("0" . $docNumeroStr);
        //echo "Numero calculado " . $numero . "<br>";
        // Si el número es inferior a 23, se devuelve vacío
        if ($numero < 23) {
            //echo "Numero inferior a 23" . "<br>";
            return $letraDni;
        }
        // Se calcula el resto (numero = (numero mod 23) + 1)
        $numero = ($numero % 23);
        //echo "Número calculado tras últimas operaciones " . $numero . "<br>";
        // Se busca en la Tabla de letras la asociada a la posición del número
        $letraDni = substr($tablaLetras, $numero,1);

        // Se devuelve la letra
        //echo "Letra Dni Calculada: " . $letraDni . "<br>";
        return $letraDni;
    }

    //////////////////////////////////////////////
    // Función para calcular la letra del Cif   //
    //////////////////////////////////////////////
    public function cifDigito($cif)
    {
        //echo "Entro en calcular CifDigito " . "<br>";
        // Inicialización de Variables
        $letras = "JABCDEFGHIJ";
        $A = 0;
        $B = 0;
        $i=  1;

        // Se recorre el Cif
        while ($i <= 6) {
            // Suma de posición impar
            $A = $A + substr($cif, $i+1, 1);
            // Doble de posición par
            $C = 2 * substr($cif, $i, 1);
            // Suma de dígitos de doble de pares
            $B = $B + ($C % 10) + (int)($C / 10);
            // Se suma el dígito
            $i = $i + 2;
        }
        //echo "Valor A: " . $A . "<br>";
        //echo "Valor B: " . $B . "<br>";
        //echo "Valor C: " . $C . "<br>";

        // Se hacen los últimos Cálculos
        $val1 = (2 * (int)(substr($cif, 7, 1))) % 10;
        //echo "Valor 1: " . $val1 . "<br>";
        $val2 = (int)((2 * (int)(substr($cif, 7, 1))) / 10);
        //echo "Valor 2: " . $val2 . "<br>";
        $B = $B + $val1 + $val2;
        $C = ((10 - ((($A + $B) % 10))) % 10);
        //echo "Ultimo Valor B: " . $B . "<br>";
        //echo "Ultimo Valor C: " . $C . "<br>";

        // Se calcula el Dígito
        $digito = "";
        $car = substr($cif, 0, 1);
        //echo "Caracter a considerar: " . $car . "<br>";
        $pos = strpos("NPQRSW", $car);
        //echo "Valor de pos: " . $pos . "<br>";
        if ($pos === false) {
            //echo "Entro a comprobar ABCDEFGHJUV" . "<br>";
            $pos2 = strpos("ABCDEFGHJUV", $car);
            if ($pos2 === false) {
                $digito = '';
            } else {
                $digito = $C;
            }
        } else {
            $digito = substr($letras, $C, 1);
        }
        //echo "Digito calculado: " . $digito . "<br>";
        return $digito;
    }

}