<?php

namespace App\Validation;
use Illuminate\Validation\Validator as LaravelValidator;
use App\Http\Requests\ValidatorRequest;

class Validator extends LaravelValidator{

    public function validateFecDevengo($attribute, $value,$parameters){
        dd('validateFecDevengo');

        //Comprobar que la fecha devengo es una fecha válida
        if (len($parameters[0]==8)){
            $dia = substr($value, 0, 2);
            $mes = substr($value, 2, 2);
            $anio = substr($value, 4, 4);
            if (checkdate($mes, $dia, $anio)) {
                return  true;
            };
            return false;
        }
        return false;
    }

    public function validateNumJustificante($attribute, $value,$parameters){
        dd('validateNumJustificante');
        if (ValidatorRequest::getTributo(substr($value, 0, 3), $parameters[0])) {
            if (ValidatorRequest::comprobarDC($value,3)){
                return false;
            }
        }
        return false;
    }
}
