<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Acciones</title>
    <script src="<?=asset('js/jquery.min.js')?>"></script>
</head>
<style>
    body {
        font-family: "Helvetica Neue", Helvetica, Arial;
        font-size: 14px;
        line-height: 20px;
        font-weight: 400;
        color: #3b3b3b;
        -webkit-font-smoothing: antialiased;
        font-smoothing: antialiased;
        background: #2b2b2b;
    }

    @media screen and (max-width: 580px) {
        body {
            font-size: 16px;
            line-height: 22px;
        }
    }

    .wrapper {
        margin: 0 auto;
        padding: 40px;
        max-width: 800px;
    }

    .table {
        margin: 0 0 40px 0;
        width: 100%;
        box-shadow: 0 1px 3px rgba(0, 0, 0, 0.2);
        display: table;
    }

    @media screen and (max-width: 580px) {
        .table {
            display: block;
        }
    }

    .row {
        display: table-row;
        background: #f6f6f6;
    }

    .row:nth-of-type(odd) {
        background: #e9e9e9;
    }

    .row.header {
        font-weight: 900;
        color: #ffffff;
        background: #ea6153;
    }

    .row.green {
        background: #27ae60;
    }

    .row.blue {
        background: #2980b9;
    }

    @media screen and (max-width: 580px) {
        .row {
            padding: 14px 0 7px;
            display: block;
        }

        .row.header {
            padding: 0;
            height: 6px;
        }

        .row.header .cell {
            display: none;
        }

        .row .cell {
            margin-bottom: 10px;
        }

        .row .cell:before {
            margin-bottom: 3px;
            content: attr(data-title);
            min-width: 98px;
            font-size: 10px;
            line-height: 10px;
            font-weight: bold;
            text-transform: uppercase;
            color: #969696;
            display: block;
        }
    }

    .cell {
        padding: 6px 12px;
        display: table-cell;
    }

    @media screen and (max-width: 580px) {
        .cell {
            padding: 2px 16px;
            display: block;
        }
    }
</style>
<body>
<div class="wrapper">
    @foreach ($acciones as $key => $accion)
        <div class="table">
            <div class="
            @if($accion->status == 'ok' || $accion->status == 'OK')
                row header green
            @endif
            @if($accion->status == 'ko' || $accion->status == 'KO')
                row header
            @endif">
                <div class="cell">
                    id
                </div>
                <div class="cell">
                    ID Ayuntamiento
                </div>
                <div class="cell">
                    id App
                </div>
                <div class="cell">
                    Nombre de la accion
                </div>
                <div class="cell">
                    Status
                </div>
                <div class="cell">
                    Mensaje
                </div>
                <div class="cell">
                    fecha
                </div>
            </div>

            <div class="row">
                <div class="cell" data-title="id">
                    {{$accion->id}}
                </div>
                <div class="cell" data-title="ID Ayuntamiento">
                    {{$accion->id_ayuntamiento}}
                </div>
                <div class="cell" data-title="id App">
                    {{$accion->id_app}}
                </div>
                <div class="cell" data-title="Nombre de la accion">
                    {{$accion->nombre_accion}}
                </div>

                <div class="cell" data-title="Status">
                    {{$accion->status}}
                </div>

                <div class="cell" data-title="Mensaje">
                    <span id="{{$accion->id}}_mensaje">
                        {{$accion->mensaje}}
                    </span>
                </div>
                <div class="cell" data-title="Fecha">
                    {{$accion->created_at}}
                </div>
            </div>
        </div>
    @endforeach
</div>
<script type="application/javascript">
    var acciones = <?php echo $acciones;?>;

        for(let i = 1; i < acciones.length; i++){
            var id_temp = '#'+acciones[i]['id']+'_mensaje';
            element = document.getElementById(acciones[i]['id']+'_mensaje').innerHTML;
            console.log(element);
            $(id_temp).empty();
            $( id_temp ).append(''+element+'')
            //$(id_temp).append(element);
        }

</script>
</body>
</html>
