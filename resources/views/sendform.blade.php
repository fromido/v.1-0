<div class="flex-center position-ref full-height" style="display: none">
    @if ($url)
    <form name ="formSendParams" id="formSendParams" action="{!! $url !!}" method="POST">
        @csrf
        <input name="Ds_SignatureVersion" type="hidden" value="{!! $Ds_SignatureVersion !!}">
        <input name="Ds_MerchantParameters" type="hidden" value="{!! $Ds_MerchantParameters !!}">
        <input name="Ds_Signature" type="hidden" value="{!! $Ds_Signature !!}">
    </form>
    @endif
</div>
<script>
    document.forms["formSendParams"].submit();
</script>
