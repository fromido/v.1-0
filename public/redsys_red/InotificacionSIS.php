<?php
/*
include_once 'apiRedsys.php';
$miObj = new RedsysAPI;
*/

///////////////////////////////////////////////////////////////////
// Importación de la librería para consumir los Servicios        //
///////////////////////////////////////////////////////////////////
require '../../vendor/autoload.php';

////////////////////////////////////////////////
// Montaje del Servidor SOAP de Notificación  //
// El Servidor se arranca por cada petición,  //
// no está arrancado                          //
////////////////////////////////////////////////
// Se deshabilita la Cache WSDL
//ini_set("soap.wsdl_cache_enabled", "0");
// Se crea el Servidor
$server = new SoapServer("INotificacionSIS.wsdl");
// Se le añade la Función para Procesar las Notificaciones
$server->addFunction("procesaNotificacionSIS");
// Se lanza el Servidor
$server->handle();
/////////////////////////////////////////////////////////////////
// Función para procesar la Notificación que llega al Servidor //
/////////////////////////////////////////////////////////////////
function procesaNotificacionSIS($xml_entrada) {
    ///////////////////////////////////////////////
    // Se colocan la Empresa y la Entidad        //
    ///////////////////////////////////////////////
    /// ELIMINAR , NO ES NECESARIO SE ENVIARA CODIFICADA EN REDSYS
    //$empresa = 903;
    //$entidad = 0;

    ///////////////////////////////////////////////
    // Variable para marcar nuestros Errores     //
    ///////////////////////////////////////////////
    $fErrorInterno = false;

    ///////////////////////////////////////////////
    // Includes de Objetos de Redsys y Errores   //
    ///////////////////////////////////////////////
    /// INCLUIR SOLAMENTE OBJETOS DE REDSYS
    include_once 'apiRedsys.php';
    //
    include 'errores_tpv.php';

    ///////////////////////////////
    // Abrimos Fichero de Log    //
    ///////////////////////////////

    ///////////////////////////////////////////////
    // Creamos el Objeto de la API de Redsys     //
    ///////////////////////////////////////////////
    $miObj = new RedsysAPI;


    //////////////////////////////////////////////////////
    /// Se genera la Firma con la Clave de Producción    //
    //////////////////////////////////////////////////////
    // Clave de Producción recuperada de Canales
    $claveModuloAdmin = '';
    // Clave de Test recuperada de Canales
    $claveModuloAdmin = 'sq7HjrUOBfKmC576ILgskD5srU870gJ7';
    // Clave de Producción recuperada de Canales
    //$claveModuloAdmin = 'JT+CHkxsBP2SCXnVK0lg52F+WOod+grS';
    $signatureCalculada = $miObj->createMerchantSignatureNotifSOAPRequest($claveModuloAdmin, $xml_entrada);
    // Se escribe en el Fichero Log

    //fwrite($file, "signatureCalculada: " . $signatureCalculada . PHP_EOL);
    $signatureRecibida = getSignatureNotifSOAP($xml_entrada);
    //fwrite($file, "signature Recibida: " . $signatureRecibida . PHP_EOL);

    $file = fopen('log_' . date('dmY') . '.txt', "a");
    fwrite($file, '------------------------------------' . PHP_EOL);

    ////////////////////////////////////////////////
    // Firma recibida OK con la calculada         //
    ////////////////////////////////////////////////

    if ($signatureCalculada == $signatureRecibida) {

        $requestPeticion = getRequestNotifSOAP($xml_entrada);
        $numPedido = $miObj->getOrderNotifSOAP($xml_entrada);

        // Se montan los Parámetros
        $method = 'POST';
        $datos = array(
            "data" => $requestPeticion
        );
        $data_string = json_encode($datos);
        // Se monta la Petición


        $url = 'http://tpv-generico.gestionmunicipal.es/api/post/notificacionRedsysok';

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_NOBODY, false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
            $ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string)
            )
        );
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
        //$result = curl_exec($ch);


        $response = curl_exec($ch);
        $error = curl_error($ch);
        $result = array( 'header' => '',
                         'body' => '',
                         'curl_error' => '',
                         'http_code' => '',
                         'last_url' => '');
        if ( $error != "" ){
            $result['curl_error'] = $error;
            fwrite($file, "error: " . json_encode($error) . PHP_EOL);
        }else{
            $header_size = curl_getinfo($ch,CURLINFO_HEADER_SIZE);
            $result['header'] = substr($response, 0, $header_size);
            $result['body'] = substr( $response, $header_size );
            $result['http_code'] = curl_getinfo($ch,CURLINFO_HTTP_CODE);
            $result['last_url'] = curl_getinfo($ch,CURLINFO_EFFECTIVE_URL);
            fwrite($file, "Codigo: " . json_encode($result['body']) . PHP_EOL);
        }

        // Se toma el Código http
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        //fwrite($file, "despues de la funciuon" . PHP_EOL);

        // Si el resultado es erróneo
        $respuesta = 'KO';
        if ($http_code == 200 && $result['body'] == 'OK') {
            $respuesta = 'OK';
            $fErrorInterno = false;
            fwrite($file, "Resultado Actualización RCPagoOnline -- OK" . PHP_EOL);
        } else {
            fwrite($file, "Resultado Actualización RCPagoOnline -- Error" . PHP_EOL);
            $respuesta = 'KO';
            $fErrorInterno = true;
            return false;
        }
        curl_close($ch);

        ////////////////////////////////////////////////////
        // Generamos la firma de la respuesta             //
        ////////////////////////////////////////////////////
        $response = '<Response Ds_Version="0.0"><Ds_Response_Merchant>' . $respuesta . '</Ds_Response_Merchant></Response>';
        $firmaRespuesta = $miObj->createMerchantSignatureNotifSOAPResponse($claveModuloAdmin, $response, $numPedido);
        ///////////////////////////////////////////////////
        // Generamos el XML de salida                    //
        ///////////////////////////////////////////////////
        $xml_salida = '<Message>' . $response . '<Signature>' . $firmaRespuesta . '</Signature></Message>';
        //$xml_salida = '<Message><Response Ds_Version="0.0"><Ds_Response_Merchant>OK</Ds_Response_Merchant></Response><Signature>d/VtqOzNlds9MTL/QO12TvGDNT+yTfawFlg55ZcjX9Q=</Signature></Message>';
        // Se devuelve
        return $xml_salida;

        if (count($chorizo) == 2) {
            fwrite($file, "Origen: Ventanilla" . PHP_EOL);
            //pregunto a pascual
            $id_online = (int)$chorizo[0];
            $codbar = $chorizo[1];
            $referencia = ReferenciaDesdeCodBar($codbar);
            $emisora = EmisoraDesdeCodbar($codbar);
            ///////////////////////////////////////////////////////
            // Se llama al Servicio que actualiza RCPagoOnline   //
            ///////////////////////////////////////////////////////
            $resp_redSys = $requestPeticion['Ds_Response'];
            // Se montan los Parámetros
            $method = 'PUT';
            $data = array(
                "IdPagoOnline" => $id_online,
                "Resultado"    => "$resp_redSys",
                "Origen"       => "ventanilla",
                "Idioma"       => "vlc",
                "IdEmpresa"    => "$empresa",
                "IdEnte"       => "$entidad",
                "Referencia"   => $referencia,
                "Emisora"      => "$emisora"
            );
            $data_string = json_encode($data);
            fwrite($file, "SQL: " . $data_string . PHP_EOL);
            // Se monta la Petición
            $url = 'http://10.254.0.26:9001/PagosOnline/v1/903/' . $id_online;
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLOPT_NOBODY, false);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt(
                $ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string)
                )
            );
            curl_setopt($ch, CURLOPT_TIMEOUT, 0);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
            $result = curl_exec($ch);
            // Se toma el Código http
            $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            fwrite($file, "Codigo: " . $http_code . PHP_EOL);
            fwrite($file, "Resultado: " . $result . PHP_EOL);
            // Si el resultado es erróneo
            if ($http_code == 200) {
                fwrite($file, "Resultado Actualización RCPagoOnline -- OK" . PHP_EOL);
            } else {
                fwrite($file, "Resultado Actualización RCPagoOnline -- Error" . PHP_EOL);
                $mensaje_salida_mail = "ERROR: " . $errores_tpv['pago_OKKO_mensaje_KO'];
                $respuesta = 'KO';
                $fErrorInterno = true;
            }
            curl_close($ch);
            ////////////////////////////////////////
            // Origen distinto de Ventanilla      //
            ////////////////////////////////////////
        } else {
            //////////////////////////////////////////////
            // Se toma el Origen y todos los datos     ///
            //////////////////////////////////////////////
            $origen = explode("origen=", $chorizo[0]);
            $tipoenvio = explode("tipoenvio=", $chorizo[1]);
            $email = explode("email=", $chorizo[2]);
            $telefono = explode("telefono=", $chorizo[3]);
            $codbar = explode("codbar=", $chorizo[4]);
            $referencia = ReferenciaDesdeCodbar($codbar[1]);
            $idioma = explode("idioma=", $chorizo[5]);
            $emisora = EmisoraDesdeCodbar($codbar[1]);
            if (isset($tipoenvio[1])) {
                $tipoenv = $tipoenvio[1];
            } else {
                $tipoenv = '0';
            }
            if (isset($email[1])) {
                $email = $email[1];
            } else {
                $email = '';
            }
            if (isset($telefono[1])) {
                $tlf = $telefono[1];
            } else {
                $tlf = '0';
            }
            fwrite($file, "Origen: " . $origen[1] . PHP_EOL);
            fwrite($file, "Tipo Envio: " . $tipoenv . PHP_EOL);
            fwrite($file, "Referencia: " . $referencia . PHP_EOL);
            fwrite($file, "Idioma: " . $idioma[1] . PHP_EOL);
            fwrite($file, "Emisora: " . $emisora . PHP_EOL);
            ///////////////////////////////////
            // Paralizamos la cuota en SIMEP //
            ///////////////////////////////////
            // Si la operación fue correcta
            if ((int)$requestPeticion['Ds_Response'] == 0 && $origen[1] != 'tributaria') {
                // Se montan los Parámetros
                $method = 'POST';
                $data = array(
                    "idEmpresa"     => "$empresa",
                    "referenciaDoc" => $referencia,
                    "Emisora"       => "$emisora",
                    "codigo"        => "PT",
                    "motivoPar"     => "Pago con tarjeta",
                    "origen"        => "$origen[1]"
                );
                $data_string = json_encode($data);
                fwrite($file, "Intentamos paralizar la couta en SIMEP: " . $data_string . PHP_EOL);
                // Se monta la Petición
                $url = 'http://10.254.0.26:9029/paralizaCuota/v1';
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_HEADER, true);
                curl_setopt($ch, CURLOPT_NOBODY, false);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt(
                    $ch, CURLOPT_HTTPHEADER,
                    array('Content-Type: application/json', 'Content-Length: ' . strlen($data_string))
                );
                curl_setopt($ch, CURLOPT_TIMEOUT, 0);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
                $result = curl_exec($ch);
                // Se toma el Código http
                $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                fwrite($file, "Codigo: " . $http_code . PHP_EOL);
                fwrite($file, "Resultado: " . $result . PHP_EOL);
                // Si el resultado es erróneo
                if ($http_code == 200) {
                    fwrite($file, "Resultado Paralización de cuota -- OK" . PHP_EOL);
                } else {
                    fwrite($file, "Resultado Paralización de cuota -- Error" . PHP_EOL);
                    $mensaje_salida_mail = "ERROR: " . $errores_tpv['pago_OKKO_mensaje_KO'];
                    $respuesta = 'KO';
                    $fErrorInterno = true;
                }
                curl_close($ch);
            }

        }

        if (!$fErrorInterno) {
            fwrite($file, "Insertar Notificaciones tras Operación" . PHP_EOL);
            try {
                // Insertamos en la tabla que controla el origen de la petición para devolver al lugar adecuado cuando termine
                $sql2 = "INSERT INTO notificaciones (`fecha`, `hora`, `importe`, `moneda`, `numero_pedido`, `codigo_fuc`, `terminal`, `codigo_respuesta`, `datos_comercio`, `pago_seguro`, `tipo_operacion`, `pais`, `codigo_autorizacion`, `idioma`, `referencia`, `emisora`) VALUES ('" . $requestPeticion['Fecha'] . "', '" . $hora_minutos . "', '" . $requestPeticion['Ds_Amount'] . "', '" . $requestPeticion['Ds_Currency'] . "', '" . $requestPeticion['Ds_Order'] . "', '" . $requestPeticion['Ds_MerchantCode'] . "', '" . $requestPeticion['Ds_Terminal'] . "', '" . $requestPeticion['Ds_Response'] . "', '" . $requestPeticion['Ds_MerchantData'] . "', '" . $requestPeticion['Ds_SecurePayment'] . "', '" . $requestPeticion['Ds_TransactionType'] . "', '" . $requestPeticion['Ds_Card_Country'] . "', '" . $requestPeticion['Ds_AuthorisationCode'] . "', '" . $requestPeticion['Ds_ConsumerLanguage'] . "','" . $referencia . "','" . $emisora . "')";
                fwrite($file, "SQL Inserción Notificaciones: " . $sql2 . PHP_EOL);
                $res2 = mysqli_query($con, $sql2);
                // Si no se ha podido hacer la inserción, entonces se manda un KO
                if ($res2 > 0) {
                    fwrite($file, "Resultado inserción Notificaciones -- OK" . PHP_EOL);
                } else {
                    fwrite($file, "Resultado inserción Notificaciones -- KO" . PHP_EOL);
                    $mensaje_salida_mail = "ERROR: " . $errores_tpv['pago_OKKO_mensaje_KO'];
                    $respuesta = 'KO';
                }
            } catch (Exception $exc) {
                fwrite($file, "Resultado inserción Notificaciones -- KO por " . $exc . PHP_EOL);
                $mensaje_salida_mail = "ERROR: " . $errores_tpv['pago_OKKO_mensaje_KO'];
                $respuesta = 'KO';
            }
        } else {
            fwrite($file, "Insertar Notificaciones tras Error Interno" . PHP_EOL);
            try {
                // Insertamos en la tabla que controla el origen de la petición para devolver al lugar adecuado cuando termine
                $sql2 = "INSERT INTO notificaciones (`fecha`, `hora`, `importe`, `moneda`, `numero_pedido`, `codigo_fuc`, `terminal`, `codigo_respuesta`, `datos_comercio`, `pago_seguro`, `tipo_operacion`, `pais`, `codigo_autorizacion`, `idioma`, `referencia`, `emisora`) VALUES ('" . $requestPeticion['Fecha'] . "', '" . $hora_minutos . "', '" . $requestPeticion['Ds_Amount'] . "', '" . $requestPeticion['Ds_Currency'] . "', '" . $requestPeticion['Ds_Order'] . "', '" . $requestPeticion['Ds_MerchantCode'] . "', '" . $requestPeticion['Ds_Terminal'] . "', '" . $requestPeticion['Ds_Response'] . "', '" . $requestPeticion['Ds_MerchantData'] . "', '" . $requestPeticion['Ds_SecurePayment'] . "', '" . $requestPeticion['Ds_TransactionType'] . "', '" . $requestPeticion['Ds_Card_Country'] . "', '" . $requestPeticion['Ds_AuthorisationCode'] . "', '" . $requestPeticion['Ds_ConsumerLanguage'] . "','" . $referencia . "','" . $emisora . "')";
                fwrite($file, "SQL Inserción Notificaciones: " . $sql2 . PHP_EOL);
                $res2 = mysqli_query($con, $sql2);
                // Si no se ha podido hacer la inserción, entonces se manda un KO
                if ($res2 > 0) {
                    fwrite($file, "Resultado inserción Notificaciones -- OK" . PHP_EOL);
                } else {
                    fwrite($file, "Resultado inserción Notificaciones -- KO" . PHP_EOL);
                    $mensaje_salida_mail = "ERROR: " . $errores_tpv['pago_OKKO_mensaje_KO'];
                    $respuesta = 'KO';
                }
            } catch (Exception $exc) {
                fwrite($file, "Resultado inserción Notificaciones -- KO por " . $exc . PHP_EOL);
                $mensaje_salida_mail = "ERROR: " . $errores_tpv['pago_OKKO_mensaje_KO'];
                $respuesta = 'KO';
            }
        }
    } else {

        $method = 'POST';
        $datos = array(
            "data" =>array (
                'fecha'=>date("d/m/y"),
                'datos_comercio'=>'FIRMA KO',
            )
        );
        $data_string = json_encode($datos);
        // Se monta la Petición


        $url = 'http://tpv-generico.gestionmunicipal.es/api/post/notificacionRedsysko';

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_NOBODY, false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
            $ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string)
            )
        );
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
        $result = curl_exec($ch);
        // Se toma el Código http
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        fwrite($file, "Codigo: " . $data_string . PHP_EOL);
        fwrite($file, "despues de la funciuon" . PHP_EOL);

        // Si el resultado es erróneo
        $respuesta = 'KO';
        if ($http_code == 200) {
            $respuesta = 'OK';
            $fErrorInterno = false;
            //fwrite($file, "Resultado Actualización RCPagoOnline -- OK" . PHP_EOL);
        } else {
            //fwrite($file, "Resultado Actualización RCPagoOnline -- Error" . PHP_EOL);
            $respuesta = 'KO';
            $fErrorInterno = true;
        }
        curl_close($ch);

    }
}

////////////////////////////////////////////////////////
// Función para recuperar la Firma de los datos XML   //
////////////////////////////////////////////////////////
function getSignatureNotifSOAP($datos) {
    $z = new XMLReader;
    $z->xml($datos);
    while ($z->read()) {
        if (($z->nodeType == XMLReader::ELEMENT) && ($z->name == 'Message')) {
            $zz = new SimpleXMLElement($z->readOuterXml());
            return $zz->Signature;
        }
    }
}

////////////////////////////////////////////////////////
// Función para Recuerperar los Datos de Notificación //
// del Merchant Data                                  //
////////////////////////////////////////////////////////
function getRequestNotifSOAP($datos) {
    $z = new XMLReader;
    $z->xml($datos);
    while ($z->read()) {
        if (($z->nodeType == XMLReader::ELEMENT) && ($z->name == 'Message')) {

            $zz = new SimpleXMLElement($z->readOuterXml());

            $array_salida = array(
                "Fecha"                     => $zz->Request->Fecha,
                "Hora"                      => $zz->Request->Hora,
                "Ds_TransactionType"        => $zz->Request->Ds_TransactionType,
                "Ds_Card_Country"           => $zz->Request->Ds_Card_Country,
                "Ds_MerchantData"           => $zz->Request->Ds_MerchantData,
                "Ds_SecurePayment"          => $zz->Request->Ds_SecurePayment,
                "Ds_Order"                  => $zz->Request->Ds_Order,
                "Ds_Response"               => $zz->Request->Ds_Response,
                "Ds_AuthorisationCode"      => $zz->Request->Ds_AuthorisationCode,
                "Ds_Currency"               => $zz->Request->Ds_Currency,
                "Ds_ConsumerLanguage"       => $zz->Request->Ds_ConsumerLanguage,
                "Ds_Card_Type"              => $zz->Request->Ds_Card_Type,
                "Ds_MerchantCode"           => $zz->Request->Ds_MerchantCode,
                "Ds_Amount"                 => $zz->Request->Ds_Amount,
                "Ds_Terminal"               => $zz->Request->Ds_Terminal,
                "Ds_ErrorCode"              => $zz->Request->Ds_ErrorCode,
                "Ds_MerchantPartialPayment" => $zz->Request->Ds_MerchantPartialPayment
            );

            return $array_salida;
        }
    }
}

///////////////////////////////////////////////////////////////
// Función para obtener el Idioma desde el Chorizo de Datos  //
///////////////////////////////////////////////////////////////
function LangDesdeChorizo($chorizo_aux) {
    $chorizo_trozos = explode("#", base64_decode($chorizo_aux));
    $lang = explode("idioma=", $chorizo_trozos[5]);

    return $lang[1];
}

///////////////////////////////////////////////////////////////////
// Función para obtener la Referencia desde el Código de Barras  //
///////////////////////////////////////////////////////////////////
function ReferenciaDesdeCodbar($codbar_aux) {

    $cpr = substr($codbar_aux, 0, 5);
    $referencia = '';
    if ($cpr == '90502') {
        //$codbar = $cpr . $emisora . $referencia . $identificacion . str_pad($importe, 8, "0" , STR_PAD_LEFT);
        $referencia = substr($codbar_aux, 11, 12);
    } elseif ($cpr == '90508') {
        //$codbar = $cpr . $entFinalista . $fechaLimite. $emisora . $referencia . $identificacion . str_pad($importe, 8, "0" , STR_PAD_LEFT);
        $referencia = substr($codbar_aux, 21, 12);
    } elseif ($cpr == '90521') {
        //$codbar = $cpr . $emisora . $referencia . $identificacion . str_pad($importe, 8, "0", STR_PAD_LEFT) . '0';
        $referencia = substr($codbar_aux, 11, 12);
    } elseif ($cpr == '90522') {
        $referencia = substr($codbar_aux, 11, 12);
    } elseif ($cpr == '90523') { // Justificante tiene 13 caracteres
        $referencia = substr($codbar_aux, 11, 13);
    } else {
        $referencia = '';
    }

    return $referencia;
}

///////////////////////////////////////////////////////////////////
// Función para obtener la Emisora desde el Código de Barras     //
///////////////////////////////////////////////////////////////////
function EmisoraDesdeCodbar($codbar_aux) {
    $cpr = substr($codbar_aux, 0, 5);
    $emisora = '';
    if ($cpr == '90502') {
        $emisora = substr($codbar_aux, 5, 6);
    } elseif ($cpr == '90508') {
        $emisora = substr($codbar_aux, 15, 6);
    } elseif ($cpr == '90521') {
        $emisora = substr($codbar_aux, 5, 6);
    } elseif ($cpr == '90522') {
        $emisora = substr($codbar_aux, 5, 6);
    } elseif ($cpr == '90523') {
        $emisora = substr($codbar_aux, 5, 6);
    } else {
        $emisora = '';
    }

    return $emisora;
}

?>
