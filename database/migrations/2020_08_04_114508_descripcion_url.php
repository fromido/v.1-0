<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models;

class DescripcionUrl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            ['nombre'=>'pagament','descripcion'=> 'web'],
            ['nombre'=>'formulario2_ovt','descripcion'=> 'OVT'],
            ['nombre'=>'formulario2_ventanilla','descripcion'=> 'Ventanilla'],
            ['nombre'=>'ppv','descripcion'=> 'Punto verde'],
        ];

        Models\TiposUrl::insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
