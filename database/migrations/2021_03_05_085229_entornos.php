<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Entornos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entornos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 255);
            $table->string('ip_ubicacion');
            $table->string('ip_servicios');
            $table->string('ip_externa');
            $table->timestamps();
        });
        Schema::create('apps', function (Blueprint $table) {
            $table->integer('id', false, true);
            $table->unsignedInteger('id_entorno');
            $table->text('descripcion');
            $table->string('url')->nullable(true);
            $table->string('pago_online')->nullable(true);
            $table->string('paralizar_cuota')->nullable(true);
            $table->string('url_redsys', 255)->nullable(true);
            $table->string('url_pagookko_redsys', 255)->nullable(true);
            $table->string('url_notificacion_redsys', 255)->nullable(true);
            $table->string('formulario_smartpol')->nullable(true);
            $table->foreign('id_entorno')->references('id')->on('entornos');
            $table->primary(array('id', 'id_entorno'));
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entornos');
        Schema::dropIfExists('apps');
    }
}
