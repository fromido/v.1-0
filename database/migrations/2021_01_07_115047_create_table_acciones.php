<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableAcciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acciones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_ayuntamiento',false,true)->nullable(true);
            $table->unsignedInteger('id_app')->nullable(true);
            $table->string('nombre_accion',255)->nullable(true);
            $table->string('status',25)->nullable(true);
            $table->text('mensaje')->nullable(true);
            $table->timestamps();
            $table->foreign('id_app')->references('id')->on('modulos');
            $table->foreign('id_ayuntamiento')->references('id')->on('ayuntamientos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acciones');
    }
}
