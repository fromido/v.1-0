<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Notificaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notificaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo_ayuntamiento')->nullable(false);
            $table->string('fecha',15);
            $table->string('hora',10);
            $table->bigInteger('importe');
            $table->integer('moneda');
            $table->string('numero_pedido',25);
            $table->string('codigo_fuc',15);
            $table->string('terminal',5);
            $table->integer('codigo_respuesta');
            $table->longText('datos_comercio');
            $table->integer('pago_seguro');
            $table->string('tipo_operacion',5);
            $table->string('pais',5);
            $table->string('codigo_autorizacion',10);
            $table->string('idioma',5);
            $table->string('tipo_tarjeta',5);
            $table->string('referencia',13);
            $table->string('emisora',6);
            $table->string('barcode',42)->nullable(false)->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notificaciones');
    }
}
