<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModificarNotificaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notificaciones', function (Blueprint $table) {
            $table->string('barcode', 54)->nullable(true)->unique()->change();
            $table->string('identificacion',10)->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notificaciones', function (Blueprint $table) {
            $table->dropUnique('notificaciones_barcode_unique');
            $table->string('barcode', 42)->nullable(true)->change();
            $table->dropColumn('identificacion');
        });
    }
}
