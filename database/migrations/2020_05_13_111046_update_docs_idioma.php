<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateDocsIdioma extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('docstpv_idiomas', function(Blueprint $table){
            $table->string('ayuda',300)->nullable();
            $table->string('nombre',300)->nullable();
            $table->string('img',1000)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('docstpv_idiomas', function (Blueprint $table) {
            $table->dropColumn('ayuda');
            $table->dropColumn('nombre');
            $table->dropColumn('img');
        });
    }
}
